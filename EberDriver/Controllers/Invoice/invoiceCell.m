//
//  invoiceCell.m
//  TaxiAnytimeAnywhereProvider
//
//  Created by Elluminati Mini Mac 5 on 20/09/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "invoiceCell.h"
#import "UIColor+Colors.h"
#import "invoiceCellData.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "invoice.h"
#import "UILabel+overrideLabel.h"

@implementation invoiceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _viewForCell.backgroundColor = [UIColor whiteColor];
    _viewForCell.layer.masksToBounds = NO;
    _viewForCell.layer.cornerRadius = 3.0;
    _viewForCell.layer.shadowColor=[UIColor textColor].CGColor;
    _viewForCell.layer.shadowOffset = CGSizeMake(-1, 1);
    _viewForCell.layer.shadowOpacity = 0.2;
    
    [_Title setTextColor:[UIColor textColor]];
    [_Value setTextColor:[UIColor textColor]];
}

-(void)setCellData:(invoiceCellData *)invoiceData
{
    _Title.text=invoiceData.cost_title;
    _Value.text=[NSString stringWithFormat:@"%@ %@",[invoice sharedObject].currency,invoiceData.cost_value];
    [_subTitle setFontForLabel:_subTitle withMaximumFontSize:10 andMaximumLines:1];
    if ([invoiceData.cost_title isEqualToString:NSLocalizedString(@"TIME_COST", nil)])
    {
       [_subTitle setText:[NSString stringWithFormat:@"%@ %@ / %@",CurrencySign,[invoice sharedObject].pricePerUnitTime,TIME_SUFFIX]];
    }
    else if([invoiceData.cost_title isEqualToString:NSLocalizedString(@"DISTANCE_COST", nil)])
    {
        [_subTitle setText:[NSString stringWithFormat:@"%@ %@ / %@",[invoice sharedObject].currency,[invoice sharedObject].pricePerUnitDistance,[invoice sharedObject].distanceUnit]];
    }
    else if([invoiceData.cost_title isEqualToString:NSLocalizedString(@"TAX_COST", nil)])
    { [_subTitle setText:[NSString stringWithFormat:@"%@ %%",[invoice sharedObject].tax]];
    }
    else if([invoiceData.cost_title isEqualToString:NSLocalizedString(@"BASE_PRICE", nil)])
    {
        
        if ([[invoice sharedObject].basePriceDistance isEqualToString:@"1.00"] || [[invoice sharedObject].basePriceDistance isEqualToString:@"0.00"])
        {
            [_subTitle setText:[NSString stringWithFormat:@"%@",[invoice sharedObject].distanceUnit]];
        }
        else
        {
            [_subTitle setText:[NSString stringWithFormat:@"/%@ %@",[invoice sharedObject].basePriceDistance,[invoice sharedObject].distanceUnit]];
        }
        
    }
    else
    {
        [_subTitle setHidden:YES];
    }
 }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
