//
//  DocumentCell.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 16/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "DocumentCell.h"
#import "UIColor+Colors.h"
#import "UtilityClass.h"
#import "UIImageView+image.h"
@implementation DocumentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_imgDocument setRoundImageWithColor:[UIColor borderColor]];
    _viewForCell.backgroundColor = [UIColor whiteColor];
    _viewForCell.layer.masksToBounds = NO;
    _viewForCell.layer.cornerRadius = 3.0;
    _viewForCell.layer.shadowOffset = CGSizeMake(-1, 1);
    _viewForCell.layer.shadowOpacity = 0.3;
    [_lblDocumentTitle setTextColor:[UIColor textColor]];
}

-(void)setCellData:(Document*)doc
{
    if(doc.isUploaded)
    {
        [_btnCell setHidden:NO];
        
        [[UtilityClass sharedObject] loadFromURL:[NSURL URLWithString:doc.providerDocumenUrl] callback:^(UIImage *image) {
            if (image)
            {
                _imgDocument.image=image;
                
            }else
            {
                _imgDocument.image=[UIImage imageNamed:@"user"];
            }
        }];
        
    }
    else
    {
        [_btnCell setHidden:YES];
        [_imgDocument setImage:[UIImage imageNamed:@"user"]];
    }
    [_lblDocumentTitle setText:[NSString stringWithFormat:@"%@",doc.providerDocumentTitle]];
    _strDocId=doc.providerDocumentId;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (IBAction)onClickBtnCell:(id)sender {
    [_btnCell setHidden:YES];
    [_imgDocument setImage:[UIImage imageNamed:@"user"]];
}
@end
