//
//  UIImageView+image.h
//  
//
//  Created by Elluminati Mini Mac 5 on 06/09/16.
//
//

#import <UIKit/UIKit.h>

@interface UIImageView (image)
-(void)setRoundImageWithColor:(UIColor*)borderColor;
-(void)downloadFromURL:(NSString *)url withPlaceholder:(UIImage *)placehold;
@end
