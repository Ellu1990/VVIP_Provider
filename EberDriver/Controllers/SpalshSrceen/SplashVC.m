//
//  SplashVC.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 10/10/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "SplashVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Parser.h"
#import "UtilityClass.h"
#import "Hotline.h"


@interface SplashVC ()
{
    BOOL isApiLoaded,isSettingGet;
}
@end

@implementation SplashVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_loadingIndicator setHidden:YES];
    [self.view addSubview:_loadingIndicator];
    [self doWebServiceCall];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
#pragma mark-WebService Calls
- (void)doWebServiceCall
{
     [self getApiKeys];
    }
-(void)getAppSetting
{
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:@"NOT_REQUIRED" forKey:@"NOT_REQUIRED"];
        [afn getDataFromUrl:WS_GET_SETTING_DETAILS withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error) {
            
            if ([[Parser sharedObject]parseVerificationTypes:response])
            {
                isSettingGet=YES;
                [self checkLogin];
            }
            
        }];
}
-(void)getApiKeys
{
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:@"NOT_REQUIRED" forKey:@"NOT_REQUIRED"];
    [afn getDataFromUrl:WS_GET_APP_KEYS withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error) {
        if ([[Parser sharedObject]parseApiKeys:response])
        {
            HotlineConfig *config = [[HotlineConfig alloc]initWithAppID:HotlineAppId  andAppKey:HotlineAppKey];
            [[Hotline sharedInstance] initWithConfig:config];

            [self getAppSetting];
        }
    }];
}
-(void)checkLogin
{
    if(PREF.isProviderLogin)
    {
        if (PREF.ProviderApproved)
        {
            if (PREF.ProviderType==1 )
            {
                if (PREF.PartnerApproved)
                {
                    [self wsCheckExsistingTrips];
                }
                else
                {
                    [APPDELEGATE goToMap];
                }
            }
            else
            {
                [self wsCheckExsistingTrips];
            }
        }
        else
        {
            [APPDELEGATE goToMap];
        }
    }
    else
    {
        [APPDELEGATE goToMain];
        return;
    }
}
-(void)wsCheckExsistingTrips
{
    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
    [dictparam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    AFNHelper *afn=[[AFNHelper alloc]init];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@/%@",PROVIDER_GET_TRIP,PREF.providerId,PREF.providerToken];
    [afn getDataFromUrl:strUrl withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error)
    {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               if ([[Parser sharedObject]getTrip:response])
                               {
                                   [self wsGetTripStatus];
                               }
                               else
                               {
                                   [APPDELEGATE goToMap];
                                   return;
                               }
                               
                           });
        }];
    
}
-(void)wsGetTripStatus
{
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        [dictparam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:PROVIDER_GET_TRIP_STATUS withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error) {
                               if ([[Parser sharedObject]getTripStatus:response])
                               {
                                   NSDictionary *jsonResponse;
                                   if([[Parser sharedObject ] stringToJson:response To:&jsonResponse])
                                   {
                                       NSDictionary *temp=[jsonResponse valueForKey:PARAM_TRIP];
                                       NSString *isProviderStatus=[NSString stringWithFormat:@"%@",[temp objectForKey:PARAM_IS_PROVIDER_ACCEPTED] ];
                                       if ([isProviderStatus isEqualToString:@"1"])
                                       {
                                           IS_TRIP_EXSIST=YES;
                                           IS_PROVIDER_ACCEPTED=YES;
                                       }
                                       else
                                       {
                                           IS_TRIP_EXSIST=YES;
                                           IS_PROVIDER_ACCEPTED=NO;
                                       }
                                       dispatch_async(dispatch_get_main_queue(), ^
                                       {
                                          // [_loadingIndicator stopAnimating];
                                          
                                           [APPDELEGATE goToMap];
                                           return;
                                       });
                                   }
                               }
			}];
}
@end
