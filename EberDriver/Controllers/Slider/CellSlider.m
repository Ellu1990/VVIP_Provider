//
//  CellSlider.m
//  UberNewUser
//
//  Created by Elluminati - macbook on 30/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "CellSlider.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"

@implementation CellSlider

#pragma mark -
#pragma mark - Methods
@synthesize lblName,imgIcon;
-(void)setCellData:(NSString *)strName andImage:(NSString*)strImg
{
    [lblName setTextColor:[UIColor textColor]];
    lblName.text=NSLocalizedString(strName,nil);
    lblName.font=[lblName.font fontWithSize:12];
    imgIcon.image=[UIImage imageNamed:strImg];
}
@end
