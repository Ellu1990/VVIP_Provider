//
//  CountryCode.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 09/08/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryCodeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryName;
-(void)setCellData:(NSString*)strCountryCode andCountry:(NSString *)strCountryName;

@end
