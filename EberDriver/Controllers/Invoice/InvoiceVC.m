//
//  DummyInvoice.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 20/09/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "InvoiceVC.h"
#import "invoice.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"

@interface InvoiceVC ()
{
NSMutableArray *arrForInvoice;
}
@end

@implementation InvoiceVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocalization];
    [self setInvoiceData];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{   [self customSetup];
    [_btnNavigation setTitle:NSLocalizedString(@"TITLE_INVOICE", nil) forState:UIControlStateNormal];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}
- (void)customSetup
{
    [self.btnNavigation addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)setInvoiceData
{
     Invoice *invoice=[Invoice sharedObject];
    [_lblInvoiceId setText:invoice.invoiceNumber];
    if ([invoice.paymentMode boolValue])
    {
        [_imgPayment setImage:[UIImage imageNamed:@"invoice_cash"]];
        [_lblPayment setText:NSLocalizedString(@"PAY_BY_CASH", nil)];
        
        [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"CASH_REMAINING_AMOUNT", nil), invoice.cashPayment]];
    }
    else
    {
        [_imgPayment setImage:[UIImage imageNamed:@"invoice_cash"]];
        [_lblPayment setText:NSLocalizedString(@"PAY_BY_CARD", nil)];
        if ([invoice.remainingPayment doubleValue]>0.00)
        {
            [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"REMAINING_AMOUNT", nil), invoice.remainingPayment]];
        }
        else
        {
            [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"CARD_REMAINING_AMOUNT", nil), invoice.cardPayment]];
        }
    }
    
    [_lblWalletAmount setText:[NSString stringWithFormat:NSLocalizedString(@"WALLET_PAYMENT", nil), invoice.walletPayment]];
    [_lblTotalValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.total]];
    [_lblTime setText:[NSString stringWithFormat:@"%@ %@", invoice.time,TIME_SUFFIX]];
    [_lblDistance setText:[NSString stringWithFormat:@"%@ %@", invoice.distance,DISTANCE_SUFFIX]];
    [_lblBaseCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.basePrice]];
    NSString *strTitle,*strSubValue;
    strTitle=NSLocalizedString(@"BASE_PRICE", nil);
    if ([invoice.basePriceDistance intValue]==1)
    {
        strSubValue=[NSString stringWithFormat:@"%@",invoice.distanceUnit];
    }
    else
    {
        strSubValue=[NSString stringWithFormat:@"%@ / %@",invoice.basePriceDistance,invoice.distanceUnit];
    }
    [_lblBaseCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    strTitle=NSLocalizedString(@"DISTANCE_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@ / %@",invoice.currency,invoice.pricePerUnitDistance,invoice.distanceUnit];
    [_lblDistanceCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblDistanceCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.distanceCost]];
    strTitle=NSLocalizedString(@"TIME_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@ / %@",invoice.currency,invoice.pricePerUnitTime,TIME_SUFFIX];
    [_lblTimeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblTimeCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.timeCost]];
    strTitle=NSLocalizedString(@"TAX_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %%",invoice.tax];
    [_lblTax setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblTaxCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.taxCost]];
    
    strTitle=NSLocalizedString(@"WAIT_TIME_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@",invoice.pricePerWaitingTime,TIME_SUFFIX];
    [_lblWaitTimeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblWaitTimeCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.waitingTimeCost]];
    
    strTitle=NSLocalizedString(@"SURGE_COST", nil);
    strSubValue=[NSString stringWithFormat:@"x%@",invoice.surgeMultiplier];
    [_lblSurgeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblSurgeCostValue setText:[NSString stringWithFormat:@" %@ %@",invoice.currency,invoice.surgeTimeFee]];
    
    [_lblRefferalBonousValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.referralBonus]];
    [_lblPromoBonousValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.promoBonus]];
    [APPDELEGATE hideLoadingView];
}
- (IBAction)onClickBtnSubmit:(id)sender
{
    [APPDELEGATE
     showLoadingWithTitle:NSLocalizedString(@"LOADING_FEEDBACK_VIEW", nil)];
    [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:self];
}
-(NSMutableAttributedString*)makeAttributedString:(NSString*)title andSubtitle:(NSString*)subtitle
{
    UIFont *titleFont = [UIFont fontWithName:_lblBaseCostValue.font.fontName    size:14.0];
    // Define your regular font
    UIFont *subtitleFont = [UIFont fontWithName:_lblBaseCostValue.font.fontName    size:11.0];;
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",title,subtitle]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor textColor] range:NSMakeRange(0,title.length)];
    [string addAttribute:NSFontAttributeName value:titleFont range:NSMakeRange(0,title.length)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor labelTextColor] range:NSMakeRange(title.length+1,subtitle.length)];
    [string addAttribute:NSFontAttributeName value:subtitleFont range:NSMakeRange(title.length+1,subtitle.length)];
    return string;
    
}
-(void)setLocalization
{
    [_lblInvoiceId setTextColor:[UIColor textColor]];
    [_lblInvoiceId setBackgroundColor:[UIColor whiteColor]];
    
    [_lblTotalValue setTextColor:[UIColor buttonColor]];
    [_lblDiscount setTextColor:[UIColor buttonColor]];
    [_lblRemainingAmount setTextColor:[UIColor labelTextColor]];
    [_lblWalletAmount setTextColor:[UIColor labelTextColor]];
    [_lblTime setTextColor:[UIColor buttonTextColor]];
    [_lblDistance setTextColor:[UIColor buttonTextColor]];
    [_lblPayment setTextColor:[UIColor buttonTextColor]];
    [_lblTotal setContentMode:UIViewContentModeCenter];
    [_lblTotalValue setContentMode:UIViewContentModeCenter];
    [_btnSubmit setBackgroundColor:[UIColor buttonColor]];
    [_lblTimeCost setTextColor:[UIColor textColor]];
    [_lblTimeCostValue setTextColor:[UIColor textColor]];
    [_lblDistanceCost setTextColor:[UIColor textColor]];
    [_lblDistanceCostValue setTextColor:[UIColor textColor]];
    [_lblBaseCost setTextColor:[UIColor textColor]];
    [_lblBaseCostValue setTextColor:[UIColor textColor]];
    [_lblWaitTimeCost setTextColor:[UIColor textColor]];
    [_lblWaitTimeCostValue setTextColor:[UIColor textColor]];
    [_lblSurgeCost setTextColor:[UIColor textColor]];
    [_lblSurgeCostValue setTextColor:[UIColor textColor]];
    [_lblTotal setTextColor:[UIColor textColor]];
    [_lblTotal setText:[NSLocalizedString(@"TOTAL", nil) uppercaseString]];
    [_lblRefferalBonous setText:[NSLocalizedString(@"REFFEREL_BONOUS", nil) capitalizedString]];
    [_lblPromoBonous setText:[NSLocalizedString(@"PROMO_BONOUS", nil) capitalizedString]];
    [_lblDiscount setText:[NSLocalizedString(@"DISCOUNT", nil) uppercaseString]];
    
    [_lblTax setTextColor:[UIColor textColor]];
    [_lblTaxCostValue setTextColor:[UIColor textColor]];
    [_btnSubmit setTitle:[NSLocalizedString(@"BTN_SUBMIT", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnSubmit setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    _viewForDiscounts=[[UtilityClass sharedObject]addShadow:_viewForDiscounts];
    _btnSubmit.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSubmit.clipsToBounds = YES;
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [self.viewForTimeAndDistance setBackgroundColor:[UIColor GreenColor]];
    [self.viewForDiscounts setBackgroundColor:[UIColor whiteColor]];
}

@end
