//
//  LoginVc.m
//  Eber Driver
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "LoginVc.h"
#import "UIColor+Colors.h"
#import "NSObject+Constants.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UITextField+overtextfield.h"
#import "PreferenceHelper.h"
#import "Parser.h"
#import "GoogleUtility.h"
#import "UILabel+overrideLabel.h"

@interface LoginVc ()
{
    CustomAlertWithTextInput *dialogForForgetPassword;
    UITapGestureRecognizer     *tapGesture;

}
@end

@implementation LoginVc

@synthesize activeTextField,strEmail,strLoginBy,strSocialUniqueID,dictParam,txtEmail,txtPsw,btnFb,btnForgotPsw,btnSignIn,btnSignUp,btnGoogle;
#pragma mark-View Life-Cycle


- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setLocalization];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    [_scrLogin addGestureRecognizer:tapGesture];;
    /*Google SignIn Delegate*/
    if (!btnGoogle.hidden && !btnFb.hidden)
    {
        FBSDKLoginManager *logout = [[FBSDKLoginManager alloc] init];
        [logout logOut];
    }
	dictParam=[[NSMutableDictionary alloc]init];
    strLoginBy=MANUAL;
    strSocialUniqueID=@"";
    [dictParam setValue:strLoginBy forKey:PARAM_LOGIN_BY];
    [dictParam setValue:DEVICE_TOKEN forKey:PARAM_DEVICE_TOKEN];
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	 self.navigationController.navigationBarHidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:self.view.window];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}
-(void)dealloc
{
    btnGoogle=nil;
    btnFb=nil;
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGRect frame=_imgLogo.frame;
    frame.size.height=MIN(_imgLogo.frame.size.width, _imgLogo.frame.size.height) ;
    frame.size.width=frame.size.height;
    _imgLogo.frame=frame;
}
#pragma mark-Actions

-(IBAction)onClickMoveToRegister:(id)sender
{
 [self performSegueWithIdentifier:SEGUE_TO_DIRCET_REGI sender:self];
}


//Manual Login
-(IBAction)onClickLogin:(id)sender
{
    [_scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.view endEditing:YES];
    if (![[UtilityClass sharedObject]isValidEmailAddress:txtEmail.text])
	{
		[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
       // [txtPsw becomeFirstResponder];
	}
	else if (txtPsw.text.length<6)
	{
		[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_PASSWORD", nil)];
       // [txtPsw becomeFirstResponder];
	}
	else
	{
		[dictParam setValue:txtEmail.text forKey:PARAM_EMAIL];
		[dictParam setValue:txtPsw.text forKey:PARAM_PASSWORD];
		[dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
		[dictParam setValue:@"" forKey:PARAM_SOCIAL_UNIQUE_ID];
		[dictParam setValue:DEVICE_TOKEN forKey:PARAM_DEVICE_TOKEN];
        [dictParam setValue:MANUAL forKey:PARAM_LOGIN_BY];
		[self wsLogin];
	}

}
//FB Login
-(IBAction)onClickFacebook:(id)sender
{

    
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"google"];//Set PreferenceHelpererence Login With Facebook
	if ([APPDELEGATE connected])
	{
		strLoginBy=@"facebook";
		FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
		[login prepareForInterfaceBuilder];
		
		[login 	logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
							handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
		 {
				if (error)
				{
				}
				else if (result.isCancelled)
				{
				}
				else
				{
					[APPDELEGATE  showLoadingWithTitle:NSLocalizedString(@"LOADING_FACEBOOK_INFO", nil)];
					if ([FBSDKAccessToken currentAccessToken])
					{
						FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
												initWithGraphPath:@"me"
												parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
												HTTPMethod:@"GET"];
						
						[request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
													   id result,
													   NSError *error)
						 {
							 // Handle the result
							 [APPDELEGATE hideLoadingView];
							 strSocialUniqueID=[result valueForKey:PARAM_FB_ID];
							 strEmail=[result valueForKey:PARAM_FB_EMAIL];
							 strLoginBy=FACEBOOK;

							 [dictParam setValue:strEmail forKey:PARAM_EMAIL];
							 [dictParam setValue:@"" forKey:PARAM_PASSWORD];
							 [dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
							 [dictParam setValue:strSocialUniqueID forKey:PARAM_SOCIAL_UNIQUE_ID];
							 [dictParam setValue:DEVICE_TOKEN forKey:PARAM_DEVICE_TOKEN];
							 [dictParam setValue:strLoginBy forKey:PARAM_LOGIN_BY];
							 [self wsLogin];
							 
							 }];
					}
				}
		 }];
		[login logOut];
	}
    else
    {
    
    
    }
	
}
//Google Login
-(IBAction)onClickGooglePlus:(id)sender
{
    [[GoogleUtility sharedObject] signInGoogle:^(BOOL success,GIDGoogleUser *user,NSError *error) {
        
        if(success)
        {
            
            strSocialUniqueID= user.authentication.idToken;
            strLoginBy=GOOGLE;
            strEmail=user.profile.email;
            
            if ([UtilityClass isEmpty:DEVICE_TOKEN])
            {
                DEVICE_TOKEN=@"";
            }
            [dictParam setValue:strEmail forKey:PARAM_EMAIL];
            [dictParam setValue:@"" forKey:PARAM_PASSWORD];
            [dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
            [dictParam setValue:strSocialUniqueID forKey:PARAM_SOCIAL_UNIQUE_ID];
            [dictParam setValue:DEVICE_TOKEN forKey:PARAM_DEVICE_TOKEN];
            [dictParam setValue:strLoginBy forKey:PARAM_LOGIN_BY];
            [self wsLogin];
        }
        else
        {
            
        }
    } withParent:self];
}

-(IBAction)onClickForgotPsw:(id)sender
{
    dialogForForgetPassword=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"FORGET_PASSWORD", nil) placeHolder:NSLocalizedString(@"EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) okButtonTitle:NSLocalizedString(@"ALERT_BTN_SEND", nil)];
    [self.view bringSubviewToFront:dialogForForgetPassword];
}
-(void)onClickOkButton:(NSString *)inputTextData view:(CustomAlertWithTextInput *)view
{
    if([[UtilityClass sharedObject]isValidEmailAddress:inputTextData])
    {
        [dialogForForgetPassword removeFromSuperview];
        [self wsForgetPassword:inputTextData andView:view];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
    }
}
-(void)wsForgetPassword:(NSString*)email andView:(CustomAlertWithTextInput*)view
{
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_SENDING_PASSWORD", nil)];
        NSMutableDictionary *tempData=[[NSMutableDictionary alloc]init];
        [tempData setObject:email forKey:PARAM_EMAIL];
        [tempData setObject:[NSNumber numberWithInteger:PROVIDER_TYPE ] forKey:PARAM_TYPE];
        
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:WS_PROVIDER_FORGET_PASSWORD withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                               NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                            options:kNilOptions
                                                                                              error:nil];
                               
                               if([[jsonResponse valueForKey:SUCCESS] boolValue])
                               {
                                   [[AppDelegate sharedAppDelegate]hideLoadingView];
                                   NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE]];
                                   [view removeFromSuperview];
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
                               }
                               else
                               {
                                   [[AppDelegate sharedAppDelegate]hideLoadingView];
                                   NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               }
                           });
			}];
}

#pragma mark- Web Service Methods
-(void)wsLogin
{
    AFNHelper *afn=[[AFNHelper alloc]init];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_LOG_IN", nil)];
        
		[afn getDataFromUrl:PROVIDER_LOGIN withParamData:dictParam withMethod:@"POST" withBlock:^(id response, NSError *error)
		 {
			 dispatch_async(dispatch_get_main_queue(), ^
			{
                if([[Parser sharedObject]isLoginSuccess:response])
                {
                    if (PREF.isProviderActive)
                    {
                        [self wsCheckTrip];
                    }
                    else
                    {
                        [APPDELEGATE hideLoadingView];
                        [self performSegueWithIdentifier:SEGUE_TO_MAP sender:self];
                        return;
                    }
                }
                else
                {
                    [APPDELEGATE hideLoadingView];
                }
			});
         }];
}
-(void)wsCheckTrip
{
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        [dictparam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@/%@",PROVIDER_GET_TRIP,PREF.providerId,PREF.providerToken];
        [afn getDataFromUrl:strUrl withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               if ([[Parser sharedObject]getTrip:response])
                               {
                                   [self wsGetTripStatus];
                               }
                               else
                               {
                                   [APPDELEGATE hideLoadingView];
                                   [self performSegueWithIdentifier:SEGUE_TO_MAP sender:self];
                                   return;
                               }
                               
                           });
        }];
}
-(void)wsGetTripStatus
{
   
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        [dictparam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:PROVIDER_GET_TRIP_STATUS withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               if ([[Parser sharedObject]getTripStatus:response])
                               {
                                   NSDictionary *jsonResponse;
                                   if([[Parser sharedObject ] stringToJson:response To:&jsonResponse])
                                   {
                                       NSDictionary *temp=[jsonResponse valueForKey:PARAM_TRIP];
                                       
                                       NSString *isProviderStatus=[NSString stringWithFormat:@"%@",[temp objectForKey:@"is_provider_accepted"] ];
                                       
                                       if ([isProviderStatus isEqualToString:@"1"])
                                       {
                                           IS_TRIP_EXSIST=YES;
                                           IS_PROVIDER_ACCEPTED=YES;
                                       }
                                       else
                                       {
                                           IS_TRIP_EXSIST=YES;
                                           IS_PROVIDER_ACCEPTED=NO;
                                       }
                                       [APPDELEGATE hideLoadingView];
                                       [self performSegueWithIdentifier:SEGUE_TO_MAP sender:self];
                                           return;
									}
                               }
                               [APPDELEGATE hideLoadingView];
                               
                           });
			}];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}
#pragma mark - SET LOCALIZATION
-(void) setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
	[btnFb setHidden:YES];
    [btnGoogle setHidden:YES];
    [_viewForNavigationBar setBackgroundColor:[UIColor GreenColor]];
	/*set Title and Color*/
    [btnForgotPsw setTitle:NSLocalizedString(@"FORGET_PASSWORD", nil) forState:UIControlStateNormal];
    [_lblTitle setText:[NSLocalizedString(@"LOGIN", nil) capitalizedString] withColor:[UIColor labelTitleColor]];
    [btnSignIn setBackgroundColor:[UIColor buttonColor]];
    /**SignIN Button SetUp*/
    [btnSignIn setTitle:[NSLocalizedString(@"LOGIN", nil) uppercaseString] forState:UIControlStateNormal];
    [btnSignIn setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
     btnSignIn.layer.shadowOpacity=0.8;
	 btnSignIn.layer.shadowOffset= CGSizeMake(0, 3.0f);
	 btnSignIn.layer.shadowColor = [UIColor blackColor].CGColor;
     [btnSignUp setTitle:[NSLocalizedString(@"REGISTER", nil) capitalizedString] forState:UIControlStateNormal];
     UIFont *font=[UIFont fontWithName:@"Roboto-Bold" size: btnSignUp.titleLabel.font.pointSize];
     [btnSignUp.titleLabel setFont:font];
     [btnForgotPsw.titleLabel setFont:font];
    [btnForgotPsw setTitleColor:[UIColor textColor] forState:UIControlStateNormal];
    [btnSignUp setTitleColor:[UIColor textColor] forState:UIControlStateNormal];
    [_lblHaventAccount setText:NSLocalizedString(@"Haven't account yet ?", nil)];
    [txtEmail setBorder];
	[txtPsw setBorder];
    [txtPsw setTextColor:[UIColor textColor]];
    [txtEmail setTextColor:[UIColor textColor]];
    txtEmail.placeholder=NSLocalizedString(@"EMAIL", nil);
    txtPsw.placeholder=NSLocalizedString(@"PASSWORD", nil);
    [_viewForLogin setBackgroundColor:[UIColor backGroundColor]];
}
#pragma mark-TEXTVIEW DELEGATE
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField=textField;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField=nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtEmail)
    {
        [txtPsw becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self onClickLogin:nil];
    }
    return YES;
}

#pragma mark-KeyBoardNotificationMethod
// Called when UIKeyboardWillShowNotification is sent
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    if (activeTextField)
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGPoint pt;
        CGRect rc = [activeTextField bounds];
        rc = [activeTextField convertRect:rc toView:_scrLogin];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= kbSize.height;
        CGRect mainRect=self.scrLogin.frame;
        mainRect.size.height-=kbSize.height;
        if (!CGRectContainsRect(mainRect, rc))
        {
            [_scrLogin setContentOffset:pt animated:YES];
        }
    }
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillHide:(NSNotification*)notification
{
    
    
}
- (void)hideKeyBoard
{
    [self.view endEditing:YES];
    [_scrLogin setContentOffset:CGPointZero];
}
@end
