//
//  main.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 12/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
