//
//  Document.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 16/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Document : NSObject
@property(nonatomic,copy) NSString* providerDocumentId;
@property(nonatomic,copy) NSString* providerDocumentTitle;
@property(nonatomic,copy) NSString* providerDocumenUrl;
@property(nonatomic,assign)BOOL isUploaded;
@property(nonatomic,assign)BOOL isOptional;

@end
