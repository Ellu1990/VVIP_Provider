//
//  ProfileVC.m
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//


#import "ProfileVC.h"
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "UITextField+overtextfield.h"
#import "CountryCodeCell.h"
#import "Parser.h"
#import "PreferenceHelper.h"
#import "UIImageView+image.h"
#define  MAX_NUMBER 10

@interface ProfileVC ()
{
    BOOL searching;
    NSString *tempContactNumber,*strSelectedCountryCode;
    NSMutableArray *tempArrForCountry;
    UITapGestureRecognizer *singleTap,*tapGesture;
    CustomAlertWithTextInput *dialogForAccountVerification;
    CustomOtpDialog *optDialog;
}
@end

@implementation ProfileVC
@synthesize activeTextField,strForID,strForSocialId,strForToken,strImageData,arrForCountry,isPicAdded,dictParam;

#pragma mark
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getServiceCountry];
    arrForCountry=[[NSMutableArray alloc]init];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    [_scrollView addGestureRecognizer:tapGesture];
    dictParam=[[NSMutableDictionary alloc]init];
    tempArrForCountry=[[NSMutableArray alloc]init];
    strSelectedCountryCode=@"+93";
    [self SetLocalization];
    [self setDataForUserInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:self.view.window];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
-(void)dealloc
{}
#pragma mark
#pragma mark - Action Methods

- (IBAction)onClickBtnPickerCancel:(id)sender
{
    _viewForSelectCountryCode.hidden=YES;
}

- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnselectCountry:(id)sender
{
    [self.view endEditing:YES];
    [_tblForCountryCode reloadData];
    _searchbar.text=@"";
    _viewForSelectCountryCode.hidden=NO;
    _viewForSelectCountryCode=[[UtilityClass sharedObject]addShadow:_viewForSelectCountryCode];
}
- (IBAction)onClickBtnUpdate:(id)sender
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    UIButton *btn=(UIButton *)sender;
    if (!self.txtAddress.enabled)
    {
        if(!btn.isSelected)
        {
            [btn setSelected:YES];
            [self textEnable];
            [_btnUpdateProfile setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
            [_txtFirstName becomeFirstResponder];
            [[UtilityClass sharedObject] showToast:NSLocalizedString(@"TOAST_EDIT_PROFILE", nil) ];
        }
    }
    else
    {
        if ([PREF.providerLoginBy isEqualToString:GOOGLE] || [PREF.providerLoginBy isEqualToString:FACEBOOK] )
        {
            if([self checkValidation])
            {
                    if (PREF.isSmsOtpOn)
                    {
                        if([PREF.providerPhone isEqualToString:_txtNumber.text] && [PREF.providerCountryCode isEqualToString:[_btnSelectCountry titleForState:UIControlStateNormal]])
                        {
                            [self wsUpdateProfile];
                        }
                        else
                        {
                            NSString *concatNumber=[NSString stringWithFormat:@"%@%@",_btnSelectCountry.titleLabel.text,_txtNumber.text];
                            if ([tempContactNumber isEqualToString:concatNumber])
                            {
                                dialogForAccountVerification=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_ACCOUNT_VERIFY", nil) placeHolder:NSLocalizedString(@"CURRENT_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) okButtonTitle:NSLocalizedString(@"YES", nil)];
                                [self.view bringSubviewToFront:dialogForAccountVerification];
                                
                            }
                            else
                            {
                                [self wsGetOtp];
                            }
                        }
                    }
                    else
                    {
                        [self wsUpdateProfile];
                    }
                }
        }
        else
        {
            if(_txtNewPassword.text.length>=6)
                    {
                        if([_txtNewPassword.text isEqualToString:_txtConfirmPassword.text])
                        {
                            if([self checkValidation])
                            {
                                dialogForAccountVerification=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_ACCOUNT_VERIFY", nil) placeHolder:NSLocalizedString(@"CURRENT_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) okButtonTitle:NSLocalizedString(@"ALERT_BTN_SEND", nil)];
                                [self.view bringSubviewToFront:dialogForAccountVerification];
                                
                            }
                        }
                        else
                        {
                            [[UtilityClass sharedObject] showToast:NSLocalizedString(@"TOAST_CONFIRM_PASSWORD_NOT_MATCH", nil) ];
                           // [_txtNewPassword becomeFirstResponder];
                        }
                    }
                    else
                    {
                        if([self checkValidation])
                        {
                        if (PREF.isSmsOtpOn)
                        {
                            if([PREF.providerPhone isEqualToString:_txtNumber.text] && [PREF.providerCountryCode isEqualToString:[_btnSelectCountry titleForState:UIControlStateNormal]])
                            {
                                dialogForAccountVerification=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_ACCOUNT_VERIFY", nil) placeHolder:NSLocalizedString(@"CURRENT_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) okButtonTitle:NSLocalizedString(@"YES", nil)];
                                [self.view bringSubviewToFront:dialogForAccountVerification];
                            }
                            else
                            {
                                NSString *concatNumber=[NSString stringWithFormat:@"%@%@",_btnSelectCountry.titleLabel.text,_txtNumber.text];
                                if ([tempContactNumber isEqualToString:concatNumber])
                                {
                                    dialogForAccountVerification=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_ACCOUNT_VERIFY", nil) placeHolder:NSLocalizedString(@"CURRENT_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) okButtonTitle:NSLocalizedString(@"YES", nil)];
                                    [self.view bringSubviewToFront:dialogForAccountVerification];
                                    
                                }
                                else
                                {
                                    [self wsGetOtp];
                                }
                            }
                        }
                        else
                        {
                            dialogForAccountVerification=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_ACCOUNT_VERIFY", nil) placeHolder:NSLocalizedString(@"CURRENT_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) okButtonTitle:NSLocalizedString(@"YES", nil)];
                            [self.view bringSubviewToFront:dialogForAccountVerification];
                        }
                        }
                    }
        }
    }
 }
#pragma mark-TableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searching)
        return tempArrForCountry.count;
    else
        return arrForCountry.count;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    CountryCodeCell *cell = (CountryCodeCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
        cell=[[CountryCodeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableCell"];
    if (searching)
        [cell setCellData: [[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] andCountry:[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_NAME]];
    else
        [cell setCellData: [[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] andCountry:[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_NAME]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    if(searching)
    {
        [_btnSelectCountry setTitle:[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] forState:UIControlStateNormal];
    }
    else
    {
        [_btnSelectCountry setTitle:[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] forState:UIControlStateNormal];
    }
    [_tblForCountryCode deselectRowAtIndexPath:indexPath animated:YES];
    [_viewForSelectCountryCode setHidden:YES];
}
#pragma  mark Search Country;
- (void)searchBar:(UISearchBar *)SearchBar textDidChange:(NSString *) searchText {
    
    [tempArrForCountry removeAllObjects];
    if (searchText.length == 0)
    {
        searching = NO;
    }
    else
    {
        searching = YES;
        for (NSDictionary *Country in arrForCountry)
        {
            NSString *countryName=[Country valueForKey:PARAM_COUNTRY_NAME];
            
            if ([countryName hasPrefix:[searchText capitalizedString]] )
            {
                [tempArrForCountry addObject:Country];
            }
        }
        
    }
    
    [_tblForCountryCode reloadData];
}

#pragma mark
#pragma mark - WEB SERVICE Methods

-(BOOL) checkValidation
{
    if(_txtFirstName.text.length<1 || _txtLastName.text.length<1 || _txtEmail.text.length<1 || _txtNumber.text.length<10 )
    {
        if(_txtFirstName.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_FIRST_NAME", nil)];
            //[_txtFirstName becomeFirstResponder];
        }
        else if(_txtLastName.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_LAST_NAME", nil)];
            //[_txtLastName becomeFirstResponder];
        }
        else if(_txtEmail.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
            //[_txtEmail becomeFirstResponder];
        }
        else if(_txtNumber.text.length<10)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_PHONE", nil)];
           // [_txtNumber becomeFirstResponder];
        }
        return false;
    }
    else
    {
        if([[UtilityClass sharedObject]isValidEmailAddress:_txtEmail.text])
        {
            return true;
        }
        
        else
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
            return false;
        }
    }
    
}


-(void)buildParameter
{
    [dictParam setValue:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictParam setValue:_txtFirstName.text forKey:PARAM_FIRST_NAME];
    [dictParam setValue:_txtLastName.text forKey:PARAM_LAST_NAME];
    [dictParam setValue:_txtNumber.text forKey:PARAM_PHONE];
    [dictParam setValue:_btnSelectCountry.titleLabel.text forKey:PARAM_COUNTRY_CODE];
    [dictParam setValue:_txtBio.text forKey:PARAM_BIO];
    [dictParam setValue:_txtAddress.text forKey:PARAM_ADDRESS];
    [dictParam setValue:_txtZipCode.text forKey:PARAM_ZIPCODE];
    
    [dictParam setValue:@"" forKey:@"new_password"];
    if (_txtNewPassword.text.length)
    {
        [dictParam setValue:_txtNewPassword.text forKey:@"new_password"];
    }
    
    [dictParam setValue:PREF.providerSocialId  forKey:PARAM_SOCIAL_UNIQUE_ID];
    
    if (!isPicAdded)
    {
        [dictParam setValue:@"" forKey:PARAM_PICTURE_DATA];
    }
}
-(void) wsUpdateProfile
{
    [self buildParameter];
    [self.view endEditing:YES];

    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATING_PROFILE", nil)];
    
        
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSString *url=[NSString stringWithFormat:@"%@%@",PROVIDER_UPDATE_PROFILE,PREF.providerId];
        
        [afn getDataFromUrl:url withParamData:dictParam withMethod:put withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                if([[Parser sharedObject]isProfileUpdated:response])
                                {
                                    [self setDataForUserInfo];
                                    [_overlayView setHidden:YES];
                                    [APPDELEGATE hideLoadingView];
                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                }
                                [APPDELEGATE hideLoadingView];
                                
                                
                            });
             
         }];
}

#pragma mark
#pragma mark - Text Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{  activeTextField=textField;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField=nil;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_txtFirstName)
    {
        [_txtLastName becomeFirstResponder];textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtLastName)
    {
        [_txtNumber becomeFirstResponder]; textField.text = [textField.text capitalizedString];
    }

    else if(textField==_txtNewPassword)
    {
        [_txtConfirmPassword becomeFirstResponder];}
    else if(textField==_txtConfirmPassword){
        [textField resignFirstResponder];}
    else if(textField==_txtNumber){
	       [_txtBio becomeFirstResponder];}
    else if(textField==_txtBio){
        [_txtAddress becomeFirstResponder];
        textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtAddress)
    {   [_txtZipCode becomeFirstResponder];
        textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtZipCode)
    {
        [_txtNewPassword becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField==_txtNumber)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
            
        }
        
        else if(_txtNumber.text.length >=MAX_NUMBER)
        {
            [_txtAddress becomeFirstResponder];
            return NO;
        }
        
    }
    return YES;
}
- (NSString *)encodeToBase64String:(UIImage *)image
{
    NSData * data = [UIImagePNGRepresentation(image) base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return [NSString stringWithUTF8String:[data bytes]];
}
-(void)tapDetected
{
    [self.view endEditing:YES];
    SelectImage *view=[SelectImage getSelectImageViewwithParent:self];
    [view bringSubviewToFront:self.parentViewController.view];
    
}
#pragma -
#pragma mark Image picker delegate methdos
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    _imgProPic.image=image;
    isPicAdded=true;
    NSString *strimage =[self encodeToBase64String:_imgProPic.image];
    [dictParam setObject:strimage forKey:PARAM_PICTURE_DATA];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)SetLocalization
{
    [_btnNavigation setTitle:NSLocalizedString(@"TITLE_PROFILE", nil) forState:UIControlStateNormal];
    [_lblFullName setTextColor:[UIColor whiteColor]];
    [_lblChangePassword setText:[NSLocalizedString(@"CHANGE_PASSWORD", nil) capitalizedString]];
    [_lblChangePassword setTextColor:[UIColor whiteColor]];
    [_lblChangePassword setBackgroundColor:[UIColor buttonColor]];
    [_profileView setBackgroundColor:[UIColor backGroundColor]];
    _viewForProfile.layer.borderColor = [UIColor borderColor].CGColor;
    _viewForProfile.layer.borderWidth = 2.0f;
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    /*textView setUp*/
    [_txtEmail  setPlaceholder:[NSLocalizedString(@"EMAIL", nil) capitalizedString]];
    [_txtNumber setPlaceholder:[NSLocalizedString(@"PHONE", nil) capitalizedString]];
    [_txtNewPassword setPlaceholder:[NSLocalizedString(@"NEW_PASSWORD", nil) capitalizedString]];
    [_txtConfirmPassword setPlaceholder:[NSLocalizedString(@"CONFIRM_PASSWORD", nil) capitalizedString]];
    [_txtBio setPlaceholder:[NSLocalizedString(@"BIO", nil) capitalizedString]];
    [_txtAddress setPlaceholder:[NSLocalizedString(@"ADDRESS", nil) capitalizedString]];
    [_txtZipCode setPlaceholder:[NSLocalizedString(@"ZIPCODE", nil) capitalizedString]];
    [_txtFirstName setPlaceholder:[NSLocalizedString(@"FIRST_NAME", nil) capitalizedString]];
    [_txtLastName setPlaceholder:[NSLocalizedString(@"LAST_NAME", nil) capitalizedString]];
   
    
    [_txtEmail   setTextColor:[UIColor  textColor]];
    [_txtNumber  setTextColor:[UIColor  textColor]];
    [_txtNewPassword  setTextColor:[UIColor  textColor]];
    [_txtConfirmPassword  setTextColor:[UIColor  textColor]];
    [_txtBio  setTextColor:[UIColor  textColor]];
    [_txtAddress  setTextColor:[UIColor  textColor]];
    [_txtZipCode  setTextColor:[UIColor  textColor]];
    [_txtFirstName  setTextColor:[UIColor  textColor]];
    [_txtLastName  setTextColor:[UIColor  textColor]];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _profileView.frame.size.height);
    [_lblFullName setTextColor:[UIColor labelTitleColor]];
    UIFont *font=[UIFont fontWithName:@"Roboto-Bold" size: _titleLabel.font.pointSize];
    [_titleLabel setFont:font];
    [_titleLabel setTextColor:[UIColor GreenColor]];
    
    /*Add Gesture To Image*/
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imgProPic setUserInteractionEnabled:NO];
    [_imgProPic addGestureRecognizer:singleTap];
    
    [self textDisable];
    /*test Code*/
    CGRect frame=CGRectMake(_imgProPic.frame.origin.x, _imgProPic.frame.origin.y+_imgProPic.frame.size.height/2, _imgProPic.frame.size.width, _imgProPic.frame.size.height/2);
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame =frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor gradientColorUp] CGColor], (id)[[UIColor gradientColorDown] CGColor], nil];
    [_imgProPic.layer insertSublayer:gradient atIndex:0];
    [self.view bringSubviewToFront:_viewForProfile];

}
/*
 *Set Data From Login Preference
 */
-(void)setDataForUserInfo
{
    _txtFirstName.text=PREF.providerFirstName;
    _txtLastName.text=PREF.providerLastName;
    [_lblFullName setBackgroundColor:[UIColor clearColor]];
    _lblFullName.text=[NSString stringWithFormat:@"%@ %@",_txtFirstName.text,_txtLastName.text];
    _txtEmail.text=PREF.providerEmail;
    _txtNumber.text=[NSString stringWithFormat:@"%@",PREF.providerPhone ];
    _txtAddress.text=PREF.providerAddress;
    _txtBio.text=PREF.providerBio;
    _txtZipCode.text=PREF.providerZipcode;
    if ([PREF.providerLoginBy isEqualToString:GOOGLE] || [PREF.providerLoginBy isEqualToString:FACEBOOK] )
    {
        [_txtNewPassword setHidden:YES];
        [_txtConfirmPassword setHidden:YES];
        _txtAddress.frame=_txtNewPassword.frame;
        _txtZipCode.frame=_txtConfirmPassword.frame;
    }
    [_imgProPic downloadFromURL:PREF.providerPicture withPlaceholder:[UIImage imageNamed:@"user"]];
    [_imgProPic setUserInteractionEnabled:NO];
    [self textDisable];
}
-(void)textDisable
{
    _txtEmail.enabled=NO;
    _txtNumber.enabled=NO;
    _txtNewPassword.enabled=NO;
    _txtConfirmPassword.enabled=NO;
    _txtBio.enabled=NO;
    _txtAddress.enabled=NO;
    _txtZipCode.enabled=NO;
    _txtFirstName.enabled=NO;
    _txtLastName.enabled=NO;
    [_btnSelectCountry setEnabled:NO];
    
    
}
-(void)textEnable
{
    _txtEmail.enabled=NO;
    _txtNumber.enabled=YES;
    _txtNewPassword.enabled=YES;
    _txtConfirmPassword.enabled=YES;
    _txtBio.enabled=YES;
    _txtAddress.enabled=YES;
    _txtZipCode.enabled=YES;
    _txtFirstName.enabled=YES;
    _txtLastName.enabled=YES;
    [_imgProPic setUserInteractionEnabled:YES];
    [_btnSelectCountry setEnabled:YES];
}
-(void)onClickOkButton:(NSString *)inputTextData view:(CustomAlertWithTextInput *)view
{
    if (inputTextData.length<6)
    {
        [[UtilityClass sharedObject] showToast:NSLocalizedString(@"TOAST_ENTER_PASSWORD", nil) ];
    }
    else
    {
        
        [dictParam setObject:inputTextData forKey:PARAM_OLD_PASSWORD];
        [self buildParameter];
        [self.view endEditing:YES];
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATING_PROFILE", nil)];
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSString *url=[NSString stringWithFormat:@"%@%@",PROVIDER_UPDATE_PROFILE,PREF.providerId];
        [afn getDataFromUrl:url withParamData:dictParam withMethod:put withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                if([[Parser sharedObject]isProfileUpdated:response])
                                {
                                    [self setDataForUserInfo];
                                    [_overlayView setHidden:YES];
                                    [APPDELEGATE hideLoadingView];
                                    [view removeFromSuperview];
                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                }
                                [APPDELEGATE hideLoadingView];
                                
                                
                            });
             
         }];

    
    };
}
// Called when UIKeyboardWillShowNotification is sent
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    if (activeTextField)
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGPoint pt;
        CGRect rc = [activeTextField bounds];
        rc = [activeTextField convertRect:rc toView:_scrollView];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= kbSize.height;
        CGRect mainRect=self.scrollView.frame;
        mainRect.size.height-=kbSize.height;
        if (!CGRectContainsRect(mainRect, rc))
        {
            [_scrollView setContentOffset:pt animated:YES];
        }
    }
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillHide:(NSNotification*)aNotification
{
    
}
- (void)hideKeyBoard
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];
}

-(void)wsGetOtp
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_SENDING_OTP", nil)];
    NSMutableDictionary *dictOtpParam=[[NSMutableDictionary alloc]init];
    [dictOtpParam setObject:_txtNumber.text forKey:PARAM_PHONE];
    [dictOtpParam setObject:_btnSelectCountry.titleLabel.text forKey:PARAM_COUNTRY_CODE];
    [dictOtpParam setObject:[NSNumber numberWithInteger:PROVIDER_TYPE] forKey:PARAM_TYPE];
    
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_GET_VERIFICATION_OTP withParamData:dictOtpParam withMethod:post withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                         options:kNilOptions
                                                                                           error:nil];
                            NSLog(@"Respose Otp %@",response);
                            
                            if([[jsonResponse valueForKey:SUCCESS] boolValue])
                            {
                                NSString *emailOtp=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_EMAIL_OTP] ];
                                NSString *smsOtp=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_SMS_OTP]];
                                
                                optDialog=[[CustomOtpDialog alloc]initWithOtpEmail:emailOtp optSms:smsOtp emailOtpOn:NO smsOtpOn:YES delegate:self];
                                [self.view bringSubviewToFront:optDialog];
                            }
                            else
                            {
                                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE] ];
                                [[UtilityClass sharedObject]showToast:NSLocalizedString(errorCode, nil)];
                            }
                            [[AppDelegate sharedAppDelegate] hideLoadingView];
                        });
         
     }];
    
}
-(void)onClickCustomDialogOtpOk:(CustomOtpDialog *)view
{
    dialogForAccountVerification=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_ACCOUNT_VERIFY", nil) placeHolder:NSLocalizedString(@"CURRENT_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) okButtonTitle:NSLocalizedString(@"YES", nil)];
    [self.view bringSubviewToFront:dialogForAccountVerification];
    tempContactNumber= [NSString stringWithFormat:@"%@%@",_btnSelectCountry.titleLabel.text,_txtNumber.text];
}
-(void)getServiceCountry
{
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_GET_COUNTRIES withParamData:nil withMethod:@"GET" withBlock:^(id response, NSError *error)
     {
         if ([[Parser sharedObject]parseServiceCountries:response toArray:&arrForCountry])
         {
             
         }
     }];
    
}


@end
