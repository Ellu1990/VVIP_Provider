//
//  User.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 20/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSObject+Constants.h"

@interface User : NSObject
+ (User *)sharedObject;

@property(nonatomic,copy) NSString*  firstName;
@property(nonatomic,copy) NSString*  tripNumber;
@property(nonatomic,copy) NSString*  lastName;
@property(nonatomic,copy) NSString*  rating;
@property(nonatomic,copy) NSString*  profileImage;
@property(nonatomic,copy) NSString*  pickUpLatitude;
@property(nonatomic,copy) NSString*  pickUpLongitude;
@property(nonatomic,copy) NSString*  destLatitude;
@property(nonatomic,copy) NSString*  destLongitude;
@property(nonatomic,copy) NSString*  user_id;
@property(nonatomic,copy) NSString*  phone;
@property(nonatomic,copy) NSString*  pickUpAddress;
@property(nonatomic,copy) NSString*  destAddress;
@property(nonatomic,copy) NSString*  paymentMode;
@property(nonatomic,copy) NSString*  totalTime;
@property(nonatomic,copy) NSString*  totalDistance;
@property(nonatomic,copy) NSString*  phoneCountryCode;
@property(nonatomic,copy) NSString*  note;
@property(nonatomic,assign) NSInteger  providerStatus;
@property(nonatomic,assign) double  waitingPrice;
-(void)resetObject;
@end
