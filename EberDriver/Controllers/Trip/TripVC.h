//
//  TripVC.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 22/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "CustomCancelTripDialog.h"

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@import CoreLocation;


@interface TripVC : UIViewController
<GMSMapViewDelegate,
UINavigationControllerDelegate,
CLLocationManagerDelegate,
CustomCancelTripDialogDelegate,
AVAudioPlayerDelegate
>
{
    CLLocationManager *locationManager;
    CLLocationCoordinate2D
    source_coordinate,
    dest_coordinate,
    current_coordinate;
    NSString *strDestLatitude,*strDestLongitude;
    float myDirection;
}
@property (strong, nonatomic) AVAudioPlayer *pickupSound;
@property (weak, nonatomic) IBOutlet UILabel *lblCancel;
-(void)checkPush;
/*view*/
@property (weak, nonatomic) IBOutlet UIView *viewForUser;
@property (weak, nonatomic) IBOutlet UIView *viewForAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForNote;
/**************/
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelRequest;

#pragma mark-Timers
@property(nonatomic, strong) NSTimer *timerForTripUpdateLocation,*timerForSOS,*timerForWaitingTime;
- (IBAction)onClickBtnProviderStatus:(id)sender;
- (IBAction)onClickBtnCall:(id)sender;
- (IBAction)onClickBtnCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTripId;
@property (weak, nonatomic) IBOutlet UILabel *lblTripIdValue;

/*View For Address*/
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressValue;
/*view For User*/
@property (weak, nonatomic) IBOutlet UILabel *lblEstTimeValue;
@property (weak, nonatomic) IBOutlet UILabel *lblEstDistanceValue;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblEstTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEstDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPic;
@property (weak, nonatomic) IBOutlet UIButton *btnProviderStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnGoToGoogle;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentValue;
/*View For Waiting Time*/
@property (weak, nonatomic) IBOutlet UIView *viewForWaitingTime;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitingTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitingTimeValue;
- (IBAction)onClickViewNote:(id)sender;

@end
