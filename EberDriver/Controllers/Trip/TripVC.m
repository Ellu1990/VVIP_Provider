//
//  TripVC.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 22/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "TripVC.h"
#import "NSObject+Constants.h"
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "PreferenceHelper.h"
#import "Parser.h"
#import "User.h"
#import "SWRevealViewController.h"
#import "UILabel+overrideLabel.h"
#import "SliderVC.h"
#import "UIImageView+image.h"
#import "UIColor+Colors.h"
#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiansToDegrees(x) (x * 180.0 / M_PI)
#define d2r ((M_PI)/180.0)
#define r2d (180.0/(22/7.0))

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

@interface TripVC ()
{
    NSMutableDictionary *postUpdateParam;
    GMSMarker *driverMarker,*clientMarker,*destinationMarker;
    ProviderStatus providerStatus;
    NSString *distance,*time;
    GMSPath *googlePath;
    GMSMutablePath *currentPath;
    NSInteger currentWaitingTime;
    GMSPolyline *polyLineGooglePath,*polyLineCurrentPath;
    BOOL isCurrentPathDraw,isPathDrawn,isDoublePressed,isMapBounds,isSoundPlayed,timerForGetTripStatus,call_from_set_trip_status;
}
@end

@implementation TripVC
@synthesize pickupSound;
#pragma mark-VIEW LIFE_CYCLE
-(void)viewDidLoad
{
    [super viewDidLoad];
    IS_TRIP_EXSIST=YES;
    isPathDrawn=true;
    isCurrentPathDraw=FALSE;
    currentPath= [GMSMutablePath path];
    
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    [self customSetup];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    self.mapView.delegate = self;
    [[_mapView settings]  setRotateGestures:NO];
    current_coordinate=[self getLocation];
    [self setLocalization];
    
    timerForGetTripStatus=YES;
    [self setUserInfo];
    providerStatus=ProviderStatusComing;
    clientMarker = [[GMSMarker alloc] init];
    driverMarker = [[GMSMarker alloc] init];
    destinationMarker=[[GMSMarker alloc] init];
    [self wsGetTripStatus];
    /**set UserData*/
   clientMarker.icon=[UIImage imageNamed:@"pin_pickup"];
    destinationMarker.icon=[UIImage imageNamed:@"pin_destination"];
    driverMarker.icon=[UIImage imageNamed:@"pin_driver"];
    postUpdateParam=[[NSMutableDictionary alloc]init];
    [self wsUpdateLocation];
    [self performSelectorOnMainThread:@selector(startTripUpdateLocationTimer) withObject:nil waitUntilDone:YES];
	
	User *user = [User sharedObject];
	NSLog(@"Note = %@",user.note);
	
	if (![user.note isEqualToString:@""]) {
		self.viewForNote.hidden = NO;
	}
	else
	{
		self.viewForNote.hidden = YES;
	}
}
-(void)setUserInfo
{
    User *u=[User sharedObject];
    [_lblTripIdValue setText:[User sharedObject].tripNumber];
    [_lblAddressValue setText:u.pickUpAddress];
    _lblUserName.text=[NSString stringWithFormat:@"%@ %@",u.firstName,u.lastName];
    [_lblUserName setTextColor:[UIColor textColor]];
    
   [_imgUserPic downloadFromURL:u.profileImage withPlaceholder:[UIImage imageNamed:@"user"]];
    dest_coordinate.longitude=[u.pickUpLongitude doubleValue];
    dest_coordinate.latitude=[u.pickUpLatitude doubleValue];
    if (current_coordinate.latitude && current_coordinate.longitude)
    {
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:current_coordinate zoom:17];
        [_mapView animateWithCameraUpdate:updatedCamera];
    }
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgUserPic setRoundImageWithColor:[UIColor borderColor]];
}
-(void)setLocalization
{
    [self.view bringSubviewToFront:_viewForUser];
    [self.view bringSubviewToFront:_viewForAddress];
    [_lblTripId setTextColor:[UIColor labelTextColor]];
    [_lblTripIdValue setTextColor:[UIColor textColor]];
    [_lblTripId setText:NSLocalizedString(@"TRIP_ID", nil)];
    [_lblPaymentValue setText:NSLocalizedString(@"CASH", nil) withColor:[UIColor textColor]];
    [_lblEstDistanceValue setText:[NSString stringWithFormat:@"0.0 %@",DISTANCE_SUFFIX] withColor:[UIColor textColor]];
    [_lblEstTimeValue setText:NSLocalizedString(@"00", nil) withColor:[UIColor textColor]];
    _lblAddress.textColor=[UIColor labelTitleColor];
    _lblAddressValue.textColor=[UIColor labelTitleColor];
    [_lblAddress setText:[NSLocalizedString(@"PICKUP_ADDRESS", nil)uppercaseString]];
    [_viewForAddress setBackgroundColor:[UIColor GreenColor]];
    [_lblEstTime setText:NSLocalizedString(@"ESTIMATE_TIME", nil) withColor:[UIColor labelTextColor]];
    [_lblEstDistance setText:NSLocalizedString(@"ESTIMATE_DISTANCE", nil) withColor:[UIColor labelTextColor]];
    [_lblPayment setText:NSLocalizedString(@"PAYMENT_IN", nil) withColor:[UIColor labelTextColor]];
    [_btnProviderStatus setTitle:[NSLocalizedString(@"COMING", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnProviderStatus setBackgroundColor:[UIColor buttonColor]];
    [_btnProviderStatus setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    _btnProviderStatus.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    _btnProviderStatus.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_btnGoToGoogle setBackgroundColor:[UIColor buttonColor]];
    [_btnCancelRequest setBackgroundColor:[UIColor buttonColor]];
    _btnProviderStatus.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [_lblEstTimeValue setText:[NSString stringWithFormat:@"0.0 %@",TIME_SUFFIX]];
    [_lblEstDistanceValue setText:[NSString stringWithFormat:@"0.0 %@",DISTANCE_SUFFIX]];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}
#pragma mark-Location Delegate Methods
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *newLocation = [locations lastObject];
    if (newLocation)
    {
        current_coordinate=[newLocation coordinate];
        if (current_coordinate.latitude && current_coordinate.longitude)
        {
            strForCurLatitude=[NSString stringWithFormat:@"%.8f",newLocation.coordinate.latitude];
            strForCurLongitude=[NSString stringWithFormat:@"%.8f",newLocation.coordinate.longitude];
            
        }
        myDirection=[newLocation course];
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat: 0.60f] forKey:kCATransactionAnimationDuration];
        [CATransaction setCompletionBlock:^{
            [currentPath addCoordinate:[newLocation coordinate]];
            [self drawCurrentPath];
        
        }];
        driverMarker.position=current_coordinate;
        [self rotateMarker:myDirection];
        [CATransaction commit];
    }
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}
#pragma mark WEbServices
-(void)wsUpdateLocation
{
    if (current_coordinate.latitude && current_coordinate.longitude)
     {
            NSLog(@"Update Location:%f %f",current_coordinate.latitude,current_coordinate.longitude);
            [postUpdateParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
            [postUpdateParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
            [postUpdateParam setObject:[NSString stringWithFormat:@"%.8f",current_coordinate.latitude] forKey:PARAM_LATITUDE];
            [postUpdateParam setObject:[NSString stringWithFormat:@"%.8f",current_coordinate.longitude] forKey:PARAM_LONGITUDE];
            [postUpdateParam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
            [postUpdateParam setObject:[NSNumber numberWithFloat:myDirection] forKey:PARAM_BEARING];
            AFNHelper *afn=[[AFNHelper alloc]init];
            NSString *strUrl=[NSString stringWithFormat:@"%@%@",PROVIDER_UPDATE_LOCATION,PREF.providerId];
            if (!postUpdateParam) {
                
            }
            else
            {
            [afn getDataFromUrl:strUrl withParamData:postUpdateParam withMethod:put withBlock:^(id response, NSError *error) {
                
                                   NSDictionary *dictResponse=[[NSDictionary alloc]init];
                                   
                                   if ([[Parser sharedObject] stringToJson:response To:&dictResponse])
                                   {
                                       
                                       if ([[dictResponse valueForKey:SUCCESS]boolValue])
                                       {
                                           if (timerForGetTripStatus)
                                           {
                                               double tempTime,tempDistance;
                                               tempDistance=[[dictResponse valueForKey:PARAM_TOTAL_DISTANCE] doubleValue];
                                               tempTime=[[dictResponse valueForKey:PARAM_TOTAL_TIME]doubleValue];
                                               if (tempTime<1 )
                                               {
                                                   tempTime=0;
                                                   
                                               }
                                               if (tempDistance<=0)
                                               {
                                                   tempDistance=0.0;
                                               }
                                               [User sharedObject].totalTime=[NSString stringWithFormat:@"%.0f",tempTime];
                                               [User sharedObject].totalDistance=[NSString stringWithFormat:@"%.2f",tempDistance];
                                               
                                               [self wsGetTripStatus];
                                           }
                                           else
                                           {
                                           
                                           }
                                       }
                                   }
						}];
            }
        }
}
-(void)wsGetTripStatus
{
    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
    [dictparam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
     AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:PROVIDER_GET_TRIP_STATUS withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error) {
        if (timerForGetTripStatus)
        {
            if ([[Parser sharedObject]getTripStatus:response])
            {
              source_coordinate.latitude=[[User sharedObject].pickUpLatitude doubleValue];
              source_coordinate.longitude=[[User sharedObject].pickUpLongitude doubleValue];
              dest_coordinate.latitude=[[User sharedObject].destLatitude doubleValue];
              dest_coordinate.longitude=[[User sharedObject].destLongitude doubleValue];
                providerStatus=(ProviderStatus)[User sharedObject].providerStatus;
              dispatch_async(dispatch_get_main_queue(), ^{
                      if([[User sharedObject].paymentMode isEqualToString:@"1"])
                          [_lblPaymentValue setText:NSLocalizedString(@"CASH", nil)];
                      else
                          [_lblPaymentValue setText:NSLocalizedString(@"CARD", nil)];
                      switch (providerStatus)
                      {
                          case ProviderStatusAccepted:
                              [APPDELEGATE hideLoadingView];
                              providerStatus=ProviderStatusComing;
                              [self.btnProviderStatus setTitle:[NSLocalizedString(@"COMING", nil) uppercaseString] forState:UIControlStateNormal];
                              [self getTimeAndDistance:current_coordinate destLocation:source_coordinate];
                              break;
                          case ProviderStatusComing:
                              [APPDELEGATE hideLoadingView];
                              [self getGooglePathFromServer];
                              providerStatus=ProviderStatusArrived;
                              [self.btnProviderStatus setTitle:[NSLocalizedString(@"ARRIVED", nil) uppercaseString] forState:UIControlStateNormal];
                              [self getTimeAndDistance:current_coordinate destLocation:source_coordinate];
                              break;
                          case ProviderStatusArrived:
                              
                              [APPDELEGATE hideLoadingView];
                              providerStatus=ProviderStatusTripStarted;
                              [_lblEstDistanceValue setText:[NSString stringWithFormat:@"0.0 %@",DISTANCE_SUFFIX] withColor:[UIColor textColor]];
                              [_lblEstTimeValue setText:[NSString stringWithFormat:@"0.0 %@",TIME_SUFFIX] withColor:[UIColor textColor]];
                              [_lblAddress setText:NSLocalizedString(@"DESTINATION", nil)];
                              [self.btnProviderStatus setTitle:[NSLocalizedString(@"START_TRIP", nil) uppercaseString] forState:UIControlStateNormal];
                              if(![_timerForWaitingTime isValid]&&[User sharedObject].waitingPrice>0.0)
                              {
                                  currentWaitingTime=WAITING_SECONDS_REMAIN;
                                  _timerForWaitingTime=[NSTimer scheduledTimerWithTimeInterval:1.0f target:self
                                                                                      selector:@selector(waitingCounter) userInfo:nil repeats:YES];
                              }
                              
                              if ([UtilityClass isEmpty:[User sharedObject].destAddress])
                              {
                                  _lblAddressValue.text=NSLocalizedString(@"DEST_ADDRESS_NOT_AVAILABLE", nil);
                                  break;
                              }
                              else
                              {
                                  [self getGooglePathFromServer];
                                  [_lblAddressValue setText:[UtilityClass formatAddress:[User sharedObject].destAddress]];
                                 break;
                              }
                             
                          case ProviderStatusTripStarted:
                              [APPDELEGATE hideLoadingView];
                              providerStatus=ProviderStatusTripCompleted;
                              [_lblCancel setHidden:YES];
                              if (!_btnCancelRequest.hidden)
                              {
                                  CGRect frame=_btnProviderStatus.frame;
                                  frame.size.width=APPDELEGATE.window.frame.size.width-_btnGoToGoogle.frame.size.width;
                                  [_btnProviderStatus setFrame:frame];
                              }
                              [_btnProviderStatus setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
                              [_btnCancelRequest setHidden:YES];
                              [_lblAddress setText:NSLocalizedString(@"DESTINATION", nil)];
                              
                              [_lblEstTimeValue setText:[NSString stringWithFormat:@"%@ %@",[User sharedObject].totalTime,TIME_SUFFIX] ];
                              [_lblEstDistanceValue setText:[NSString stringWithFormat:@"%@ %@",[User sharedObject].totalDistance,DISTANCE_SUFFIX]];
                              [_lblEstTime setText:NSLocalizedString(@"TOTAL_TIME", nil) ];
                              [_lblEstDistance setText:NSLocalizedString(@"TOTAL_DISTANCE", nil)];
                              [self.btnProviderStatus setTitle:[NSLocalizedString(@"END_TRIP", nil) uppercaseString] forState:UIControlStateNormal];
                              
                              if ([UtilityClass isEmpty:[User sharedObject].destAddress])
                              {
                                  _lblAddressValue.text=NSLocalizedString(@"DEST_ADDRESS_NOT_AVAILABLE", nil);
                                  dest_coordinate.latitude=0;
                                  dest_coordinate.longitude=0;
                                  break;
                              }
                              else
                              {
                                  if ([_lblAddressValue.text isEqualToString:[UtilityClass formatAddress:[User sharedObject].destAddress]])
                                  {
                                      [self getGooglePathFromServer];
                                  }
                                  else
                                  {
                                      [_lblAddressValue setText:[UtilityClass formatAddress:[User sharedObject].destAddress]];
                                      [self showRouteFromSourceToDestination:source_coordinate to:dest_coordinate];
                                  }
                                  break;
                              }

                          case ProviderStatusTripCompleted:
                              
                              [_lblAddress setText:NSLocalizedString(@"DESTINATION", nil)];
                              source_coordinate.latitude=[[User sharedObject].pickUpLatitude doubleValue];
                              source_coordinate.longitude=[[User sharedObject].pickUpLongitude doubleValue];
                              if (IS_TRIP_EXSIST)
                              {
                                 [self getAddressForLocation:current_coordinate];
                              }
                              
                              break;
                          default:
                              [APPDELEGATE hideLoadingView];
                              break;
                      }
                      if (call_from_set_trip_status)
                      {
                          
                          
                          call_from_set_trip_status=NO;
                      }
                      [self setMarkerAndCameraWithCurrentLatitude:current_coordinate pickUpLatitude:source_coordinate andDestinationLatLong:dest_coordinate];
                  });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                [[UtilityClass sharedObject]displayAlertWithTitle:@"Trip Cancel" andMessage:@"ok"];
                [self performSelectorOnMainThread:@selector(stopTripUpdateLocationTimer) withObject:nil waitUntilDone:YES];
                 IS_TRIP_EXSIST=NO;
                [_viewForUser setHidden:YES];
                [_viewForAddress setHidden:YES];
                [[User sharedObject]resetObject];
                [APPDELEGATE goToMap];});
            }
         }
	}];
}
-(void)wsSetTripStatus
{
    if (current_coordinate.latitude && current_coordinate.longitude)
    {
        timerForGetTripStatus=NO;
        NSMutableDictionary *putStatusParam=[[NSMutableDictionary alloc]init];
        [putStatusParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        [putStatusParam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
        [putStatusParam setObject:[NSNumber numberWithDouble:[strForCurLatitude doubleValue]] forKey:PARAM_LATITUDE];
        [putStatusParam setObject:[NSNumber numberWithDouble:[strForCurLongitude doubleValue]] forKey:PARAM_LONGITUDE];
        [putStatusParam setObject:[NSNumber numberWithInteger:providerStatus] forKey:PARAM_PROVIDER_STATUS];
        [putStatusParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@",PROVIDER_SET_TRIP_STATUS,PREF.providerTripId];
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATING_TRIP_STATUS", nil) ];
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:strUrl withParamData:putStatusParam withMethod:put withBlock:^(id response, NSError *error) {
        NSDictionary *jsonResponse;
        if([[Parser sharedObject ] stringToJson:response To:&jsonResponse])
        {
            timerForGetTripStatus=YES;
            if ([jsonResponse valueForKey:SUCCESS])
            {
                call_from_set_trip_status=YES;
                
                [self performSelectorInBackground:@selector(wsGetTripStatus) withObject:nil];	//you can pass any object if you have
            }
        }
        else
        {
          dispatch_async(dispatch_get_main_queue(), ^
         {
             [APPDELEGATE hideLoadingView];
             [[UtilityClass sharedObject]displayAlertWithTitle:@"Trip Cancel" andMessage:@"ok"];
         });
        }
        }];
    }
}
-(void)wsCompleteTrip
{
        [APPDELEGATE showLoadingWithTitle:@""];
        NSLog(@"End Trip Called");
        NSMutableDictionary *putCompleteTripParam=[[NSMutableDictionary alloc]init];
        [putCompleteTripParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        [putCompleteTripParam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
        [putCompleteTripParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
        [putCompleteTripParam setObject:[User sharedObject].destAddress forKey:PARAM_TRIP_DESTINATION_ADDRESS];
        [putCompleteTripParam setObject:[NSString stringWithFormat:@"%f",current_coordinate.latitude] forKey:PARAM_LATITUDE];
        [putCompleteTripParam setObject:[NSString stringWithFormat:@"%f",current_coordinate.longitude] forKey:PARAM_LONGITUDE];
        [putCompleteTripParam setObject:[NSNumber numberWithInteger:providerStatus ] forKey:PARAM_PROVIDER_STATUS];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@/%@",PROVIDER_COMPLETE_TRIP,PREF.providerId,PREF.providerToken];
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:strUrl withParamData:putCompleteTripParam withMethod:put withBlock:^(id response, NSError *error)
         {
             if([[Parser sharedObject ] completeTrip:response])
             {
                 dispatch_async(dispatch_get_main_queue(), ^
                            {
                                if([_timerForTripUpdateLocation isValid])
                                    {
                                       [self performSelectorOnMainThread:@selector(stopTripUpdateLocationTimer) withObject:nil waitUntilDone:YES];
                                        IS_TRIP_EXSIST=NO;
                                        IS_PROVIDER_ACCEPTED=NO;
                                        
                                        [self performSegueWithIdentifier:SEGUE_TO_INVOICE sender:self];
                                        
                                        return ;
                                    }
                                    
                            });
            }
            else
            {dispatch_async(dispatch_get_main_queue(), ^
                            {
                 [APPDELEGATE hideLoadingView];
                  [[UtilityClass sharedObject]displayAlertWithTitle:@"Trip Cancel" andMessage:@"ok"];
                            });
                            }
                                
                          
         }];
    
}
-(void)wsCancelTrip:(NSString*)cancelReason
{
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictParam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
    [dictParam setObject:cancelReason forKey:PARAM_CANCEL_TRIP_REASON];
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_CANCLE_REQUEST", nil)];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",WS_CANCEL_TRIP,PREF.providerId];
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:strUrl withParamData:dictParam withMethod:put withBlock:^(id response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                           NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions
                                                                                          error:nil];
                           if (jsonResponse)
                           {
                               if([[jsonResponse valueForKey:SUCCESS]boolValue])
                               {
                                   [[AppDelegate sharedAppDelegate]hideLoadingView];
                                   IS_TRIP_EXSIST=NO;
                                   IS_PROVIDER_ACCEPTED=NO;
                                   [APPDELEGATE goToMap];
                               }
                               else
                               {
                                   NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               }
                           }
                           [[AppDelegate sharedAppDelegate]hideLoadingView];
                       });
		}];
}
#pragma mark-MAP AND CAMERA METHOD
#pragma mark - Google Map Delegate
-(void)mapViewSnapshotReady:(GMSMapView *)mapView
{
}
#pragma mark-Actions
-(void)onClickCancelTripDialogWithReason:(NSString *)strCancelTripReason
{
    [self wsCancelTrip:strCancelTripReason];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
- (IBAction)onClickBtnProviderStatus:(id)sender
{
    if (providerStatus==ProviderStatusTripCompleted)
    {
        [self clickTwiceToEndTrip];
    }
    else
    {
        isPathDrawn=true;
        [self wsSetTripStatus];
    }
}
- (IBAction)onClickBtnCall:(id)sender
{
   NSString *mobileNo=[NSString stringWithFormat:@"tel:%@%@",[User sharedObject].phoneCountryCode,[User sharedObject].phone ];
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNo]];
}
- (IBAction)onClickBtnTargetLocation:(id)sender
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    if (providerStatus>=ProviderStatusTripStarted)
    {
        if (dest_coordinate.latitude && dest_coordinate.longitude)
        {
            bounds = [bounds includingCoordinate:destinationMarker.position];
        }
    }
    else
    {
        bounds = [bounds includingCoordinate:clientMarker.position];
    }
    bounds = [bounds includingCoordinate:driverMarker.position];
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat: animationMapDelayed] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
    }];
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100.0f]];
    [CATransaction commit];
}
- (IBAction)onClickBtnCancel:(id)sender
{
    CustomCancelTripDialog *viewForAlert=[[CustomCancelTripDialog alloc]initInView:self];
    [viewForAlert bringSubviewToFront:self.parentViewController.view];
}
- (IBAction)onClickBtnGoogle:(id)sender
{
    NSString *strUrl;
    if ([_lblAddress.text isEqualToString:NSLocalizedString(@"PICKUP_ADDRESS",nil)])
    {
        strUrl=[NSString stringWithFormat:@"daddr=%@&directionsmode=driving",[User sharedObject].pickUpAddress];
        [self goToGoogleMapApp:strUrl];
    }
    else
    {
        if ([UtilityClass isEmpty:[User sharedObject].destAddress]) {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_DEST_ADDRESS_NOT_AVAILABLE", nil)];
        }
        else
        {
            strUrl=[NSString stringWithFormat:@"daddr=%@&directionsmode=driving",[User sharedObject].destAddress];
            [self goToGoogleMapApp:strUrl];
        }
    }
    
}
-(void)goToGoogleMapApp:(NSString*)strUrl
{
    NSString *escapedString = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    strUrl=[escapedString stringByReplacingOccurrencesOfString:@"%20" withString:@"+"];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]])
    {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[@"comgooglemaps://?" stringByAppendingString:strUrl]]];
    }
    else
    {
        [[UtilityClass sharedObject] showToast:NSLocalizedString(@"TOAST_GOOGLE_MAP_NOT_INSTALLED", nil)];
    }
}
#pragma mark-SetMarkers
-(void)setMarkerAndCameraWithCurrentLatitude:(CLLocationCoordinate2D)currentLatlong pickUpLatitude:(CLLocationCoordinate2D)pickUpLatitude andDestinationLatLong:(CLLocationCoordinate2D)destinationLatLong
{
    isMapBounds=NO;
    if (driverMarker.map)
    {
        driverMarker.position = currentLatlong;
        
    }
    else
    {
        isMapBounds=YES;
        driverMarker.position = currentLatlong;
        driverMarker.map = _mapView;
    }
    if (destinationLatLong.latitude!=0 && destinationLatLong.longitude !=0) {
            [self setDestinationMarker:destinationLatLong];
    }
    if(pickUpLatitude.latitude!=0 && pickUpLatitude.longitude != 0)
    {
            [self setPickUpMarker:pickUpLatitude];
     }
    if (isMapBounds)
    {
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        if (providerStatus>=ProviderStatusTripStarted)
        {
            bounds = [bounds includingCoordinate:destinationMarker.position];
        }
        else
        {
            bounds = [bounds includingCoordinate:clientMarker.position];
        }
        bounds = [bounds includingCoordinate:driverMarker.position];
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100.0f]];
   
    }
    else
    {
        [_mapView animateToLocation:current_coordinate];
        [_mapView animateToZoom:17];
    }
}
-(void)setDestinationMarker:(CLLocationCoordinate2D)destinationLatLong
{
    if (destinationLatLong.latitude!=0 && destinationLatLong.longitude!=0)
    {
        destinationMarker.position = destinationLatLong;
        destinationMarker.map = _mapView;
    }
}
-(void)setPickUpMarker:(CLLocationCoordinate2D)pickUpLatLong
{
    if (pickUpLatLong.latitude!=0 && pickUpLatLong.longitude!=0)
    {
        clientMarker.position = pickUpLatLong;
        clientMarker.map = _mapView;
    }
}
#pragma mark-USER DEFINE METHOD
- (void)customSetup
{
    [self.btnNavigation addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
     SWRevealViewController *revealController = [self revealViewController];
    [revealController tapGestureRecognizer];
    [self.btnNavigation addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
	_viewForNote.layer.cornerRadius = 5.0;

}
/*Get Time And Distance*/
-(void) getTimeAndDistance:(CLLocationCoordinate2D) src_location destLocation:(CLLocationCoordinate2D)dest_location
{
    NSString *src_lat=[NSString stringWithFormat:@"%f",src_location.latitude];
    NSString *src_long=[NSString stringWithFormat:@"%f",src_location.longitude];
    NSString *dest_lat=[NSString stringWithFormat:@"%f",dest_location.latitude];
    NSString *dest_long=[NSString stringWithFormat:@"%f",dest_location.longitude];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@,%@&destinations=%@,%@&key=%@",src_lat,  src_long, dest_lat, dest_long, GoogleServerKey];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSData *jsonData = [NSData dataWithContentsOfURL:url];
    @try {
        if(jsonData != nil)
        {
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
            NSString *status=[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_STATUS];
            if ([status isEqualToString:GOOGLE_PARAM_STATUS_OK])
            {
                
                    
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       
                                       NSString *tempDistance=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DISTANCE] valueForKey:GOOGLE_PARAM_VALUES];
                                       NSString *second=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DURATION] valueForKey:GOOGLE_PARAM_VALUES];
                                       NSString *tempTime=[[UtilityClass sharedObject]secondToTime:[second intValue]];
                                       tempDistance=[NSString stringWithFormat:@"%.2f",[[UtilityClass sharedObject]meterToKilometer:[tempDistance doubleValue]] ];
                                       if ([tempDistance floatValue]<50 && providerStatus==ProviderStatusArrived && (!isSoundPlayed) )
                                       {   if(!PREF.isPickUpSoundOff)
                                       {[self playSound];}
                                       }
                                       [_lblEstTimeValue setText:[NSString stringWithFormat:@"%@ %@",tempTime,TIME_SUFFIX]];
                                       [_lblEstDistanceValue setText:[NSString stringWithFormat:@"%@ %@",tempDistance,DISTANCE_SUFFIX]];
                                   });
            }
            
        }
        else
        {
            //NSlog(@"API NOT CALLED");
        }

    }
    @catch (NSException *exception) {}
}
-(void) playSound
{
    NSString *bk=[NSString stringWithFormat:@"pickUpSound"];
    NSString *path = [[NSBundle mainBundle] pathForResource:bk ofType:@"mp3"];
    NSURL *url=[NSURL fileURLWithPath:path];
    pickupSound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    if(!pickupSound)
        NSLog(@"error in play sound");
    pickupSound.delegate=self;
    pickupSound.numberOfLoops=0;
    [pickupSound play];
    isSoundPlayed=YES;
}
-(CLLocationCoordinate2D) getLocation
{
    CLLocationCoordinate2D coordinate;
    if([APPDELEGATE connected])
    {
        if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager startUpdatingLocation];
            [locationManager setAllowsBackgroundLocationUpdates:YES];
            locationManager.headingFilter=kCLHeadingFilterNone;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            locationManager.distanceFilter =5.0f;
            #ifdef __IPHONE_8_0
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8"))
            {
                [locationManager requestAlwaysAuthorization];
            }
            #endif
            CLLocation *location = [locationManager location];
            if (location)
            {
                coordinate = [location coordinate];
            }
            else
            {
                
            }
            return coordinate;
        }
        else
        {
            [[UtilityClass sharedObject]displayAlertWithTitle:@"" andMessage:@"Please Enable location access from Setting -> Taxinow Driver -> Privacy -> Location services"];
        }
    }
        return coordinate;
}
-(void)checkPush
{
    if (checkCancelTrip)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UtilityClass sharedObject]displayAlertWithTitle:@"Trip Cancel" andMessage:@"ok"];
          [self performSelectorOnMainThread:@selector(stopTripUpdateLocationTimer) withObject:nil waitUntilDone:YES];
            IS_TRIP_EXSIST=NO;
            [_viewForUser setHidden:YES];
            [_viewForAddress setHidden:YES];
            [[User sharedObject]resetObject];
            [APPDELEGATE goToMap];});
    }
    else
    {
    [self wsGetTripStatus];
    }
}

#pragma mark- Bearing Calculation
-(void)rotateMarker:(CGFloat)degrees
{
    [_mapView animateToBearing:degrees];
    [_mapView animateToLocation:current_coordinate];
    [_mapView animateToViewingAngle:45];
}

#pragma mark-END TRIP LOGIC
-(void) clickTwiceToEndTrip
{
    if (isDoublePressed)
    {
        [self wsSetTripStatus];
        return;
    }
    isDoublePressed = true;
    [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_CLICK_TWICE_TO_END_TRIP", nil)];
  [self performSelector:@selector(run) withObject:self afterDelay:2.0 ];
}
-(void) run
{
    isDoublePressed = false;
}
#pragma mark-PATH DRAWING
-(void)showRouteFromSourceToDestination:(CLLocationCoordinate2D)source to:(CLLocationCoordinate2D)destination
{
    if (PREF.isPathDraw)
    {
        NSString* saddr = [NSString stringWithFormat:@"%f,%f", source.latitude, source.longitude];
        NSString* daddr = [NSString stringWithFormat:@"%f,%f", destination.latitude, destination.longitude];
        NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GoogleServerKey];
        NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
        NSError* error = nil;
        NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
        NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:json options:0 error:&err];
        NSString * myGooglePath = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        [self setGooglePathOnServer:myGooglePath];
        
        NSArray *routes = json[@"routes"];
        
        if(routes.count!=0)
        {
            googlePath =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
            [self drawPath:googlePath withColor:[UIColor googlePathColor]];
            
        }
    }
}
-(void)setGooglePathOnServer:(NSString*)path
{
    NSMutableDictionary *dictParam;
    dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
    if ([User sharedObject].providerStatus==ProviderStatusTripStarted||[User sharedObject].providerStatus ==ProviderStatusArrived)
    {
        [dictParam setObject:path forKey:PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION];
        [dictParam setObject:@"" forKey:PARAM_GET_GOOGLE_MAP_PATH_START_LOCATION_TO_PICKUP_LOCATION];
    }
    else
    {
        [dictParam setObject:@"" forKey:PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION];
        [dictParam setObject:path forKey:PARAM_GET_GOOGLE_MAP_PATH_START_LOCATION_TO_PICKUP_LOCATION];
    }
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_SET_GOOGLE_MAP_PATH withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
    {
        NSDictionary *dictResponse=[[NSDictionary alloc]init];
            
            if ([[Parser sharedObject] stringToJson:response To:&dictResponse])
            {   if ([[dictResponse valueForKey:SUCCESS]boolValue])
                {
                }
            }
    }];
}
-(void)getGooglePathFromServer
{
    if(isPathDrawn && PREF.isPathDraw)
    {
        NSMutableDictionary *dictParam;
        dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
        isPathDrawn=false;
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:WS_GET_GOOGLE_MAP_PATH withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
         {
         dispatch_async(dispatch_get_main_queue(), ^{
             NSDictionary *dictResponse=[[NSDictionary alloc]init];
             if ([[Parser sharedObject] stringToJson:response To:&dictResponse])
             {
                 if ([[dictResponse valueForKey:SUCCESS]boolValue])
                 {
                     
                     NSString *path;
                     NSDictionary *dict=[dictResponse valueForKey:PARAM_TRIP_LOCATION];
                     if ([User sharedObject].providerStatus==ProviderStatusTripStarted||[User sharedObject].providerStatus==ProviderStatusArrived)
                     {
                         path=[dict objectForKey:PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION];
                         if ([User sharedObject].providerStatus==ProviderStatusTripStarted && PREF.isPathDraw) {
                             NSArray *startToEndTripLocation=[dict valueForKey:PARAM_START_TRIP_TO_END_TRIP_LOCATION];
                             GMSMutablePath *tempPath=[[GMSMutablePath alloc]init];
                             for (NSArray *location in startToEndTripLocation)
                             {
                                 CLLocationCoordinate2D currentCoordinate=
                                 CLLocationCoordinate2DMake(
                                                            [[location objectAtIndex:0] doubleValue],
                                                            [[location objectAtIndex:1] doubleValue]);
                                 [tempPath addCoordinate:currentCoordinate];
                                 
                                 
                             }
                             if (!isCurrentPathDraw)
                             {
                                 polyLineCurrentPath = [GMSPolyline polylineWithPath:tempPath];
                                 polyLineCurrentPath.strokeWidth = 5.f;
                                 polyLineCurrentPath.strokeColor=[UIColor currentPathColor];
                                 polyLineCurrentPath.geodesic = YES;
                                 polyLineCurrentPath.map = _mapView;
                                 isCurrentPathDraw=TRUE;
                             }
                             
                        }
                     }
                     else
                     {
                         path=[dict objectForKey:PARAM_GET_GOOGLE_MAP_PATH_START_LOCATION_TO_PICKUP_LOCATION];
                     }
                     NSError * err;
                     NSData *data =[path dataUsingEncoding:NSUTF8StringEncoding];
                     NSDictionary *json;
                     if(data!=nil && ![UtilityClass isEmpty:path])
                     {
                         json = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
                         NSArray *routes = json[@"routes"];
                         
                         if(routes.count!=0)
                         {
                             
                             googlePath =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                             [self drawPath:googlePath withColor:[UIColor googlePathColor]];
                         }
                     }
                     else
                     {
                         if ([User sharedObject].providerStatus==ProviderStatusTripStarted || [User sharedObject].providerStatus==ProviderStatusArrived)
                         {
                             if (dest_coordinate.latitude && dest_coordinate.longitude)
                             {
                                 [self showRouteFromSourceToDestination:source_coordinate to:dest_coordinate];
                             }
                             
                         }
                         else
                         {
                             [self showRouteFromSourceToDestination:current_coordinate to:source_coordinate];
                         }
                     }
                 }
             }
         });

     }];
    }
}
-(void)drawPath:(GMSPath*)path withColor:(UIColor*)color
{
        polyLineGooglePath.map=nil;
        polyLineGooglePath = [GMSPolyline polylineWithPath:path];
        polyLineGooglePath.strokeWidth = 5.f;
        polyLineGooglePath.strokeColor=[UIColor googlePathColor];
        polyLineGooglePath.geodesic = YES;
        if (polyLineGooglePath.map)
        {
            polyLineGooglePath.map=nil;
        }
            polyLineGooglePath.map=_mapView;
}

-(void)getAddressForLocation:(CLLocationCoordinate2D)location
{
    [APPDELEGATE showLoadingWithTitle:@""];
    NSString *strAddress=@"";
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f", location.latitude, location.longitude];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:NULL];
    
    if (result)
    {
        NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if([[jsonResponse valueForKey:GOOGLE_PARAM_STATUS]isEqualToString:GOOGLE_PARAM_STATUS_OK])
        {
            NSArray *arr=[jsonResponse valueForKey:GOOGLE_PARAM_RESULTS];
            NSDictionary *dict=[arr objectAtIndex:0];
            strAddress=[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_FORMATTED_ADDRESS];
        }
        
    }
    [User sharedObject].destAddress=strAddress;
    [self wsCompleteTrip];
}
-(void)drawCurrentPath
{
    if (isCurrentPathDraw && PREF.isPathDraw && [User sharedObject].providerStatus == ProviderStatusTripStarted)
    {
        polyLineCurrentPath = [GMSPolyline polylineWithPath:currentPath];
        polyLineCurrentPath.strokeWidth = 5.f;
        polyLineCurrentPath.strokeColor=[UIColor currentPathColor];
        polyLineCurrentPath.geodesic = YES;
        polyLineCurrentPath.map = _mapView;
    }
}

-(void)waitingCounter
{
    if([User sharedObject].providerStatus  ==ProviderStatusArrived)
    {
        currentWaitingTime++;
        if (currentWaitingTime<0)
        {
            [_lblWaitingTimeTitle setText:NSLocalizedString(@"WAIT_TIME_START_AFTER", nil)];
        }
        else
        {
            [_lblWaitingTimeTitle setText:NSLocalizedString(@"WAIT_TIME_START", nil)];
        }
        [_lblWaitingTimeValue setText:[NSString stringWithFormat:@"%li s",(long)currentWaitingTime]];
        [_viewForWaitingTime setHidden:NO];
        [self.view bringSubviewToFront:_viewForWaitingTime ];
    }
    else
    {
        
            [self performSelectorOnMainThread:@selector(stopWaitingTimer) withObject:nil waitUntilDone:YES];
        [_viewForWaitingTime setHidden:YES];
    }
}


#pragma mark-Timers
-(void)startTripUpdateLocationTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![_timerForTripUpdateLocation isValid])
        {
            _timerForTripUpdateLocation=[NSTimer scheduledTimerWithTimeInterval:10.0f
                                                                         target:self
                                                                       selector:@selector(wsUpdateLocation)
                                                                       userInfo:nil
                                                                        repeats:YES];
            
            [[NSRunLoop currentRunLoop] addTimer:_timerForTripUpdateLocation forMode:NSRunLoopCommonModes];        }
    });
}
-(void)stopTripUpdateLocationTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([_timerForTripUpdateLocation isValid])
        {
            [_timerForTripUpdateLocation invalidate];
            _timerForTripUpdateLocation = nil;
        }});
}

-(void)stopWaitingTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        if ([_timerForWaitingTime isValid ])
        {
            [_timerForWaitingTime invalidate];
            _timerForWaitingTime = nil;
            
        }
    });
}


- (IBAction)onClickViewNote:(id)sender
{
	User *user = [User sharedObject];
	NSLog(@"Note = %@",user.note);
	
	UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",user.note] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	alert.tag = 3434;
	[alert show];
}
@end
