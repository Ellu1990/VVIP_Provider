//
//  DocumentUpload.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 16/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertWithTitle.h"
@interface DocumentUpload : UIViewController
<UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
CustomAlertDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *tblForDocument;
@property(strong,nonatomic)NSMutableArray *arrForDocuments;
@property (assign,nonatomic) BOOL parentIsRegister;
- (IBAction)onClickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblTakePictureTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;

@end
