//
//  invoiceCell.h
//  TaxiAnytimeAnywhereProvider
//
//  Created by Elluminati Mini Mac 5 on 20/09/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface invoiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Title;
@property (weak, nonatomic) IBOutlet UILabel *Value;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
-(void)setCellData:(NSDictionary*)dict;
@property (weak, nonatomic) IBOutlet UIView *viewForCell;
@end
