//
//  CustomAlertWithTitle.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "CustomVisitorTypeDialog.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
@implementation CustomVisitorTypeDialog
- (instancetype)initWithTitle:(NSString*)title
  message:(nullable NSString *)message
  delegate:(nullable id)delegate
  typeName:(nullable NSString *)typeName
  baseFareCost:(nullable NSString *)baseFareCostValue
  minFareCost:(nullable NSString *)minFareCostValue
  distanceCost:(nullable NSString *)distanceCostValue
  cancelButtonTitle:(nullable NSString *)cancelButtonTitle
  otherButtonTitles:(nullable NSString *)otherButtonTitle
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomVisitorTypeDialog" owner:nil options:nil];
        self = [nibContents lastObject];
        self.delegate = delegate;
       [_lblTitle setTextColor:[UIColor textColor]];
        self.frame=APPDELEGATE.window.frame;
        self.alertView.center=self.center;
        self.alertView=[[UtilityClass sharedObject]addShadow:self.alertView];
        [self setLocalization];
        self.backgroundColor=[UIColor clearColor];
        [self.lblTitle setText:[title capitalizedString]];
        [self.lblMessage setText:message];
        if(typeName)
                    [_lblTypeName setText:typeName];
        [_lblBaseFareCost setText:baseFareCostValue];
        [_lblMinFareCost setText:minFareCostValue];
        [_lblDistanceCost setText:distanceCostValue];
        [_btnYes setTitle:[otherButtonTitle uppercaseString] forState:UIControlStateNormal];
        [_btnNo setTitle:[cancelButtonTitle uppercaseString] forState:UIControlStateNormal];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        return self;
    }
    return self;
}

- (IBAction)onClickBtnNo:(id)sender
{[[UtilityClass sharedObject] animateHide:self];}
-(void)setLocalization
{
    self.lblTitle.textColor=[UIColor labelTextColor];
    self.lblMessage.textColor=[UIColor textColor];
    self.btnNo.titleLabel.textColor=[UIColor buttonTextColor];
    self.btnYes.titleLabel.textColor=[UIColor buttonTextColor];
    self.btnNo.backgroundColor=[UIColor buttonColor];
    self.btnYes.backgroundColor=[UIColor buttonColor];
    self.alertView.backgroundColor=[UIColor whiteColor];
    [_lblBaseFare setText:NSLocalizedString(@"BASE_FARE", nil)];
    [_lblMinFare setText:NSLocalizedString(@"MIN_FARE", nil)];
    [_lblDistance setText:NSLocalizedString(@"DISTANCE", nil)];
    [_lblBaseFareCost setTextColor:[UIColor textColor]];
    [_lblMinFareCost setTextColor:[UIColor textColor]];
    [_lblDistanceCost setTextColor:[UIColor textColor]];
    [_lblTypeName setTextColor:[UIColor textColor]];
    [_btnYes setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnYes setBackgroundColor:[UIColor buttonColor]];
    [_btnNo setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnNo setBackgroundColor:[UIColor buttonColor]];
    self.backgroundColor=[UIColor clearColor];
}
- (IBAction)onClickBtnYes:(id)sender {
    [[UtilityClass sharedObject] animateHide:self];
    [_delegate onClickCustomVisitorDialogOk:self];
}
- (void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;
{
    self.lblTitle.textColor=titleColor;
    self.lblMessage.textColor=messageColor;
    self.btnNo.titleLabel.textColor=cancelButtonColor;
    self.btnYes.titleLabel.textColor=otherButtonColor;
}





@end
