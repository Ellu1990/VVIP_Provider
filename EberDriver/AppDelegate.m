//
//  AppDelegate.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 12/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import "UIColor+Colors.h"
#import <GoogleMaps/GoogleMaps.h>
#import "NSObject+Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "HomeVC.h"
#import "SWRevealViewController.h"
#import "SliderVC.h"
#import "PreferenceHelper.h"
#import "HomeVC.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "Hotline.h"
#import "Parser.h"
#define push_alert @"alert"
#define push_id  @"id"
#define push_aps @"aps"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


#define PUSH_CODE_FOR_NEW_TRIP  @"201"
#define PUSH_CODE_FOR_ACCEPT_TRIP @"203"
#define PUSH_CODE_FOR_TRIP_CANCELLED_BY_USER @"205"
#define PUSH_CODE_FOR_PROVIDER_APPROVED @"207"
#define PUSH_CODE_FOR_PROVIDER_DECLINED  @"208"

#define PUSH_CODE_FOR_SET_DESTINATION  @"210"
#define PUSH_CODE_FOR_PAYMENT_MODE_CASH @"211"
#define PUSH_CODE_FOR_PAYMENT_MODE_CARD  @"212"
#define PUSH_CODE_FOR_PROVIDER_LOGIN_IN_OTHER_DEVICE @"230"


@interface AppDelegate ()
{
}
@end

@implementation AppDelegate
{
  

}
@synthesize objMap,objTrip;
#pragma mark -
#pragma mark - Life Cycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{      //do calculations
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        [FIRApp configure];
        [GIDSignIn sharedInstance].clientID = @"330619371849-4k584tavaclk3ngeerkm1f6fgrn8mgds.apps.googleusercontent.com";
        [GIDSignIn sharedInstance].delegate = self;
        [GMSServices provideAPIKey:GoogleKey];
        /*Navigation Controller*/
        [[UINavigationBar appearance] setBarTintColor:[UIColor GreenColor]];
        [[UINavigationBar appearance] setTranslucent:NO];
        [application setStatusBarHidden:NO];
        [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.window makeKeyAndVisible]; // or similar code to set a visible view
    
    /*  Set your view before the following snippet executes */
    
    /* Handle remote notifications */
    if ([[Hotline sharedInstance]isHotlineNotification:launchOptions])
    {
        [[Hotline sharedInstance]handleRemoteNotification:launchOptions
                                              andAppstate:application.applicationState];
    }
    
    /* Any other code to be executed on app launch */
    
    /* Reset badge app count if so desired */
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];/* Initialize Hotline*/
    
    /* Enable remote notifications */
    /*PUSH NOTIFICATION*/
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        
        UIUserNotificationType types = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [application registerUserNotificationSettings:mySettings];
        [application registerForRemoteNotifications];
    }

    
    [self.window makeKeyAndVisible]; // or similar code to set a visible view
    
    /*  Set your view before the following snippet executes */
    
    /* Handle remote notifications */
    if ([[Hotline sharedInstance]isHotlineNotification:launchOptions]) {
        [[Hotline sharedInstance]handleRemoteNotification:launchOptions
                                              andAppstate:application.applicationState];
    }
    
    /* Any other code to be executed on app launch */
    
    /* Reset badge app count if so desired */
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
            return YES;
}
- (void)applicationWillResignActive:(UIApplication *)application {}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    UIApplication *app = [UIApplication sharedApplication];
    UIBackgroundTaskIdentifier bgTask = 0;
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

}
- (void)applicationWillEnterForeground:(UIApplication *)application {

}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

}
- (void)applicationWillTerminate:(UIApplication *)application {}
#pragma mark -
#pragma mark - Loading View
-(void)showLoadingWithTitle:(NSString *)title
{
    if (view==nil)
    {
        view=[[UIView alloc]initWithFrame:self.window.bounds];
        [view setBackgroundColor:[UIColor clearColor]];
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(15,15,45,45)];
        img.backgroundColor=[UIColor clearColor];
        img.contentMode = UIViewContentModeScaleToFill;
        img.clipsToBounds = YES;
        img.image=[UIImage imageNamed:@"progress"];
        img.center=view.center;
        img.transform =CGAffineTransformRotate(img.transform, 1.0);
        CABasicAnimation* rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.fromValue = @(0.0);
        rotationAnimation.toValue = @(M_PI * 2.0);
        rotationAnimation.duration = 1.0;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = HUGE_VALF;
        [img.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
        [view addSubview:img];
        [self.window addSubview:view];
        [self.window bringSubviewToFront:view];
    }
}
-(void)hideLoadingView
{
    if (view)
    {
        [view removeFromSuperview];
        view=nil;
    }

}
#pragma mark -
#pragma mark - sharedAppDelegate
+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark-Orientation ManageMent
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma Mark-Google And Facebook SignIn
- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation
{
    
       return  [[GIDSignIn sharedInstance] handleURL:url
                                    sourceApplication:sourceApplication
                                           annotation:annotation];
        
   
}
#pragma mark-Google SignIN
-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
}
#pragma  Handle Push
/*
 *Register For Retrive Remote Notification
 */
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone)
    {[application registerForRemoteNotifications];}
    
}
/*
 *Register For Retrive Device Token Notification
 */
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
   
    [[Hotline sharedInstance] updateDeviceToken:deviceToken];
    NSString* devToken = [[[[deviceToken description]
                            stringByReplacingOccurrencesOfString:@"<"withString:@""]
                           stringByReplacingOccurrencesOfString:@">" withString:@""]
                          stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    if(![UtilityClass isEmpty:devToken])
    {
        DEVICE_TOKEN=devToken;
       
    }
    else
    {
        DEVICE_TOKEN=@"";
    }
    if (PREF.isProviderActive)
    {
         PREF.DeviceToken=DEVICE_TOKEN;
        [self wsRefreshToken];
    }
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"error is : %@", error);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ([[Hotline sharedInstance]isHotlineNotification:userInfo])
    {
        [[Hotline sharedInstance]handleRemoteNotification:userInfo andAppstate:[[UIApplication sharedApplication]applicationState]];
    }

    NSMutableDictionary *aps=[userInfo valueForKey:push_aps];
    NSString *msg=[aps valueForKey:push_alert];
    NSString *pushId=[msg valueForKey:push_id];
    if ([pushId isEqualToString:PUSH_CODE_FOR_NEW_TRIP])
    {
        checkNewTrip=true;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
        {
                MapVC *map=(MapVC*)nv.visibleViewController;
                [map checkPush];
        }
        else
        {
                [nv popToRootViewControllerAnimated:YES];
        }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_TRIP_CANCELLED_BY_USER])
    {
        checkCancelTrip=true;
        IS_TRIP_EXSIST=NO;
        IS_PROVIDER_ACCEPTED=NO;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc.visibleViewController;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
        {
                checkCancelTrip=true;
                MapVC *map=(MapVC*)nv.visibleViewController;
                [map checkPush];
        }
        else if ([nv.visibleViewController isKindOfClass:[TripVC class]])
        {
                TripVC *trip=(TripVC*)nv.visibleViewController;
                [trip.timerForTripUpdateLocation invalidate];
                trip.timerForTripUpdateLocation=nil;
                [trip.viewForUser setHidden:YES];
                [trip.viewForAddress setHidden:YES];
                [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
                [trip checkPush];
        }
        else
        {
                [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
                [self goToMap];
        }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_PROVIDER_APPROVED])
    {
        checkApprove=true;
        PREF.ProviderApproved=YES;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
        {
                MapVC *map=(MapVC*)nv.visibleViewController;
                [map checkPush];
        }
        else
        {
            [self goToMap];
        }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_PROVIDER_DECLINED])
    {
        checkDecline=true;
        PREF.ProviderApproved=NO;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
            {
                MapVC *map=(MapVC*)nv.visibleViewController;
                [map checkPush];
            }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_SET_DESTINATION] ||[pushId isEqualToString:PUSH_CODE_FOR_PAYMENT_MODE_CARD]||[pushId isEqualToString:PUSH_CODE_FOR_PAYMENT_MODE_CASH] )
    {
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[TripVC class]])
        {
                TripVC *trip=(TripVC*)nv.visibleViewController;
                [trip checkPush];
        }
        
    }
    else if([pushId isEqualToString:PUSH_CODE_FOR_PROVIDER_LOGIN_IN_OTHER_DEVICE])
    {
        PREF.ProviderLogin=NO;
        [self goToMain];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
    }
    
}
-(void)handleRemoteNitification:(UIApplication *)application userInfo:(NSDictionary *)userInfo
{
    if ([[Hotline sharedInstance]isHotlineNotification:userInfo])
    {
        [[Hotline sharedInstance]handleRemoteNotification:userInfo andAppstate:[[UIApplication sharedApplication]applicationState]];
    }
    
    NSMutableDictionary *aps=[userInfo valueForKey:push_aps];
    NSString *msg=[aps valueForKey:push_alert];
    NSString *pushId=[msg valueForKey:push_id];
    if ([pushId isEqualToString:PUSH_CODE_FOR_NEW_TRIP])
    {
        checkNewTrip=true;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
        {
            MapVC *map=(MapVC*)nv.visibleViewController;
            [map checkPush];
        }
        else
        {
            [nv popToRootViewControllerAnimated:YES];
        }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_TRIP_CANCELLED_BY_USER])
    {
        checkCancelTrip=true;
        IS_TRIP_EXSIST=NO;
        IS_PROVIDER_ACCEPTED=NO;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc.visibleViewController;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
        {
            checkCancelTrip=true;
            MapVC *map=(MapVC*)nv.visibleViewController;
            [map checkPush];
        }
        else if ([nv.visibleViewController isKindOfClass:[TripVC class]])
        {
            TripVC *trip=(TripVC*)nv.visibleViewController;
            [trip.timerForTripUpdateLocation invalidate];
            trip.timerForTripUpdateLocation=nil;
            [trip.viewForUser setHidden:YES];
            [trip.viewForAddress setHidden:YES];
            [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
            [trip checkPush];
        }
        else
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
            [self goToMap];
        }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_PROVIDER_APPROVED])
    {
        checkApprove=true;
        PREF.ProviderApproved=YES;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
        {
            MapVC *map=(MapVC*)nv.visibleViewController;
            [map checkPush];
        }
        else
        {
            [self goToMap];
        }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_PROVIDER_DECLINED])
    {
        checkDecline=true;
        PREF.ProviderApproved=NO;
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVC class]])
        {
            MapVC *map=(MapVC*)nv.visibleViewController;
            [map checkPush];
        }
    }
    else if ([pushId isEqualToString:PUSH_CODE_FOR_SET_DESTINATION] ||[pushId isEqualToString:PUSH_CODE_FOR_PAYMENT_MODE_CARD]||[pushId isEqualToString:PUSH_CODE_FOR_PAYMENT_MODE_CASH] )
    {
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[TripVC class]])
        {
            TripVC *trip=(TripVC*)nv.visibleViewController;
            [trip checkPush];
        }
        
    }
    else if([pushId isEqualToString:PUSH_CODE_FOR_PROVIDER_LOGIN_IN_OTHER_DEVICE])
    {
        PREF.ProviderLogin=NO;
        [self goToMain];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
    }
}

/*
 * Check Weather Given Controller is Allready  in Stack or Not.
 */
-(BOOL)isControllerAlreadyOnNavigationControllerStack:(id) myVc
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nv = [sb instantiateViewControllerWithIdentifier:@"sw"];
    self.window.rootViewController = nv;
    [self.window makeKeyAndVisible];
    UINavigationController *nav=(UINavigationController*)self.window.rootViewController;
    
    for (UIViewController *vc in nav.viewControllers)
    {
        if ([vc isKindOfClass:[SWRevealViewController class]])
        {
            SWRevealViewController *temp =(SWRevealViewController*)vc;
            if (temp.frontViewController)
            {
                UINavigationController *nv=(UINavigationController*)temp.frontViewController;
                for (UIViewController *v in nv.viewControllers)
                {
                    if ([v isKindOfClass:[myVc class]])
                    {
                        [nv popToRootViewControllerAnimated:YES];
                        return YES;
                    }
                }
                [nv pushViewController:myVc animated:YES];
                return YES;
            }
           
        }
    }
    return NO;
}
- (void)goToMain
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nv = [sb instantiateInitialViewController];
    self.window.rootViewController = nv;
    [self.window makeKeyAndVisible];
}
-(void)goToMap
{
    HotlineUser *user=[HotlineUser sharedInstance];
    user.name=[NSString stringWithFormat:@"%@ %@",PREF.providerFirstName,PREF.providerLastName];
    user.email=[NSString stringWithFormat:@"%@",PREF.providerEmail];
    user.phoneNumber=[NSString stringWithFormat:@"%@",PREF.providerPhone];
    user.phoneCountryCode=[NSString stringWithFormat:@"%@",PREF.providerCountryCode];
    user.externalID=PREF.providerFirstName;
    [[Hotline sharedInstance] updateUser:user];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nv = [sb instantiateViewControllerWithIdentifier:@"sw"];
    self.window.rootViewController = nv;
    [self.window makeKeyAndVisible];
}
-(void)wsRefreshToken
{
      NSString *providerId,*providertoken,*deviceToken;
      if (PREF.providerId && PREF.providerToken && PREF.deviceToken)
      {
      providerId=PREF.providerId;
      providertoken=PREF.providerToken;
      deviceToken=PREF.deviceToken;
      
      NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
      [dictparam setObject:providerId forKey:PARAM_PROVIDER_ID];
      [dictparam setObject:providertoken forKey:PARAM_PROVIDER_TOKEN];
      [dictparam setObject:deviceToken forKey:PARAM_DEVICE_TOKEN];
      AFNHelper *afn=[[AFNHelper alloc]init];
      NSString *strUrl=[NSString stringWithFormat:@"%@%@",PROVIDER_REFRESH_TOKEN,PREF.providerId];
      [afn getDataFromUrl:strUrl withParamData:dictparam withMethod:put withBlock:^(id response, NSError *error)
       {
           
       }];
  }
}
#pragma mark -
#pragma mark - Directory Path Methods

- (NSString *)applicationCacheDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return cacheDirectory;
}
/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
