//
//  HomeVC.h
//  
//
//  Created by Elluminati Mini Mac 5 on 12/08/16.
//
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackGround;
@property (weak, nonatomic) IBOutlet UILabel *lblCopyrighttext;

@end
