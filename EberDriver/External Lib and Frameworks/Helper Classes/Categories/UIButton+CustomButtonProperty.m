//
//  UIButton+CustomButtonProperty.m
//  Eber Driver
//
//  Created by My Mac on 6/16/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "UIButton+CustomButtonProperty.h"
#import "Aspects.h"

@implementation UIButton (CustomButtonProperty)

+ (void)load {
	[[self class]aspect_hookSelector:@selector(awakeFromNib) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo)
{
		/*UIButton* instance = [aspectInfo instance];
		UILabel* label = instance.titleLabel;
		UIFont* font = [UIFont fontWithName:@"Roboto-Regular_0" size:label.font.pointSize];
		instance.titleLabel.font = font;
        instance.titleLabel.numberOfLines = 1;
        instance.titleLabel.adjustsFontSizeToFitWidth = YES;
        instance.titleLabel.lineBreakMode = NSLineBreakByClipping;*/
		}error:nil];
}

@end
