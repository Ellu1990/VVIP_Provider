//
//  AFNHelper.h
//  Tinder
//
//  Created by Elluminati - macbook on 04/04/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomAlertWithTitle.h"

static NSString * const post = @"POST";
static NSString * const put = @"PUT";
static NSString * const get = @"GET";/*Use Del Because Delete is Reserved Key word */
static NSString * const del = @"DELETE";

typedef void (^RequestCompletionBlock)(id response, NSError *error);

@interface AFNHelper : NSObject<CustomAlertDelegate>
{
        RequestCompletionBlock dataBlock;
}
-(void)getDataFromUrl:(NSString *)path withParamData:(NSMutableDictionary *)postData  withMethod:(NSString*)method withBlock:(RequestCompletionBlock)block;
@property (nonatomic,strong)CustomAlertWithTitle *networkDialog;

@end
