//
//  RegisterVc.m
//  Eber Driver
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "RegisterVc.h"
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "UITextField+overtextfield.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "CountryCodeCell.h"
#import "Parser.h"
#import "PreferenceHelper.h"
#import "DocumentUpload.h"
#import "GoogleUtility.h"
#import "UIImageView+image.h"
#define URLEMail @"mailto:er.jaydeepvyas@gmail.com?subject=title&body=content"
#define MAX_NUMBER 10
@interface RegisterVc ()
{
    BOOL searching;
    NSString *strSelectedCountryCode;
    NSMutableArray *tempArrForCountry,*arrForCity,*userDefaultLanguages;;
    UITapGestureRecognizer *singleTap,*tapGesture;
    CustomAlertWithTitle *dilogForRegister;
    CustomOtpDialog *optDialog;
    
}
@end

@implementation RegisterVc
@synthesize activeTextField,strForID,strForRegistrationType,strForSocialId,strForToken,strImageData,arrForCountry,isPicAdded,dictParam,strForCity,strForCountry,locationManager;

#pragma mark
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
	[super viewDidLoad];
    [self getServiceCountry];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    [_scrollView addGestureRecognizer:tapGesture];
    [_btnSelectCountryName addTarget:self action:@selector(openDialogCountry) forControlEvents:UIControlEventTouchUpInside];
    [_btnSelectCity addTarget:self action:@selector(openDialogCity) forControlEvents:UIControlEventTouchUpInside];
    arrForCity=[[NSMutableArray alloc]init];
    arrForCountry=[[NSMutableArray alloc]init];
	dictParam=[[NSMutableDictionary alloc]init];
    tempArrForCountry=[[NSMutableArray alloc]init];
    strForRegistrationType=MANUAL;
	strForSocialId=@"";
    [_btnSelectCity setTitleColor:[UIColor labelTextColor] forState:UIControlStateNormal];
    [_btnSelectCountry setTitleColor:[UIColor labelTextColor] forState:UIControlStateNormal];
    [_btnSelectCountryName setTitleColor:[UIColor labelTextColor] forState:UIControlStateNormal];
    [_btnSelectCity setTitle:NSLocalizedString(@"SELECT_CITY", nil) forState:UIControlStateNormal];
    [_btnSelectCountryName setTitle:NSLocalizedString(@"SELECT_COUNTRY", nil) forState:UIControlStateNormal];
    strForCountry=NSLocalizedString(@"SELECT_COUNTRY", nil);
    strForCity=NSLocalizedString(@"SELECT_CITY", nil);
    strSelectedCountryCode=@"+93";
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    _btnTerm.titleLabel.numberOfLines = 1;
    _btnTerm.titleLabel.adjustsFontSizeToFitWidth = YES;
   	[self getLocation];
	[self SetLocalization];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	
	[[self navigationController] setNavigationBarHidden:YES animated:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
-(void)dealloc
{
}
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    [[UtilityClass sharedObject] animateHide:view];
    NSString *goToEmailApp=[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",PREF.contactEmail,@"title",@"Message"];
    NSString *url = [goToEmailApp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
}
#pragma mark
#pragma mark - Action Methods
-(IBAction)onClickBtnRegister:(id)sender
{
    if([self checkValidation])
    {
        [self buildParameter];
        if (PREF.isEmailOtpOn || PREF.isSmsOtpOn)
        {
            [self wsGetOtp];
        }
        else
        {
            [self wsRegister];
        }
    }

}
-(IBAction)onClickBtnGoogle:(id)sender
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_GOOGLE_INFO", nil)];
    [[GoogleUtility sharedObject]signInGoogle:^(BOOL success, GIDGoogleUser *user, NSError *error) {
        if (success)
        {
            _txtPassword.userInteractionEnabled=NO;
            strForSocialId=user.userID;
            _txtEmail.text=user.profile.email;
             NSArray *strname = [user.profile.name componentsSeparatedByString:@" "];
            _txtFirstName.text=[strname objectAtIndex:0];
            _txtLastName.text=[strname objectAtIndex:1];
            strForRegistrationType=GOOGLE;
            if (user.profile.hasImage)
            {
                _imgProPic.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[user.profile imageURLWithDimension:100]]]?:[UIImage imageNamed:PARAM_USER];
            }
            [[NSUserDefaults  standardUserDefaults] setBool:YES forKey:GOOGLE];
        }
        [APPDELEGATE hideLoadingView];
    } withParent:self];
    


}
-(void)onClickBtnFb:(id)sender
{
	//Set Preference Login With Facebook
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:GOOGLE];
	
	if ([APPDELEGATE connected])
	{
		strForRegistrationType=FACEBOOK;
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
		[login prepareForInterfaceBuilder];
		[login 	logInWithReadPermissions: @[PARAM_FB_PUBLIC_PROFILE, PARAM_FB_EMAIL, PARAM_FB_USER_FRIENDS]
		handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
		 {
				if (error)
				{
					NSLog(@"Process error");
				}
				else if (result.isCancelled)
				{
					NSLog(@"Cancelled");
				}
				else
				{
					NSLog(@"Logged in");
					[APPDELEGATE  showLoadingWithTitle:NSLocalizedString(@"LOADING_FACEBOOK_INFO", nil) ];
					
					if ([FBSDKAccessToken currentAccessToken])
						{
										FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
										initWithGraphPath:@"me"
										parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
										HTTPMethod:@"GET"];
							
										[request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
											   id result,
											   NSError *error)
										 {
												// Handle the result
												[APPDELEGATE hideLoadingView];
												_txtPassword.userInteractionEnabled=NO;
												strForSocialId=[result valueForKey:PARAM_FB_ID];
												_txtEmail.text=[result valueForKey:PARAM_FB_EMAIL
                                                                ];
												NSArray *arr=[[result valueForKey:@"name"] componentsSeparatedByString:@" "];
												_txtFirstName.text=[arr objectAtIndex:0];
												_txtLastName.text=[arr objectAtIndex:1];
												NSURL *pictureURL = [NSURL URLWithString:[[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"]];
												UIImage *img=[UIImage imageWithData:[NSData dataWithContentsOfURL:pictureURL]];
											 [_imgProPic setImage:img];
											 isPicAdded=YES;
										 }];
						}
				}
		 }];
		 [login logOut];
	}
   
}

- (IBAction)onClickBtnPickerCancel:(id)sender
{
 _viewForSelectCountryCode.hidden=YES;
    searching=NO;
}

- (IBAction)onClickBtnselectCountry:(id)sender
{
   
}
- (IBAction)checkBoxBtnPressed:(id)sender
{
	UIButton *btn=(UIButton *)sender;
	if(!btn.tag )
	{
		btn.tag=1;
		[btn setImage:[UIImage imageNamed:@"cb_glossy_on"] forState:UIControlStateNormal];
		[_btnRegister setBackgroundColor:[UIColor buttonColor]];
		_btnRegister.enabled=TRUE;
        _btnRegister.alpha = 1.0;
	}
	else
	{
		btn.tag=0;
        [btn setImage:[UIImage imageNamed:@"cb_glossy_off"] forState:UIControlStateNormal];
		[_btnRegister setBackgroundColor:[UIColor buttonColor]];
		_btnRegister.enabled=FALSE;
        _btnRegister.alpha = 0.5;
        

	}

}
- (IBAction)termsBtnPressed:(id)sender
{
}

- (IBAction)onClickBtnSignIn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-TableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searching)
        return tempArrForCountry.count;
    else
        return arrForCountry.count;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    CountryCodeCell *cell = (CountryCodeCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
      cell=[[CountryCodeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableCell"];
    if (searching)
       [cell setCellData: [[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] andCountry:[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_NAME]];
    else
        [cell setCellData: [[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] andCountry:[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_NAME]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    if(searching)
    {
        [_btnSelectCountry setTitle:[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] forState:UIControlStateNormal];
        [_btnSelectCountryName setTitle:[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_NAME] forState:UIControlStateNormal];
    }
    else
    {
     [_btnSelectCountry setTitle:[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_PHONE_CODE] forState:UIControlStateNormal];
     [_btnSelectCountryName setTitle:[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PARAM_COUNTRY_NAME] forState:UIControlStateNormal];
    }
    strForCountry=[_btnSelectCountryName titleForState:UIControlStateNormal];
    [self wsGetSelectedCityInCountry:strForCountry];
    [_btnSelectCity setTitle:NSLocalizedString(@"SELECT_CITY", nil) forState:UIControlStateNormal];
    [_tblForCountryCode deselectRowAtIndexPath:indexPath animated:YES];
    [_viewForSelectCountryCode setHidden:YES];
}
#pragma  mark Search Country;
- (void)searchBar:(UISearchBar *)SearchBar textDidChange:(NSString *) searchText {
    
    [tempArrForCountry removeAllObjects];
    if (searchText.length == 0)
    {
        searching = NO;
    }
    else
    {
        searching = YES;
        for (NSDictionary *Country in arrForCountry)
        {
            NSString *countryName=[Country valueForKey:PARAM_COUNTRY_NAME];
            
            if ([countryName hasPrefix:[searchText capitalizedString]] )
            {
                [tempArrForCountry addObject:Country];
            }
        }
        
    }
    
    [_tblForCountryCode reloadData];
}

#pragma mark
#pragma mark - WEB SERVICE Methods

-(BOOL) checkValidation
{
	
       if(_txtFirstName.text.length<1 || _txtLastName.text.length<1 || _txtEmail.text.length<1 || _txtNumber.text.length<10 )
       {
		if(_txtFirstName.text.length<1)
		{
			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_FIRST_NAME", nil)];
           // [_txtFirstName becomeFirstResponder];
		}
		else if(_txtLastName.text.length<1)
		{
			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_LAST_NAME", nil)];
            //[_txtLastName becomeFirstResponder];
		}
		else if(_txtEmail.text.length<1)
		{
            
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
            
            //[_txtEmail becomeFirstResponder];
        }
		else if(_txtNumber.text.length<10)
		{
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_PHONE", nil)];
            //[_txtNumber becomeFirstResponder];
		}
        else if(_txtCarModel.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_TAXI_MODEL", nil)];
            //[_txtNumber becomeFirstResponder];
        }
        else if(_txtCarNumber.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_TAXI_NUMBER", nil)];
            //[_txtNumber becomeFirstResponder];
        }
		return false;
	}
       else
       {
        
		if([[UtilityClass sharedObject]isValidEmailAddress:_txtEmail.text])
		{
            if ([strForRegistrationType isEqualToString:MANUAL] && _txtPassword.text.length<6)
            {
                [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_PASSWORD", nil)];
                // [_txtPassword becomeFirstResponder];
                return FALSE;
            }
            if (![UtilityClass isEmpty:strForCountry])
            {
                [dictParam setValue:strForCountry forKey:PARAM_COUNTRY];
            }
            else
            {
                [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_SELECT_COUNTRY_FIRST", nil)];
                return FALSE;
            }
            if (![UtilityClass isEmpty:strForCity] && ![strForCity isEqualToString:NSLocalizedString(@"SELECT_CITY", nil)])
            {
                [dictParam setValue:strForCity forKey:PARAM_CITY];
            }
            else
            {
                [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_SELECT_CITY_FIRST", nil)];
                return FALSE;
            }

            
			return true;
        }
		
		else
		{
			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
            return false;
		}
        
       

        
	}
   
	
}


-(void)buildParameter
{
    
	[dictParam setValue:_txtEmail.text forKey:PARAM_EMAIL];
	[dictParam setValue:_txtFirstName.text forKey:PARAM_FIRST_NAME];
	[dictParam setValue:_txtLastName.text forKey:PARAM_LAST_NAME];
	[dictParam setValue:_txtNumber.text forKey:PARAM_PHONE];
	[dictParam setValue:_btnSelectCountry.titleLabel.text forKey:PARAM_COUNTRY_CODE];
	[dictParam setValue:DEVICE_TOKEN forKey:PARAM_DEVICE_TOKEN];
	[dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
	[dictParam setValue:_txtBio.text forKey:PARAM_BIO];
	[dictParam setValue:_txtAddress.text forKey:PARAM_ADDRESS];
    
    strForCountry=_btnSelectCountryName.titleLabel.text;
    strForCity=_btnSelectCity.titleLabel.text;
    if (![UtilityClass isEmpty:strForCountry])
    {
        [dictParam setValue:strForCountry forKey:PARAM_COUNTRY];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_SELECT_COUNTRY_FIRST", nil)];
        
    }
    if (![UtilityClass isEmpty:strForCity] && ![strForCity isEqualToString:NSLocalizedString(@"SELECT_CITY", nil)])
    {
        [dictParam setValue:strForCity forKey:PARAM_CITY];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_SELECT_CITY_FIRST", nil)];
        
    }
	[dictParam setValue:_txtZipCode.text forKey:PARAM_ZIPCODE];
    [dictParam setValue:_txtCarModel.text forKey:PARAM_CAR_MODEL];
    [dictParam setValue:_txtCarNumber.text forKey:PARAM_CAR_NUMBER];
    [dictParam setValue:@"" forKey:PARAM_SERVICE_TYPE_ID];
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    [dictParam setValue:tzName forKey:PARAM_DEVICE_TIMEZONE];
	
	if (!isPicAdded)
	{
		[dictParam setValue:@"" forKey:PARAM_PICTURE_DATA];
	}
	
	
	[dictParam setValue:strForRegistrationType forKey:PARAM_LOGIN_BY];
	if([strForRegistrationType isEqualToString:FACEBOOK])
	{
        [dictParam setValue:@""  forKey:PARAM_PASSWORD];
		[dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];}
	else if([strForRegistrationType isEqualToString:GOOGLE])
     {
            [dictParam setValue:@""  forKey:PARAM_PASSWORD];
            [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
        }
	else
	{
		[dictParam setValue:_txtPassword.text forKey:PARAM_PASSWORD];
		[dictParam setValue:@"" forKey:PARAM_SOCIAL_UNIQUE_ID];
	}
}
-(void) wsRegister
{
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_REGISTER", nil)];
            AFNHelper *afn=[[AFNHelper alloc]init];
            [afn getDataFromUrl:PROVIDER_REGISTER withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    [APPDELEGATE hideLoadingView];
                                    if([[Parser sharedObject]isLoginSuccess:response])
                                        [self performSegueWithIdentifier:SEGUE_TO_DOCUMENT sender:self];
                                });
                 
             }];
}
-(void)openDialogCountry
{
    [self.view endEditing:YES];
   // arrForCountry = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    [_tblForCountryCode reloadData];
    _searchbar.text=@"";
    _viewForSelectCountryCode.hidden=NO;
    _viewForSelectCountryCode=[[UtilityClass sharedObject]addShadow:_viewForSelectCountryCode];
    [_tblForCountryCode reloadData];
}
-(void)openDialogCity
{
    if ([UtilityClass isEmpty:strForCountry])
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_SELECT_CITY_FIRST", nil)];
    }
    else
    {
        if (arrForCity.count)
        {
            SimpleTableView *view=[[SimpleTableView alloc]initWithTitle:NSLocalizedString(@"SELECT_CITY", nil) dataSource:arrForCity delegate:self];
            [self.view bringSubviewToFront:view];
        }
        else
        {
            dilogForRegister=nil;
            dilogForRegister=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_NO_CITY_AVAILABLE", nil) message:NSLocalizedString(@"ALERT_MSG_NO_CITY_AVAILABLE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) otherButtonTitles:[NSLocalizedString(@"ALERT_BTN_EMAIL", nil) uppercaseString ]];
		}
    }
}
-(void)onDidSelectItem:(NSString *)item
{
    [_btnSelectCity setTitle:item forState:UIControlStateNormal];
    strForCity=item;
}
#pragma mark    
#pragma mark - Text Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 100;
    [_scrollView setContentOffset:pt animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _txtNumber)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
            
        }

        else if(_txtNumber.text.length >=10)
        {
            [_txtBio becomeFirstResponder];
            return NO;
        }
        return YES;
    }
    else
    {
        return  YES;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if(textField==_txtFirstName)
    {  [_txtLastName becomeFirstResponder];textField.text = [textField.text capitalizedString];
    }
	else if(textField==_txtLastName)
    {
        [_txtEmail becomeFirstResponder]; textField.text = [textField.text capitalizedString];
    }
	else if(textField==_txtEmail)
    {[_txtPassword becomeFirstResponder];
    }
    else if(textField==_txtPassword){
        [_txtNumber becomeFirstResponder];}
    else if(textField==_txtNumber){
	       [_txtBio becomeFirstResponder];}
    else if(textField==_txtBio){
	    [_txtAddress becomeFirstResponder];
        textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtAddress)
    {   [_txtZipCode becomeFirstResponder];
        textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtZipCode)
    {   [_txtCarModel becomeFirstResponder];
        
    }
    else if(textField==_txtCarModel)
    {   [_txtCarNumber becomeFirstResponder];
    }
    else if(textField==_txtCarNumber)
    {
		[textField resignFirstResponder];
        if (_btnRegister.enabled)
        {
            [self onClickBtnRegister:nil];
        }
        else
        {
            
        }
    }
    else
    {
       	[textField resignFirstResponder];
        if (_btnRegister.enabled)
        {
            [self onClickBtnRegister:nil];
        }
        else
        {
            
        }
    }
    return YES;
}
- (NSString *)encodeToBase64String:(UIImage *)image {
	NSData *data=UIImageJPEGRepresentation(image, 100);
	NSString *str=[data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
	return str;
}

-(void)SetLocalization
{
    /*textView setUp*/
    _txtFirstName.placeholder=NSLocalizedString(@"FIRST_NAME", nil);
    _txtLastName.placeholder=NSLocalizedString(@"LAST_NAME", nil);
    _txtEmail.placeholder=NSLocalizedString(@"EMAIL", nil);
    _txtPassword.placeholder=NSLocalizedString(@"PASSWORD", nil);
    _txtBio.placeholder=NSLocalizedString(@"BIO", nil);
    _txtAddress.placeholder=NSLocalizedString(@"ADDRESS", nil);
    _txtZipCode.placeholder=NSLocalizedString(@"ZIPCODE", nil);
    _txtCarModel.placeholder=NSLocalizedString(@"TAXI_MODEL", nil);
    _txtCarNumber.placeholder=NSLocalizedString(@"TAXI_NUMBER", nil);
    _txtNumber.placeholder=NSLocalizedString(@"NUMBER", nil);
    
    [_txtEmail setTextColor:[UIColor textColor]];
    [_txtFirstName setTextColor:[UIColor textColor]];
    [_txtLastName setTextColor:[UIColor textColor]];
    [_txtPassword setTextColor:[UIColor textColor]];
    [_txtAddress setTextColor:[UIColor textColor]];
    [_txtBio setTextColor:[UIColor textColor]];
    [_txtZipCode setTextColor:[UIColor textColor]];
    [_txtEmail setTextColor:[UIColor textColor]];
    [_txtNumber setTextColor:[UIColor textColor]];
    [_txtCarModel setTextColor:[UIColor textColor]];
    [_txtCarNumber setTextColor:[UIColor textColor]];
    
    [_txtEmail setBorder];
    [_txtFirstName setBorder];
    [_txtLastName setBorder];
    [_txtPassword setBorder];
    [_txtAddress setBorder];
    [_txtBio setBorder];
    [_txtZipCode setBorder];
    [_txtEmail setBorder];
    [_txtNumber setBorder];
    [_txtCarModel setBorder];
    [_txtCarNumber setBorder];

    /*SET TITLE*/
    [_lblSelectCountry setTextColor:[UIColor GreenColor]];

    _titleLabel.text=[NSLocalizedString(@"REGISTER", nil) capitalizedString];
    _titleLabel.textColor=[UIColor labelTitleColor];
    _lblAllReadyHaveAccount.text=NSLocalizedString(@"ALREADY_HAVE_ACCOUNT?", nil);
    /*VIEW CUTTING AND VISIBILITY*/
    [_imgProPic setRoundImageWithColor:[UIColor borderColor]];
    [_viewForNavigationBar setBackgroundColor:[UIColor GreenColor]];
    
	[_scrollView setContentSize:CGSizeMake(0, _btnCheckBox.frame.origin.y+100)];
    CGPoint offset=CGPointMake(0, 10);
    [_scrollView setContentOffset:offset animated:YES];
	_viewForSelectCountryCode.hidden=YES;
	singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
	[_imgProPic setUserInteractionEnabled:YES];
	[_imgProPic addGestureRecognizer:singleTap];
    [_imgProPic setUserInteractionEnabled:YES];
    
    UIFont *font=[UIFont fontWithName:@"Roboto-Bold" size: _btnSignIn.titleLabel.font.pointSize];
    [_lblSelectCountry setFont:font];
    [_lblSelectCountry setTextColor:[UIColor GreenColor]];
    [_lblSelectCountry setText:NSLocalizedString(@"SELECT_COUNTRY", nil)];
    

    [_scrollView setShowsVerticalScrollIndicator:NO];
    [_viewForRegister setBackgroundColor:[UIColor backGroundColor]];

    /*button setUp*/
    [_btnSignIn setTitleColor:[UIColor labelTextColor] forState:UIControlStateNormal];
    [_btnSignIn.titleLabel setFont:font];
    [_btnSignIn setTitle:[NSLocalizedString(@"LOGIN", nil) capitalizedString] forState:UIControlStateNormal];
    
    [_btnRegister setAlpha:0.5];
    [_btnRegister setTitle:[NSLocalizedString(@"REGISTER", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnRegister setBackgroundColor:[UIColor buttonColor]];
    _btnRegister.layer.shadowOpacity=0.8;
    _btnRegister.layer.shadowOffset= CGSizeMake(0, 3.0f);
    _btnRegister.layer.shadowColor = [UIColor blackColor].CGColor;
    
    [_btnFb setHidden:YES];
    [_btnFb setBackgroundColor:[UIColor clearColor]];
    [_btnFb setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    
    [_btnCheckBox setImage:[UIImage imageNamed:@"cb_glossy_off"] forState:UIControlStateNormal];
    [_btnRegister setEnabled:false];
    
    [[_btnSelectCountry layer] setBorderWidth:2.0f];
    [[_btnSelectCountry layer] setBorderColor:[UIColor borderColor].CGColor];
   
    [_btnTerm setTitle:NSLocalizedString(@"I AGREE TO THE TERMS AND CONDITION", nil) forState:UIControlStateNormal];
}
-(void)tapDetected
{
    [self.view endEditing:YES];
    SelectImage *view=[SelectImage getSelectImageViewwithParent:self];
    [view bringSubviewToFront:self.parentViewController.view];
}


#pragma Location Related Methods
-(void) getCountryAndCity:(CLLocationCoordinate2D )location2d{
   CLLocation *location = [[CLLocation alloc] initWithLatitude:location2d.latitude longitude:location2d.longitude];
    userDefaultLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en_US", nil] forKey:@"AppleLanguages"];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
   [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *array, NSError *error){
                       if (error)
                       {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       CLPlacemark *placemark = [array objectAtIndex:0];
                       if (placemark.locality)
                       {
                           strForCity=placemark.locality;
                       }
                       else
                       {
                           if(placemark.administrativeArea)
                               strForCity=placemark.administrativeArea;
                           else
                               strForCity=placemark.subAdministrativeArea;
                       }
                       strForCountry=placemark.country;
                       [[NSUserDefaults standardUserDefaults] setObject:userDefaultLanguages forKey:@"AppleLanguages"];
                       
                       if (strForCountry==nil || strForCity == nil)
                       {
                            strForCity=NSLocalizedString(@"SELECT_CITY", nil);
                            strForCountry=@"";
                       }
                       else
                       {
                           [_btnSelectCountryName setTitle:strForCountry forState:UIControlStateNormal];
                           [_btnSelectCity setTitle:strForCity forState:UIControlStateNormal];
                           [self wsGetSelectedCityInCountry:strForCountry];
                       }
                   }];
}
-(void) getLocation{
    CLLocationCoordinate2D coordinate;
    if ([APPDELEGATE connected])
    {
        if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager startUpdatingLocation];
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            #ifdef __IPHONE_8_0
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8"))
                {[locationManager requestAlwaysAuthorization];}
            #endif
            CLLocation *location = [locationManager location];
            if (location)
            {
            coordinate = [location coordinate];
            currentLocation=coordinate;
            [self getCountryAndCity:coordinate];
            }
            
        }
        else
        {
        [[UtilityClass sharedObject]displayAlertWithTitle:@"LOCATION SERVICE" andMessage:@"Location Service not Enable"];
            currentLocation=coordinate;
        }
    }
   
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *lo = [locations objectAtIndex:0];
    strForCurLatitude=[NSString stringWithFormat:@"%f",lo.coordinate.latitude];
    strForCurLongitude=[NSString stringWithFormat:@"%f",lo.coordinate.longitude];
    if (lo)
    {
        currentLocation=[lo coordinate];
        [locationManager stopUpdatingLocation];
        [manager stopUpdatingHeading];
    }
    
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{NSLog(@"didUpdateToLocation");}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError %@",error);
    if (!error)
    {
    [manager stopUpdatingLocation];
    }
    
}
#pragma -
#pragma mark Image picker delegate methdos
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    _imgProPic.image=image;
    isPicAdded=true;
    NSString *strimage =[self encodeToBase64String:_imgProPic.image];
    strImageData=strimage;
    [dictParam setObject:strimage forKey:PARAM_PICTURE_DATA];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark
#pragma mark - Segue Methods
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DocumentUpload *doc=[[DocumentUpload alloc]init];
    doc.parentIsRegister=YES;
    IS_DOCUMENT_PARENT_IS_REGISTER=YES;
}
-(void)wsGetSelectedCityInCountry:(NSString*)contry
{
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GET_COUNTRYAND_CITY",nil)];
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:strForCountry forKey:PARAM_COUNTRY];
        [afn getDataFromUrl:WS_GET_CITY_LIST withParamData:dict withMethod:post withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                [arrForCity removeAllObjects];
                                
                                if([[Parser sharedObject]parseCities:response toArray:&arrForCity])
                                {
                                    NSLog(@"%@",arrForCity);
                                }
                                else
                                {
                                    dilogForRegister=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_NO_CITY_AVAILABLE", nil) message:NSLocalizedString(@"ALERT_MSG_NO_COUNTRY_AVAILABLE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) otherButtonTitles:[NSLocalizedString(@"ALERT_BTN_EMAIL", nil) uppercaseString]];
                                }
                                [APPDELEGATE hideLoadingView];
                            });
             
         }];
}

-(void)onClickAddCity
{
    dilogForRegister=nil;
    dilogForRegister=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_NO_CITY_AVAILABLE", nil) message:NSLocalizedString(@"ALERT_MSG_NO_CITY_AVAILABLE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) otherButtonTitles:[NSLocalizedString(@"ALERT_BTN_EMAIL", nil) uppercaseString ]];
}
-(void)wsGetOtp
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_SENDING_OTP", nil)];
    NSMutableDictionary *dictOtpParam=[[NSMutableDictionary alloc]init];
    [dictOtpParam setObject:_txtEmail.text forKey:PARAM_EMAIL];
    [dictOtpParam setObject:_txtNumber.text forKey:PARAM_PHONE];
    [dictOtpParam setObject:_btnSelectCountry.titleLabel.text forKey:PARAM_COUNTRY_CODE];
    [dictOtpParam setObject:[NSNumber numberWithInteger:PROVIDER_TYPE ] forKey:PARAM_TYPE];
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_GET_VERIFICATION_OTP withParamData:dictOtpParam withMethod:post withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
         {
                            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                            if([[jsonResponse valueForKey:SUCCESS] boolValue])
                            {
                                NSString *emailOtp=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_EMAIL_OTP] ];
                                NSString *smsOtp=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_SMS_OTP]];
                                optDialog=[[CustomOtpDialog alloc]initWithOtpEmail:emailOtp optSms:smsOtp emailOtpOn:PREF.isEmailOtpOn smsOtpOn:PREF.isSmsOtpOn delegate:self];
                                [self.view bringSubviewToFront:optDialog];
                            }
                            else
                            {
                                [[UtilityClass sharedObject]showToast:NSLocalizedString([jsonResponse valueForKey:ERROR_CODE],nil)];
                            }
                            [[AppDelegate sharedAppDelegate] hideLoadingView];
           });
         
     }];
    
}
-(void)onClickCustomDialogOtpOk:(CustomOtpDialog *)view
{
    [self wsRegister];
}
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    if (activeTextField)
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGPoint pt;
        CGRect rc = [activeTextField bounds];
        rc = [activeTextField convertRect:rc toView:_scrollView];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= kbSize.height;
        CGRect mainRect=self.scrollView.frame;
        mainRect.size.height-=kbSize.height;
        if (!CGRectContainsRect(mainRect, rc))
        {
            [_scrollView setContentOffset:pt animated:YES];
        }
    }
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillHide:(NSNotification*)aNotification
{
    
}
- (void)hideKeyBoard
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];
}
-(void)getServiceCountry
{
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_GET_COUNTRIES withParamData:nil withMethod:@"GET" withBlock:^(id response, NSError *error)
    {
        if ([[Parser sharedObject]parseServiceCountries:response toArray:&arrForCountry])
        {
            
        }
    }];
    
}
@end
