//
//  UIColor+Colors.h
//  Eber Driver
//
//  Created by My Mac on 6/13/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Colors)


#pragma mark-ThemeColor
+(UIColor *)GreenColor;
+(UIColor *)lightGreenColor;
+(UIColor *)DarkGreenColor;
+(UIColor *)backGroundColor;
+(UIColor *)transparentGreenColor;

#pragma mark Text Color
+(UIColor *)borderColor;
+(UIColor *)textColor;
+(UIColor *)textBackColor;
+(UIColor *)dividerColor;
#pragma mark Label Color
+(UIColor *)labelTextColor;
+(UIColor *)labelTitleColor;
#pragma mark- Button Color
+(UIColor *)buttonColor;
+(UIColor *)buttonTextColor;
#pragma mark- Gradient Color
+(UIColor *) gradientColorUp;
+(UIColor *) gradientColorDown;

#pragma mark- Path Colors
+(UIColor *) googlePathColor;
+(UIColor *) currentPathColor;

@end
