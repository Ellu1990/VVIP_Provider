//
//  NSObject+Constants.h
//  Eber Driver
//
//  Created by My Mac on 6/13/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//
#import <Foundation/Foundation.h>
#define PROVIDER_TYPE 0
#define invalidTokenId @"451"
#define animationMapDelayed 3.00f
typedef NS_ENUM(NSInteger,ProviderStatus)
{
    ProviderStatusAccepted=1,
    ProviderStatusComing=2,
    ProviderStatusArrived=4,
    ProviderStatusTripStarted=6,
    ProviderStatusTripEnd=8,
    ProviderStatusTripCompleted=9,
};


@interface NSObject (Constants)
// MACROS
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define PREF [PreferenceHelper sharedObject]
#define BASE_URL @"https://www.vvip.biz/"
///#define BASE_URL @"http://192.168.0.115:5000/"

///#define BASE_URL @"http://staging.elluminatiinc.com/"
/* WEB SERVICE Local URL*/


#pragma mark-Application Information
extern NSString *const APPLICATION_NAME;
extern NSString *const COPYRIGHTS_NOTE;
extern BOOL IS_TRIP_EXSIST;
extern BOOL IS_PROVIDER_ACCEPTED;
extern BOOL IS_DOCUMENT_PARENT_IS_REGISTER;
extern NSString *const PROVIDER_PATH_DRAW;


#pragma mark-Segues
extern NSString *const SEGUE_TO_DIRCET_REGI;
extern NSString *const SEGUE_TO_MAP;
extern NSString *const SEGUE_TO_DOCUMENT;
extern NSString *const SEGUE_TO_TRIP;
extern NSString *const SEGUE_PROFILE;
extern NSString *const SEGUE_TO_DIRCET_LOGIN;
extern NSString *const SEGUE_TO_HISTORY;
extern NSString *const SEGUE_TO_HISTORY_DETAIL;
extern NSString *const SEGUE_TO_INVOICE;
extern NSString *const SEGUE_TO_FEEDBACK;
extern NSString *const SEGUE_TO_SETTINGS;
extern NSString *const SEGUE_TO_BANKDETAIL;

#pragma mark -
#pragma mark - WS METHODS

extern NSString *const WS_ADD_BANK_DETAIL;
extern NSString *const WS_GET_BANK_DETAIL;
extern NSString *const WS_UPDATE_BANK_DETAIL;

extern NSString *const WS_SET_GOOGLE_MAP_PATH;
extern NSString *const WS_GET_GOOGLE_MAP_PATH;

extern NSString *const WS_PROVIDER_FORGET_PASSWORD;
extern NSString *const WS_PROVIDER_EARNING;
extern NSString *const WS_UPDATE_PROVIDER_TYPE;
extern NSString *const WS_GET_APP_KEYS;
extern NSString *const WS_GET_SETTING_DETAILS;
extern NSString *const WS_GET_VERIFICATION_OTP;
extern NSString *const WS_CANCEL_TRIP;
extern NSString *const WS_GET_CITY_LIST;
extern NSString *const WS_GET_TYPELIST_SELETED_CITY;
extern NSString *const WS_GET_COUNTRIES;

extern NSString *const PROVIDER_REGISTER;
extern NSString *const PROVIDER_LOGIN;
extern NSString *const PROVIDER_LOGOUT;
extern NSString *const PROVIDER_UPDATE_PROFILE;
extern NSString *const PARAM_CURRENCY;
extern NSString *const PARAM_DISTANCE_UNIT;
/**Refresh Token to Receive Push Notifications*/
extern NSString *const PROVIDER_REFRESH_TOKEN;
extern NSString *const PROVIDER_DETAIL;
extern NSString *const GET_DOCUMENTS;
extern NSString *const UPLOAD_DOCUMENT;
/**Change State Online/Offline*/
extern NSString *const PROVIDER_TOGGLE_STATE;
/**Update Location to Receive Trip */
extern NSString *const PROVIDER_UPDATE_LOCATION;
extern NSString *const PROVIDER_GET_TRIP;
extern NSString *const PROVIDER_GET_TRIP_STATUS;
/**Accept or reject Trip Request*/
extern NSString *const PARAM_RESPOND_TRIP;
/*
 * Set Trip status like
 * Arrived
 * Coming ...
 */
extern NSString *const PROVIDER_SET_TRIP_STATUS;
extern NSString *const PROVIDER_COMPLETE_TRIP;
extern NSString *const PROVIDER_HISTORY;
extern NSString *const PROVIDER_TRIP_DETAIL;
extern NSString *const PROVIDER_RATE_USER;
extern NSString *const PARAM_TYPE;

#pragma mark -
#pragma mark - COMMON-PARAMETER NAME
extern NSString *const PARAM_PROVIDER_ID;
extern NSString *const PARAM_PROVIDER_TOKEN;
extern NSString *const PARAM_ID;
extern NSString *const PARAM_USER;
extern NSString *const PARAM_CITY_TYPES;
extern NSString *const PARAM_TYPE_IMAGE_URL;
extern NSString *const PARAM_IS_PARTNER_APPROVED_BY_ADMIN;
extern NSString *const PARAM_PARTNER_EMAIL;
extern NSString *const PARAM_PROVIDER_TYPE_ID;
extern NSString *const PARAM_PROVIDER_TYPE;


/*BANK PARAMS*/
extern NSString *const PARAM_BANK_DETEILS;
extern NSString *const PARAM_BANK_HOLDER_TYPE;
extern NSString *const PARAM_BANK_HOLDER_ID;
extern NSString *const PARAM_BANK_NAME;
extern NSString *const PARAM_BANK_BRANCH;
extern NSString *const PARAM_BANK_ACCOUNT_NUMBER;
extern NSString *const PARAM_BANK_ACCOUNT_HOLDER_NAME;
extern NSString *const PARAM_BANK_BENEFICIARY_ADDRESS;
extern NSString *const PARAM_BANK_UNIQUE_CODE;
extern NSString *const PARAM_BANK_SWIFT_CODE;

#pragma mark -
#pragma mark - LOGIN-REGISTER AND PROFILE-PARAMS


extern NSString *const PARAM_COUNTRY_NAME;
extern NSString *const PARAM_COUNTRY_PHONE_CODE;
extern NSString *const PARAM_BEARING;
extern NSString *const PARAM_USER_CREATE_TIME;
extern NSString *const PARAM_SOCIAL_UNIQUE_ID;
extern NSString *const PARAM_PICTURE;
extern NSString *const PARAM_FIRST_NAME;
extern NSString *const PARAM_LAST_NAME;
extern NSString *const PARAM_EMAIL;
extern NSString *const PARAM_PASSWORD;
extern NSString *const PARAM_COUNTRY_CODE;
extern NSString *const PARAM_PHONE;
extern NSString *const PARAM_BIO;
extern NSString *const PARAM_ADDRESS;
extern NSString *const PARAM_ZIPCODE;
extern NSString *const PARAM_COUNTRY;
extern NSString *const PARAM_CITY;
extern NSString *const PARAM_CITY_NAME;
extern NSString *const PARAM_OLD_PASSWORD;
extern NSString *const PARAM_NEW_PASSWORD;
extern NSString *const PARAM_SERVICE_TYPE_ID;
extern NSString *const PARAM_SERVICE_TYPE;
extern NSString *const PARAM_TOKEN;
extern NSString *const PARAM_DEVICE_TIMEZONE;
extern NSString *const PARAM_LOGIN_BY;
extern NSString *const PARAM_DEVICE_TOKEN;
extern NSString *const PARAM_DEVICE_TYPE;
extern NSString *const PARAM_PROVIDER_APPROVED;
extern NSString *const PARAM_PROVIDER_DOC_UPLOADED;
extern NSString *const PARAM_PROVIDER_IS_ACTIVE;
extern NSString *const PARAM_EMAIL_VERIFICATION_ON;
extern NSString *const PARAM_SMS_VERIFICATION_ON;
extern NSString *const PARAM_SMS_OTP;
extern NSString *const PARAM_EMAIL_OTP;
extern NSString *const PARAM_SOURCE_LOCATION;
extern NSString *const PARAM_DESTINATION_LOCATION;
extern NSString *const PARAM_PICTURE_DATA;
extern NSString *const PARAM_CONTACT_EMAIL;
extern NSString *const PARAM_IS_PROVIDER_ACCEPTED;
extern NSString *const PARAM_MAP_IMAGE;
/*Car Detail*/
extern NSString *const PARAM_CAR_MODEL;
extern NSString *const PARAM_CAR_NUMBER;
extern NSString *const PARAM_CAR_NAME;
extern NSString *const PARAM_TYPE_IMAGE;
extern NSString *const PARAM_TIME_PRICE;
extern NSString *const PARAM_DISTANCE_PRICE;
extern NSString *const PARAM_SCHEDULED_REQUEST_PRE_START_TIME;

extern NSString *const PARAM_TYPE_ID;
extern NSString *const PARAM_ADMIN_TYPE_ID;
extern NSString *const PARAM_IS_VISIT_TYPE;
extern NSString *const PARAM_MINFARE;
#pragma Mark-Document
extern NSString *const PARAM_DOCUMENT_PICTURE;
extern NSString *const PARAM_PROVIDER_DOCUMENT;
extern NSString *const PARAM_DOCUMENT_NAME;
extern NSString *const PARAM_DOCUMENT_UPLOADED;
extern NSString *const PARAM_DOCUMENT_OPTIONAL;
#pragma mark -Trip Param
extern NSString *const PARAM_TRIP_NUMBER;
extern NSString *const PARAM_TOTAL_WAIT_TIME;
extern NSString *const PARAM_IS_TRIP_COMPLETED;
extern NSString *const PARAM_IS_TRIP_CANCELLED_BY_USER;
extern NSString *const PARAM_IS_CANCELLATION_FEE;
extern NSString *const PARAM_INVOICE_NUMBER;
extern NSString *const PARAM_GET_GOOGLE_MAP_PATH_START_LOCATION_TO_PICKUP_LOCATION;
extern NSString *const PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION;
extern NSString *const PARAM_START_TRIP_TO_END_TRIP_LOCATION;
extern NSString *const PARAM_TRIP_LOCATION;
extern NSString *const PARAM_PROVIDER_SERVICE_FEES;
extern NSString *const PARAM_IS_TRIP_CANCELLED_BY_PROVIDER;
extern NSString *const PARAM_LATITUDE;
extern NSString *const PARAM_LONGITUDE;
extern NSString *const PARAM_TRIP;
extern NSString *const PARAM_TRIP_SERVICE;
extern NSString *const PARAM_TRIPS;
extern NSString *const PARAM_TRIP_ID;
extern NSString *const PARAM_TRIP_SOURCE_ADDRESS;
extern NSString *const PARAM_TRIP_DESTINATION_ADDRESS;
extern NSString *const PARAM_PROVIDER_STATUS;
extern NSString *const PARAM_TOTAL_TIME;
extern NSString *const PARAM_TRIP_CREATE_TIME;
extern NSString *const PARAM_DISTANCE;
extern NSString *const PARAM_PAYMENT_MODE;
extern NSString *const PARAM_TIME_LEFT;
extern NSString *const PARAM_CANCEL_TRIP_REASON;
extern NSString *const PARAM_TYPE_DETAILS;
extern NSString *const PARAM_PROVIDER;
#pragma mark -INVOICE Param
extern NSString *const PARAM_BASEPRICE;
extern NSString *const PARAM_TOTAL;
extern NSString *const PARAM_TAX;
extern NSString *const PARAM_TAX_FEE;
extern NSString *const PARAM_DISTANCE_COST;
extern NSString *const PARAM_TIME_COST;
extern NSString *const PARAM_PROMO_BONOUS;
extern NSString *const PARAM_REFFERAL_BONOUS;
extern NSString *const PARAM_PRICE_FOR_TOTAL_TIME;
extern NSString *const PARAM_PRICE_PER_UNIT_DISTANCE;
extern NSString *const PARAM_RATING;
extern NSString *const PARAM_REVIEW;
extern NSString *const PARAM_TOTAL_DISTANCE;
extern NSString *const PARAM_TOTAL_TIME;
extern NSString *const PARAM_BASE_PRICE_DISTANCE;
extern NSString *const PARAM_SURGE_FEE;
extern NSString *const PARAM_TOTAL_WAITING_TIME;
extern NSString *const PARAM_FOR_WAITING_TIME;
extern NSString *const PARAM_SURGE_MULTIPLIEER;
extern NSString *const PARAM_WAITING_TIME_COST;
extern NSString *const PARAM_WALLET_PAYMENT;
extern NSString *const PARAM_REMAINING_PAYMENT;
extern NSString *const PARAM_CARD_PAYMENT;
extern NSString *const PARAM_CASH_PAYMENT;


#pragma mark - Prefences key
extern NSString *const PREF_DEVICE_TOKEN;
extern NSString *const PREF_PROVIDER_TOKEN;
extern NSString *const PREF_PROVIDER_ID;
extern NSString *const PREF_IS_LOGIN;
extern NSString *const PREF_LOGIN_OBJECT;
extern NSString *const PREF_PASSWORD;
extern NSString *const PREF_TRIP_ID;

#pragma mark -
#pragma mark - MAP Attributes
/*
 *Always Return Current Lat/Long
 */
extern NSString *strForCurLatitude;
extern NSString *strForCurLongitude;

#pragma mark -
#pragma mark - PARAM-VALUES
/**Login By*/
extern NSString *const MANUAL;
extern NSString *const GOOGLE;
extern NSString *const FACEBOOK;
/**Device Detail*/
extern NSString *DEVICE_TOKEN;
extern NSString *const DEVICE_TYPE;
extern NSString *const IOS;
/**WEB SERVICE RESPONSE*/
extern NSString *const  SUCCESS;
extern NSString *const ERROR_CODE;
extern NSString *const MESSAGE;
#pragma mark-Tablview Spacing
extern NSInteger const  ROW_HEIGHT;
extern NSInteger const CELL_SPACE;
#pragma mark-Measurement Units
extern NSString *TIME_SUFFIX;
extern NSString *DISTANCE_SUFFIX;
extern NSString *DistanceUnit;
extern NSString *CurrencySign;
extern NSInteger SECONDS_REMAIN;
#pragma mark-Push Notification Identifieres
extern Boolean checkNewTrip;
extern Boolean checkCancelTrip;
extern Boolean checkApprove;
extern Boolean checkDecline;
#pragma FACEBOOK_PARAMETERS
extern NSString *const FACEBOOK;
extern NSString *const PARAM_FB_ID;
extern NSString *const PARAM_FB_PUBLIC_PROFILE;
extern NSString *const PARAM_FB_EMAIL;
extern NSString *const PARAM_FB_USER_FRIENDS;
extern NSString *const PARAM_FB_FIELDS;
extern NSString *const PARAM_FB_REQUIRED_FIELDS;
#pragma mark - GOOGLE-PARAMS
#define GoogleKey @"AIzaSyD-X64z_0xXdiwbeuamah1Elkl2FkgEZVg"
#define GoogleDistanceAPI @"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@,%@&destinations=%@,%@&key=%@"
extern NSString *const GOOGLE_PARAM_ELEMENTS;
extern NSString *const GOOGLE_PARAM_DESTINATION_ADDRESS;
extern NSString *const PARAM_GOOGLE_SERVER_KEY;
extern NSString *const GOOGLE_PARAM_VALUES;
extern NSString *const GOOGLE_PARAM_STATUS;
extern NSString *const GOOGLE_PARAM_RESULTS;
extern NSString *const GOOGLE_PARAM_STATUS_OK;
extern NSString *const GOOGLE_PARAM_ROWS;
extern NSString *const GOOGLE_PARAM_DISTANCE;
extern NSString *const GOOGLE_PARAM_DURATION;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_FORMATTED_ADDRESS;
#pragma mark-Server keys
extern NSString *GoogleServerKey;
extern NSString *HotlineAppKey;
extern NSString *HotlineAppId;
extern NSString *const PARAM_HOTLINE_KEY;
extern NSString *const PARAM_HOTLINE_APP_ID;
extern NSInteger WAITING_SECONDS_REMAIN;
@end
