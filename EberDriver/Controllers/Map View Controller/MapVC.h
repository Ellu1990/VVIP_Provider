//
//  MapVC.h
//  
//
//  Created by Elluminati Mini Mac 5 on 17/08/16.
//
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "CustomSimpleAlert.h"
#import "CustomVisitorTypeDialog.h"
#import "CustomAlertWithTitle.h"

@import CoreLocation;


@interface MapVC : UIViewController<
GMSMapViewDelegate,
UINavigationControllerDelegate,
CustomSimpleAlertDelegate,
CLLocationManagerDelegate,
AVAudioPlayerDelegate,
CustomVisitorTypeDialogDelegate,
CustomAlertDelegate>
{
    CLLocationManager *locationManager;
    /**Current Location Coordinates of the provider*/
    CLLocationCoordinate2D current_coordinate,lastLocation;
    float myDirection;
}
#pragma mark-Check Push Notication

-(void)checkPush;

#pragma mark-Action
- (IBAction)onClickBtnReject:(id)sender;
- (IBAction)onClickBtnAccept:(id)sender;
- (IBAction)onClickBtnCall:(id)sender;
/*Navigation Bar*/
@property (weak, nonatomic) IBOutlet UIButton *btnTimer;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UILabel *lblTripIdValue;
@property (strong, nonatomic) AVAudioPlayer *sound1Player;
/**************/
/*Main View*/
@property (weak, nonatomic) IBOutlet UILabel *lblTripId;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *btnMyLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnGoOnline;
@property (weak, nonatomic) IBOutlet UIView *subViewDest;
@property (weak, nonatomic) IBOutlet UIView *subViewSrc;
#pragma mark-Timers
@property(nonatomic, strong) NSTimer *timerForUpdateLocation;
@property(nonatomic, strong) NSTimer *timerForAcceptRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForNote;
@property(nonatomic, strong) NSTimer *timerForCheckRequest;
/*view For User*/
@property (weak, nonatomic) IBOutlet UIView *viewForUser;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblEstTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEstDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPic;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeValue;
/*View For Source Address*/
@property (weak, nonatomic) IBOutlet UIView *viewForSourceAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblSrcAddress;

/*View For Destination Address*/
@property (weak, nonatomic) IBOutlet UIView *viewForDestinationAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDestAddress;

/*view For CarType*/
@property (weak, nonatomic) IBOutlet UIView *viewForCar;
@property (weak, nonatomic) IBOutlet UIImageView *imgCar;
@property (weak, nonatomic) IBOutlet UILabel *lblBasePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDistancePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTimePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBasePriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDistancePriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTimePriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCarName;


/*View For Offline*/
@property (weak, nonatomic) IBOutlet UILabel *lblGoOnline;
@property (weak, nonatomic) IBOutlet UIView *viewForOffline;
- (IBAction)onClickViewNote:(id)sender;

@end

