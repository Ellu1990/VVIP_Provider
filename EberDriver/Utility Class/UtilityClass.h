
#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
/*Date Formats*/
#define DATE_TIME_FORMAT_WEB  @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
#define DATE_TIME_FORMAT @"yyyy-MM-dd HH:mm:ss"
#define TIME_FORMAT @"HH:mm:ss"
#define DATE_FORMAT @"yyyy-MM-dd"
#define DATE_FORMAT_MONTH @"MMMM yyyy"
#define DAY @"d"
#define TIME_FORMAT_AM @"h:mm a"
@interface UtilityClass : NSObject
+(UtilityClass *)sharedObject;
-(void)animateShow:(UIView*) view;
-(void)animateHide:(UIView*) view;
-(double)meterToKilometer:(double)meter;
-(NSString *)applicationDocumentDirectoryString;
-(NSString *)applicationCacheDirectoryString;
-(NSURL *)applicationDocumentsDirectoryURL;
-(BOOL)isValidEmailAddress:(NSString *)email;
-(void)displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
-(void) showToast:(NSString *)message;
-(UIView*)addShadow:(UIView*)view;
-(NSDate*)stringToDate:(NSString *)dateString;
-(NSDate*)stringToDate:(NSString *)dateString withFormate:(NSString *)format;
-(NSString*)DateToString:(NSDate *)date;
-(NSString *)DateToString:(NSDate *)date withFormate:(NSString *)format;
-(NSString *)DateToString:(NSDate *)date withFormateSufix:(NSString *)format;
-(NSString *)secondToTime:(int)totalSeconds;
-(NSString *)DateFormate:(NSString*)date;
#pragma mark-Set Font Style
-(void)loadFromURL: (NSURL*) url callback:(void (^)(UIImage *image))callback;
+ (BOOL)isEmpty:(NSString *)str;
+(NSString *)formatAddress:(NSString *)address;
+(void)sendMailTo:(NSString*)mailId andSubject:(NSString*)subject;

@end
