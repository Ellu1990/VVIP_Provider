//
//  Parser.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 12/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "Parser.h"
#import "PreferenceHelper.h"
#import <objc/runtime.h>
#import "NSObject+Constants.h"
#import "UtilityClass.h"
#import "Document.h"
#import "Trip.h"
#import "User.h"
#import "invoice.h"
#import "AppDelegate.h"


@implementation Parser
#pragma mark - SingleTon Methods
+(Parser *) sharedObject
{
    static Parser *objParser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objParser = [[Parser alloc] init];
    });
    return objParser;
}

#pragma mark- Convert Response To Json
-(BOOL)stringToJson:(NSString*)response To:(id*)json
{
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    @try {
        
        id parsedObject= [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (parsedObject == nil)
        {
            NSLog(@"Given String Is Not Valid");
            return NO;
        }
        else if ([parsedObject isKindOfClass: [NSArray class]] )
        {
            *json=parsedObject;
            return YES;
        }
        else if ([parsedObject isKindOfClass: [NSDictionary class]] )
        {
            *json=parsedObject;
            return YES;
        }
        else
        {
            NSLog(@"Given Type And Json Type Is Not Compitable");
            return NO;
        }
        
    } @catch (NSException *exception) {
        NSLog(@"Json Exception:-- %@",exception);
    }
}

#pragma mark-REGISTER AND LOGIN
- (BOOL)isLoginSuccess:(NSString*)response
{
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
           if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
            PREF.providerId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PROVIDER_ID] ];
            PREF.providerToken=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PROVIDER_TOKEN] ];
            PREF.providerFirstName= [NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_FIRST_NAME] ];
            PREF.providerLastName=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_LAST_NAME] ];
            PREF.providerPicture= [NSString stringWithFormat:@"%@%@",BASE_URL,[jsonResponse valueForKey:PARAM_PICTURE] ];
            PREF.ProviderApproved=[[jsonResponse valueForKey:PARAM_PROVIDER_APPROVED]boolValue];
            PREF.ProviderDocumentUploaded=[[jsonResponse valueForKey:PARAM_PROVIDER_DOC_UPLOADED] boolValue];
            PREF.ProviderActive=[[jsonResponse valueForKey:PARAM_PROVIDER_IS_ACTIVE]boolValue];
            PREF.ProviderLogin=YES;
            PREF.providerEmail=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_EMAIL] ];
            PREF.providerSocialId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_SOCIAL_UNIQUE_ID] ];
            PREF.providerLoginBy=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_LOGIN_BY] ];
            PREF.providerCountryCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_COUNTRY_CODE] ];
            PREF.providerPhone=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PHONE] ];
            PREF.providerAddress=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ADDRESS] ];
            PREF.providerBio=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_BIO] ];
            PREF.providerZipcode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ZIPCODE] ];
            PREF.providerType=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PROVIDER_TYPE] ] integerValue];
            if (PREF.ProviderType==1)
            {
                PREF.PartnerEmail=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PARTNER_EMAIL] ];
                PREF.PartnerApproved=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_IS_PARTNER_APPROVED_BY_ADMIN] ] boolValue];
            }
            NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
            return YES;
            }
            else
            {
             NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
              dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
             return NO;
            }
        }
        else
        {
            return NO;
        }
    }
    @catch(NSException *e)
    {
        NSLog(@"Login Response Not Valid Generate Exception %@",e);
        return NO;
    }
}
#pragma mark-WS Profile Response
- (BOOL)isProfileUpdated:(NSString*)response
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                PREF.providerId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PROVIDER_ID]];
                PREF.providerFirstName= [NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_FIRST_NAME] ];
                PREF.providerLastName=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_LAST_NAME] ];
                PREF.providerPicture= [NSString stringWithFormat:@"%@%@",BASE_URL,[jsonResponse valueForKey:PARAM_PICTURE] ];
                PREF.providerSocialId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_SOCIAL_UNIQUE_ID] ];
                PREF.providerCountryCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_COUNTRY_CODE] ];
                PREF.providerPhone=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PHONE] ];
                PREF.providerAddress=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ADDRESS] ];
                PREF.providerBio=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_BIO] ];
                PREF.providerZipcode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ZIPCODE] ];
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Generate Exception In Parsing Profile Detail %@",e);
        return NO;
    }
 
    
return YES;
}
#pragma mark-WS Get All Uploaded Documents Response
- (BOOL)getAllDocuments:(NSString*)response toArray:(NSMutableArray * __strong *)arrForDocuments
{
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
               for(NSDictionary *docDict in [jsonResponse valueForKey:PARAM_PROVIDER_DOCUMENT])
               {
                   Document *d=[[Document alloc]init];
                   d.providerDocumentId=[NSString stringWithFormat:@"%@",[docDict valueForKey:PARAM_ID] ];
                   d.providerDocumentTitle=[NSString stringWithFormat:@"%@",[docDict valueForKey:PARAM_DOCUMENT_NAME] ];
                   d.providerDocumenUrl=[NSString stringWithFormat:@"%@%@",BASE_URL,[docDict valueForKey:PARAM_DOCUMENT_PICTURE] ];
                   d.isUploaded=[[docDict valueForKey:PARAM_DOCUMENT_UPLOADED]boolValue ];
                   d.isOptional=[[docDict valueForKey:PARAM_DOCUMENT_OPTIONAL]boolValue ];
                   [*arrForDocuments addObject:d];
               }
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Error in Parsing Documents Response  %@",e);
        return NO;
    }
    
    return YES;
}
#pragma mark-WS Get Upload Document Response
- (BOOL)getDocument:(NSString*)response toString:(NSString**)strUrl;
{
   @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                *strUrl=[NSString stringWithFormat:@"%@%@",BASE_URL,[jsonResponse valueForKey:PARAM_DOCUMENT_PICTURE] ];
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Generate Exception  In Parsing%@",e);
        return NO;
    }
    
    return YES;

}
#pragma mark-WS-Logout
- (BOOL)isLogoutSuccess:(NSString*)response
{
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                [PREF clearPreference];
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Logout Response Not Valid Generate Exception %@",e);
        return NO;
    }

}
#pragma mark-WS Toggle State-Online/Offline
- (BOOL)toggoleState:(NSString*)response
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                NSLog(@"%@",jsonResponse);
                PREF.ProviderActive=[[jsonResponse  valueForKey:PARAM_PROVIDER_IS_ACTIVE] boolValue];
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Toggle Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
#pragma mark-WS UpdateLocation
- (BOOL)updateLocation:(NSString*)response
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {

                return YES;
            }
            else
            {
                /*NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });*/
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Location Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
- (BOOL)getTrip:(NSString*)response
{
    @try
    {
       
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                User *user=[User sharedObject];
                PREF.providerTripId=[jsonResponse  valueForKey:PARAM_TRIP_ID];
                user.pickUpAddress=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TRIP_SOURCE_ADDRESS]];
                user.pickUpLatitude=[NSString stringWithFormat:@"%@",[[jsonResponse valueForKey:PARAM_SOURCE_LOCATION]objectAtIndex:0]];
                user.pickUpLongitude=[NSString stringWithFormat:@"%@",[[jsonResponse valueForKey:PARAM_SOURCE_LOCATION]objectAtIndex:1]];
                user.destAddress=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TRIP_DESTINATION_ADDRESS] ];
                user.destLatitude=[NSString stringWithFormat:@"%@",[[jsonResponse valueForKey:PARAM_DESTINATION_LOCATION]objectAtIndex:0]];
                user.destLongitude=[NSString stringWithFormat:@"%@",[[jsonResponse valueForKey:PARAM_DESTINATION_LOCATION]objectAtIndex:1]];
				user.note=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:@"note"]];
                NSDictionary *tripResponse=[jsonResponse valueForKey:PARAM_USER];
                user.firstName=[NSString stringWithFormat:@"%@",[tripResponse valueForKey:PARAM_FIRST_NAME]];
                user.lastName=[NSString stringWithFormat:@"%@",[tripResponse valueForKey:PARAM_LAST_NAME]];
                user.phone=[NSString stringWithFormat:@"%@",[tripResponse valueForKey:PARAM_PHONE]];
                user.phoneCountryCode=[NSString stringWithFormat:@"%@",[tripResponse valueForKey:PARAM_COUNTRY_CODE]];
                user.paymentMode=[NSString stringWithFormat:@"%@",[tripResponse valueForKey:PARAM_PAYMENT_MODE] ];
                user.rating=[NSString stringWithFormat:@"%@",[tripResponse valueForKey:PARAM_FIRST_NAME] ];
                user.profileImage=[NSString stringWithFormat:@"%@%@",BASE_URL,[tripResponse valueForKey:PARAM_PICTURE] ];
                
                NSString *timeLeft=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TIME_LEFT]];
                SECONDS_REMAIN=[timeLeft integerValue];
                return YES;
            }
            else
            {
              /*  NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });*/
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Get Trip Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
- (BOOL)getTripStatus:(NSString*)response
{
    @try
    {
       // NSLog(@"%@",response);
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                
                NSDictionary *tripDictionary=[jsonResponse valueForKey:PARAM_TRIP];
                
                //PREF.providerTripId=[tripDictionary  valueForKey:PARAM_ID];
                User *userInfo=[User sharedObject];
                userInfo.pickUpAddress=[NSString stringWithFormat:@"%@",[tripDictionary valueForKey:PARAM_TRIP_SOURCE_ADDRESS]];
                userInfo.pickUpLatitude=[NSString stringWithFormat:@"%@",[[tripDictionary valueForKey:PARAM_SOURCE_LOCATION]objectAtIndex:0]];
                userInfo.pickUpLongitude=[NSString stringWithFormat:@"%@",[[tripDictionary valueForKey:PARAM_SOURCE_LOCATION]objectAtIndex:1]];
                userInfo.destAddress=[NSString stringWithFormat:@"%@",[tripDictionary valueForKey:PARAM_TRIP_DESTINATION_ADDRESS] ];
                userInfo.destLatitude=[NSString stringWithFormat:@"%@",[[tripDictionary valueForKey:PARAM_DESTINATION_LOCATION]objectAtIndex:0]];
                userInfo.destLongitude=[NSString stringWithFormat:@"%@",[[tripDictionary valueForKey:PARAM_DESTINATION_LOCATION]objectAtIndex:1]];
                userInfo.paymentMode=[NSString stringWithFormat:@"%@",[tripDictionary valueForKey:PARAM_PAYMENT_MODE] ];
                CurrencySign=[NSString stringWithFormat:@"%@",[tripDictionary valueForKey:PARAM_CURRENCY] ];
                DistanceUnit=[NSString stringWithFormat:@"%@",[tripDictionary valueForKey:PARAM_DISTANCE_UNIT]];
                userInfo.providerStatus=[[tripDictionary valueForKey:PARAM_PROVIDER_STATUS] integerValue];
                userInfo.tripNumber=[NSString stringWithFormat:@"%@",[tripDictionary valueForKey:PARAM_TRIP_NUMBER]];
                userInfo.waitingPrice=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_FOR_WAITING_TIME]] doubleValue];
                WAITING_SECONDS_REMAIN=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TOTAL_WAIT_TIME] ] integerValue];
                
                if ([DistanceUnit isEqualToString:@"0"])
                {
                    DISTANCE_SUFFIX=NSLocalizedString(@"MILE", nil);
                }
                else
                {
                    DISTANCE_SUFFIX=NSLocalizedString(@"KILOMETER", nil);
                }

                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Get Trip Status Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
- (BOOL)respondTrip:(NSString*)response
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Respond Trip Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
- (BOOL)completeTrip:(NSString*)response 
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                
                Invoice *invoice=[Invoice sharedObject];
                NSDictionary *jsonTripService=[jsonResponse valueForKey:PARAM_TRIP_SERVICE];
                NSDictionary *jsonTrip=[jsonResponse valueForKey:PARAM_TRIP];
                invoice.basePrice=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_BASEPRICE] doubleValue] ];
                invoice.total=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL] doubleValue]];
                invoice.time=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL_TIME] doubleValue]];
                invoice.timeCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TIME_COST] doubleValue]];
                invoice.pricePerUnitTime=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_PRICE_FOR_TOTAL_TIME] doubleValue]];
                invoice.distance=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL_DISTANCE] doubleValue]];
                invoice.distanceCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_DISTANCE_COST] doubleValue]];
                invoice.paymentMode=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_PAYMENT_MODE]];
                invoice.referralBonus=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_REFFERAL_BONOUS] doubleValue]];
                invoice.promoBonus=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_PROMO_BONOUS] doubleValue]];
                invoice.pricePerUnitDistance=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_PRICE_PER_UNIT_DISTANCE] doubleValue]];
                invoice.currency=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_CURRENCY]];
                invoice.tax=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_TAX] doubleValue]];
                invoice.taxCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TAX_FEE] doubleValue]];
                invoice.basePriceDistance=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_BASE_PRICE_DISTANCE] doubleValue]];
                invoice.surgeTimeFee=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_SURGE_FEE] doubleValue]];
                invoice.pricePerWaitingTime=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_FOR_WAITING_TIME] doubleValue]];
                invoice.totalWaitingTime=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL_WAITING_TIME] doubleValue]];
                invoice.surgeMultiplier=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_SURGE_MULTIPLIEER] doubleValue]];
                invoice.waitingTimeCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_WAITING_TIME_COST] doubleValue]];
                invoice.invoiceNumber=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_INVOICE_NUMBER]];
                invoice.walletPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_WALLET_PAYMENT] doubleValue]];
                invoice.remainingPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_REMAINING_PAYMENT] doubleValue]];
                invoice.cardPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_CARD_PAYMENT] doubleValue]];
                 invoice.cashPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_CASH_PAYMENT] doubleValue]];
                DistanceUnit=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_DISTANCE_UNIT]];
                if ([DistanceUnit isEqualToString:@"0"])
                {
                    invoice.distanceUnit=NSLocalizedString(@"MILE", nil);
                    DISTANCE_SUFFIX=NSLocalizedString(@"MILE", nil);
                }
                else
                {
                    invoice.distanceUnit=NSLocalizedString(@"KILOMETER", nil);
                    DISTANCE_SUFFIX=NSLocalizedString(@"KILOMETER", nil);
                }
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Complete Trip Response Not Valid Generate Exception %@",e);
        return NO;
    }


}
- (BOOL)parseHistory:(NSString*)response toArray:(NSMutableArray * __strong *)arrForHistory
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                for(NSDictionary *tripDict in [jsonResponse valueForKey:PARAM_TRIPS])
                {
                    Trip *trip=[[Trip alloc]init];
                    trip.tripId=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_TRIP_ID]] ;
                    trip.userImage=[NSString stringWithFormat:@"%@%@",BASE_URL,[tripDict valueForKey:PARAM_PICTURE]];
                    trip.userFirstName=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_FIRST_NAME] ];
                    trip.userLastName=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_LAST_NAME] ];
                    trip.tripTotalCost=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_TOTAL]];
                    trip.tripTime=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_TOTAL_TIME]];
                    NSString *strTime=[tripDict valueForKey:PARAM_TRIP_CREATE_TIME];
                    NSDate *date=[[UtilityClass sharedObject]stringToDate:strTime withFormate:DATE_TIME_FORMAT_WEB];
                    trip.tripCreateTime=[[UtilityClass sharedObject]DateToString:date withFormate:TIME_FORMAT_AM];
                    NSString *strDate=[[UtilityClass sharedObject]DateToString:date withFormate:DATE_FORMAT];
                    trip.tripCreateDate=[[UtilityClass sharedObject]stringToDate:strDate withFormate:DATE_FORMAT];
                    trip.currency=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_CURRENCY]];
                    trip.distanceUnit=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_DISTANCE_UNIT]];
                    trip.isTripCancelled=[[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_IS_TRIP_CANCELLED_BY_PROVIDER]] boolValue];
                    
                    if ([trip.distanceUnit isEqualToString:@"0"])
                    {
                        trip.distanceUnit=NSLocalizedString(@"MILE", nil);
                    }
                    else
                    {
                        trip.distanceUnit=NSLocalizedString(@"KILOMETER", nil);;
                    }
                    [*arrForHistory addObject:trip];
                }
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"History Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
    return NO;
}
-(BOOL)parseCities:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCities
{
    @try
    {
        if (response)
        {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                [*arrForCities removeAllObjects];
                NSArray *tempArray=[jsonResponse valueForKey:PARAM_CITY];
                for(NSInteger i=0;i<tempArray.count;i++)
                {
                    NSString *strCity=[NSString stringWithFormat:@"%@",[[tempArray objectAtIndex:i] valueForKey:PARAM_CITY_NAME]];
                    [*arrForCities addObject:strCity];
                }
                return YES;
            }
            else
            {
                NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                               });
                return NO;
            }
        }
                else
                {return NO;}
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Citytype Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
    return NO;
}
#pragma mark-Parse Invalid Token
-(BOOL)parseSessionToken:(NSString*)response
{
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                if(![[jsonResponse valueForKey:SUCCESS]boolValue])
                {
                    NSString *strErrorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                    
                    if ([strErrorCode isEqualToString:invalidTokenId])
                    {
                        [PREF clearPreference];
                        dispatch_async(dispatch_get_main_queue()
                                       , ^{
                                           [APPDELEGATE hideLoadingView];
                                           [APPDELEGATE goToMain];
                                           
                                       });
                        return false;
                        
                    }
                    
                }
                
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        
    }
    @catch(NSException *e)
    {
    }
    
    return true;
}
#pragma mark- Pars Otp Type
- (BOOL)parseVerificationTypes:(NSString*)response
{
    @try
    {
        if (response)
        {
            NSLog(@"Verification Response %@",response);
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                
                PREF.EmailOtpOn=[[jsonResponse valueForKey:PARAM_EMAIL_VERIFICATION_ON] boolValue];
                PREF.SmsOtpOn=[[jsonResponse valueForKey:PARAM_SMS_VERIFICATION_ON] boolValue];
                PREF.ContactEmail=[jsonResponse valueForKey:PARAM_CONTACT_EMAIL];
                PREF.IsPathDraw=[[jsonResponse valueForKey:PROVIDER_PATH_DRAW] boolValue];
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
        
    }
    @catch(NSException *e)
    {
        NSLog(@"GET Application Settings Not Valid Generate Exception %@",e);
        return NO;
    }
}
- (BOOL)parseApiKeys:(NSString*)response
{
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                @try
                {
                    GoogleServerKey=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_GOOGLE_SERVER_KEY]];
                    HotlineAppKey=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_HOTLINE_KEY]];
                    HotlineAppId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_HOTLINE_APP_ID]];
                    if (GoogleServerKey && HotlineAppId && HotlineAppId)
                    {
                        PREF.ServerKey=GoogleServerKey;
                        PREF.HotlineAppId=HotlineAppId;
                        PREF.HotlineAppKey=HotlineAppKey;
                        
                    }
                    else
                    {
                        GoogleServerKey=[NSString stringWithFormat:@"%@",PREF.serverKey ];
                        HotlineAppKey=[NSString stringWithFormat:@"%@",PREF.hotLineAppKey ];
                        HotlineAppId=[NSString stringWithFormat:@"%@",PREF.hotLineAppId ];
                    }
                    
                    return true;
                }
                @catch (NSException *exception)
                {
                    GoogleServerKey=[NSString stringWithFormat:@"%@",PREF.serverKey ];
                    HotlineAppKey=[NSString stringWithFormat:@"%@",PREF.hotLineAppKey ];
                    HotlineAppId=[NSString stringWithFormat:@"%@",PREF.hotLineAppId ];
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        else
            return false;
        
    }
    @catch(NSException *e)
    {
        NSLog(@"GET APIKEYS  Not Valid Generate Exception %@",e);
        return NO;
    }
}
- (BOOL)parseServiceCountries:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCoutries
{
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                {
                    [*arrForCoutries removeAllObjects];
                    NSArray *tempArray=[jsonResponse valueForKey:PARAM_COUNTRY];
                    for(NSInteger i=0;i<tempArray.count;i++)
                    {
                        NSDictionary *dict = @{PARAM_COUNTRY_PHONE_CODE : [[tempArray objectAtIndex:i] valueForKey:PARAM_COUNTRY_PHONE_CODE], PARAM_COUNTRY_NAME :[[tempArray objectAtIndex:i] valueForKey:PARAM_COUNTRY_NAME]};
                        [*arrForCoutries addObject:dict];
                    }
                    return YES;
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                                   });
                    return NO;
                }
            }
            else
            {return NO;}
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Countries Response Not Valid Generate Exception %@",e);
        return NO;
    }
    return NO;
}
- (BOOL)parseBankDetails:(NSString*)response toBankDetailObject:(BankDetail * __strong *)bankDetail
{
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                {
                
                    NSDictionary *bankJson=[[NSDictionary alloc]init];
                    bankJson=[jsonResponse objectForKey:PARAM_BANK_DETEILS];
                    [*bankDetail setId_:[bankJson valueForKey:PARAM_ID]];
                    [*bankDetail setBankAccountHolderName:[bankJson valueForKey:PARAM_BANK_ACCOUNT_HOLDER_NAME]];
                    [*bankDetail setBankAccountNumber:[bankJson valueForKey:PARAM_BANK_ACCOUNT_NUMBER]];
                    [*bankDetail setBankName:[bankJson valueForKey:PARAM_BANK_NAME]];
                    [*bankDetail setBankBranch:[bankJson valueForKey:PARAM_BANK_BRANCH]];
                    [*bankDetail setBankUniqueCode:[bankJson valueForKey:PARAM_BANK_UNIQUE_CODE]];
                    [*bankDetail setBankSwiftCode:[bankJson valueForKey:PARAM_BANK_SWIFT_CODE]];
                    [*bankDetail setBankBeneficiaryAddress:[bankJson valueForKey:PARAM_BANK_BENEFICIARY_ADDRESS]];
                    return YES;
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       [[UtilityClass sharedObject] showToast:NSLocalizedString(str, nil)];
                                   });
                    return NO;
                }
                
            }
            {return NO;}
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Bank detail Response Not Valid Generate Exception %@",e);
        return NO;
    }
    return NO;
}
-(BOOL)parseSuccess:(NSString*)response
{
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        else
        {
        return false;
        }
    }
    @catch(NSException *e)
    {
        return false;
    }
}
@end
