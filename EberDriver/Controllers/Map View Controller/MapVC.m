//
//  MapVC.m
//
//
//  Created by Elluminati Mini Mac 5 on 17/08/16.
//
//
#import "MapVC.h"
#import "UIImageView+image.h"
#import "NSObject+Constants.h"
#import "SWRevealViewController.h"
#import "PreferenceHelper.h"
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Parser.h"
#import "UILabel+overrideLabel.h"
#import "UIColor+Colors.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "User.h"
#import "CustomSimpleAlert.h"
#define counterForOffilineWarning 50
#define counterForMakeOffiline 70

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiansToDegrees(x) (x * 180.0 / M_PI)
#define d2r ((M_PI)/180.0)
#define r2d (180.0/(22/7.0))
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
@interface MapVC ()
{
    GMSMarker *driverMarker,*clientMarker,*dest_marker;
    BOOL moveCamera,isTimerOnForGetTrip,isTypeAvailable;
    NSInteger secondsLeft,countUpdate;
    NSMutableDictionary *postUpdateParam;
    NSString *adminTypeId,
    *serviceTypeId,
    *providerRegisteredCity,
    *strForCity,
    *strForCountry;
    NSMutableArray *userDefaultLanguages;
    CustomVisitorTypeDialog *visitorTypeDialog;
    CustomAlertWithTitle *viewForAdminAlert,*viewForPartnerAlert,*viewForNoServiceProvided;
}
@end

@implementation MapVC
@synthesize sound1Player;
- (void)viewDidLoad
{
    [super viewDidLoad];
    secondsLeft=SECONDS_REMAIN;
    [_btnTimer setHidden:YES];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING",nil)];
    self.mapView.delegate = self;
    self.mapView.settings.myLocationButton=NO;
    self.mapView.myLocationEnabled=NO;
    [_mapView clear];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
    driverMarker = [[GMSMarker alloc] init];
    [_viewForCar setHidden:NO];
    current_coordinate=[self getLocation];
    if (IS_TRIP_EXSIST && IS_PROVIDER_ACCEPTED)
    {
        [locationManager setDelegate:nil];
        [self performSegueWithIdentifier:SEGUE_TO_TRIP sender:self];
        return;
    }
    else if (IS_TRIP_EXSIST && !IS_PROVIDER_ACCEPTED)
    {
        [self setLocalization];
        [self wsGetCarDetail];
        [self customSetup];
        [self onGetNewTrip];
    }
    else
    {
        postUpdateParam=[[NSMutableDictionary alloc]init];
    }
	
	User *user = [User sharedObject];
	NSLog(@"Note = %@",user.note);
	
	if (![user.note isEqualToString:@""]) {
		self.viewForNote.hidden = NO;
	}
	else
	{
		self.viewForNote.hidden = YES;
	}
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    moveCamera=YES;
    if (IS_TRIP_EXSIST && IS_PROVIDER_ACCEPTED)
    {
        return;
    }
    else if (IS_TRIP_EXSIST)
    {
        [self wsGetTrip];
        [_viewForCar setHidden:NO];
    }
    else
    {
        if (PREF.ProviderApproved)
        {
                if (PREF.ProviderType==1 )
                {
                    if (PREF.PartnerApproved)
                    {
                        [self setLocalization];
                        [self wsGetCarDetail];
                        [self customSetup];
                        IS_DOCUMENT_PARENT_IS_REGISTER=NO;
                        [self isProviderOnline];
                    }
                    else
                    {
                        if (!viewForPartnerAlert)
                        {
                            viewForPartnerAlert=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERET_TITLE_ADMIN_REVIEW", nil) message:NSLocalizedString(@"ALERET_MSG_ADMIN_REVIEW", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"LOGOUT", nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_EMAIL",nil)];
                        }
                    }
                }
                else
                {
                    [self setLocalization];
                    [self wsGetCarDetail];
                    [self customSetup];
                    IS_DOCUMENT_PARENT_IS_REGISTER=NO;
                    [self isProviderOnline];
                }
        }
        else
        {
            if(PREF.isProviderDocumentUploaded)
            {
                [APPDELEGATE hideLoadingView];
                [self setLocalization];
                
                if (!viewForAdminAlert)
                {
					viewForAdminAlert=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERET_TITLE_ADMIN_REVIEW", nil) message:NSLocalizedString(@"ALERET_MSG_ADMIN_REVIEW", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"LOGOUT", nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_EMAIL",nil)];
                }
            }
            else
            {
                IS_DOCUMENT_PARENT_IS_REGISTER=YES;
                [self performSegueWithIdentifier:SEGUE_TO_DOCUMENT sender:self];
            }
        }
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)setLocalization
{
    isTimerOnForGetTrip=YES;
    moveCamera=YES;
    
    [_lblTripId setTextColor:[UIColor labelTextColor]];
    [_lblTripId setText:NSLocalizedString(@"TRIP_ID", nil)];
 
    [_lblTripIdValue setTextColor:[UIColor textColor]];
    
    [_btnNavigation setTitle:NSLocalizedString(@"TITLE_TAXI_PROVIDER", nil) forState:UIControlStateNormal];
    [_subViewSrc setBackgroundColor:[UIColor lightGreenColor]];
    [_subViewDest setBackgroundColor:[UIColor lightGreenColor]];
    [self.view bringSubviewToFront:_btnGoOnline];
    [self.view bringSubviewToFront:_btnMyLocation];
    [_btnGoOnline setBackgroundColor:[UIColor buttonColor]];
    
    /*setUp View Hidden and Cutting*/
    CGPoint center=_imgCar.center;
    center.y=_viewForCar.center.y;
    
    _imgCar.center=center;
    [_imgCar setRoundImageWithColor:[UIColor borderColor]];
    _viewForCar=[[UtilityClass sharedObject]addShadow:_viewForCar];
    [_viewForCar setHidden:NO];
    
    _btnTimer.layer.cornerRadius=10.0f;
    _btnTimer.clipsToBounds=YES;
    [_btnTimer.titleLabel setText:NSLocalizedString(@"60 remaining", nil)];
	
	/*Set Titles And Colors*/
    [_lblDistancePrice setText:NSLocalizedString(@"DISTANCE_PRICE", nil) withColor:[UIColor labelTextColor]];
    [_lblTimePrice setText:NSLocalizedString(@"TIME_PRICE", nil) withColor:[UIColor labelTextColor]];
    [_lblBasePrice setText:NSLocalizedString(@"BASE_PRICE", nil) withColor:[UIColor labelTextColor]];
    [_lblTimeValue setTextColor:[UIColor textColor]];
    [_lblDistanceValue setTextColor:[UIColor textColor]];
    [_lblPayment setText:NSLocalizedString(@"CASH", nil) withColor:[UIColor textColor]];
    [_lblGoOnline setText:NSLocalizedString(@"LBL_GO_ONLINE", nil) withColor:[UIColor labelTextColor]];
    [_lblGoOnline setFontForLabel:_lblGoOnline withMaximumFontSize:45 andMaximumLines:2];
    [_btnAccept setTitle:NSLocalizedString(@"ACCEPT", nil) forState:UIControlStateNormal];
    [_btnAccept setBackgroundColor:[UIColor DarkGreenColor]];
    [_btnReject setTitle:NSLocalizedString(@"REJECT", nil) forState:UIControlStateNormal];
    [_btnReject setBackgroundColor:[UIColor GreenColor]];
    [_lblSrcAddress setTextColor:[UIColor labelTitleColor]];
    [_lblDestAddress setTextColor:[UIColor labelTitleColor]];
    [_viewForDestinationAddress setBackgroundColor:[UIColor GreenColor]];
    [_viewForSourceAddress setBackgroundColor:[UIColor GreenColor]];
    [_btnTimer setBackgroundColor:[UIColor DarkGreenColor]];
    [_btnGoOnline setBackgroundColor:[UIColor buttonColor]];
    [_btnGoOnline setTitle:[NSLocalizedString(@"GO_ONLINE", nil) uppercaseString] forState:UIControlStateNormal];
	_viewForNote.layer.cornerRadius = 5.0;
	_viewForNote.hidden = true;
    [APPDELEGATE hideLoadingView];
    
}
/// This Method is Used To Custom Setup For Navigation Bar & RevealViewController Sidebar
- (void)customSetup
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController tapGestureRecognizer];
    [self.btnNavigation addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
#pragma mark - Actions
- (IBAction)onClickBtnMylocation:(id)sender{
    [self showCurrentLocation];
    moveCamera=YES;
}
- (IBAction)clickGoOnline:(id)sender{
    [self wssetStatus];
}
- (IBAction)onClickBtnReject:(id)sender
{
    [_mapView clear];
    [self performSelectorOnMainThread:@selector(stopAcceptRequestTimer) withObject:nil waitUntilDone:YES];
    [self wsRespondTrip:NO];
}
- (IBAction)onClickBtnAccept:(id)sender
{
    [self performSelectorOnMainThread:@selector(stopAcceptRequestTimer) withObject:nil waitUntilDone:YES];
    [self wsRespondTrip:YES];
}
- (IBAction)onClickBtnCall:(id)sender {
    NSString *mobileNo=[NSString stringWithFormat:@"tel:%@%@",[User sharedObject].phoneCountryCode,[User sharedObject].phone ];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNo]];
}
#pragma mark- User Define Methods
/**change Timer Seconds When New Trip is come*/

-(void)changetitle
{
    if ([_timerForAcceptRequest isValid])
    {
        if (secondsLeft<=0)
        {
            if(!IS_PROVIDER_ACCEPTED)
            {
            dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [self performSelectorOnMainThread:@selector(stopAcceptRequestTimer) withObject:nil waitUntilDone:YES];
                           [_btnTimer setHidden:YES];
                           [sound1Player stop];
                           secondsLeft=SECONDS_REMAIN;
                           [self wsRespondTrip:NO];
                       });
            }
        }
        else
        {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [_btnTimer setTitle:[NSString stringWithFormat:@"%ld remaining",(long)secondsLeft ] forState:UIControlStateNormal];
                       });
        
        secondsLeft--;
        if (!PREF.isSoundOff)
        {
            [self playBeepSound];
        }
        }
    }
}
-(void) playBeepSound
{
    NSString *bk=[NSString stringWithFormat:@"beep"];
    NSString *path = [[NSBundle mainBundle] pathForResource:bk ofType:@"mp3"];
    NSURL *url=[NSURL fileURLWithPath:path];
    sound1Player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    if(!sound1Player)
        NSLog(@"error in play sound");
    sound1Player.delegate=self;
    sound1Player.numberOfLoops=0;
    [sound1Player play];
}
#pragma mark- LOCATION RELATED METHODS
-(void)showCurrentLocation
{
    if ([CLLocationManager locationServicesEnabled])
    {
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude=[strForCurLatitude doubleValue];
        coordinate.longitude=[strForCurLongitude doubleValue];
        
        driverMarker.position = coordinate;
        driverMarker.icon=[UIImage imageNamed:@"pin_driver"];
        if (!driverMarker.map)
        {
            driverMarker.map = _mapView;
        }
        
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coordinate zoom:15];
        [_mapView animateWithCameraUpdate:updatedCamera];
        
    }
    else
    {
        [[UtilityClass sharedObject]displayAlertWithTitle:@"" andMessage:NSLocalizedString(@"ALERT_MSG_LOCATION_SERVICE_NOT_AVAILABLE", nil)];
    }
    
}
/*!
 @brief Used To Get Current location Of User.
 @discussion This method accepts a nothing representing the Location in <b>Coordinate2D</b>
 To use it, simply call @c[self getLocation];
 */
-(CLLocationCoordinate2D) getLocation
{
    CLLocationCoordinate2D coordinate;
    if([APPDELEGATE connected])
    {
        if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager startUpdatingLocation];
            locationManager.headingFilter = kCLHeadingFilterNone;
            [locationManager startUpdatingHeading];
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            locationManager.distanceFilter = 3;
            
            #ifdef __IPHONE_8_0
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8"))
            {
                [locationManager requestAlwaysAuthorization];
            }
            #endif
            CLLocation *location = [locationManager location];
            if (location)
            {
                coordinate = [location coordinate];
                [self showCurrentLocation];
                [self setMarkerAndCamera:coordinate];
                GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coordinate zoom:15];
                [_mapView animateWithCameraUpdate:updatedCamera];
            }
            else
            {
                coordinate = [location coordinate];
                [self showCurrentLocation];
                [self setMarkerAndCamera:coordinate];
                GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coordinate zoom:15];
                [_mapView animateWithCameraUpdate:updatedCamera];
            }
            return coordinate;
        }
        else
        {
            [[UtilityClass sharedObject]displayAlertWithTitle:@"" andMessage:NSLocalizedString(@"ALERT_MSG_LOCATION_SERVICE_NOT_AVAILABLE", nil)];
        }
    }
    
    return coordinate;
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
    // Use the true heading if it is valid.
    
    CLLocationDirection  theHeading = ((newHeading.trueHeading > 0) ?
                                       
                                       newHeading.trueHeading : newHeading.magneticHeading);
    
    
    
    myDirection = theHeading;
    
    CLLocationDirection direction = newHeading.magneticHeading;
    CGFloat radians = direction / 180.0 * M_PI;
    
    //For Rotate Niddle
    CGFloat angle = RADIANS_TO_DEGREES(radians);
    [self rotateMarker:angle];
    // [self updateHeadingDisplays];
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{

    CLLocation *newLocation = locations.lastObject;
    if (newLocation)
    {
        strForCurLatitude=[NSString stringWithFormat:@"%.8f",newLocation.coordinate.latitude];
        strForCurLongitude=[NSString stringWithFormat:@"%.8f",newLocation.coordinate.longitude];
        current_coordinate.latitude=[strForCurLatitude doubleValue];
        current_coordinate.longitude=[strForCurLongitude doubleValue];
        if (driverMarker.map)
        {
            driverMarker.position=current_coordinate;
        }
        else
        {
            driverMarker.map=nil;
            driverMarker.icon=[UIImage imageNamed:@"pin_driver"];
            driverMarker.map = _mapView;
            driverMarker.position=current_coordinate;
        }
        myDirection=[newLocation course];
	
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat: animationMapDelayed] forKey:kCATransactionAnimationDuration];
        [CATransaction setCompletionBlock:^{
        }];
        [self rotateMarker:myDirection];
        [CATransaction commit];
    }
    else
    {
        [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_TITLE_LOCATION_NOT_FOUND", nil) andMessage:NSLocalizedString(@"ALERT_MSG_ENTER_VALID_LOCATION", nil)];
    }
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

#pragma mark WEbServices
/**Change Driver Status From Online/Offline*/
-(void)wssetStatus
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_CHANGING_AVAILABILITY", nil)];
    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
    [dictparam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictparam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
    [dictparam setObject:[NSNumber numberWithBool:!PREF.isProviderActive] forKey:PARAM_PROVIDER_IS_ACTIVE];
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",PROVIDER_TOGGLE_STATE,PREF.providerId];
    
    [afn getDataFromUrl:strUrl withParamData:dictparam withMethod:put withBlock:^(id response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [APPDELEGATE hideLoadingView];
                           if ([[Parser sharedObject]toggoleState:response])
                           {
                               [self isProviderOnline];
                           }
                       });
    }];
}
/**Update Location When Driver is Online*/
-(void)wsUpdateLocation
{
    
    if(current_coordinate.latitude &&current_coordinate.longitude && PREF.isProviderLogin)
    {
        [postUpdateParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        [postUpdateParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
        [postUpdateParam setObject:strForCurLatitude forKey:PARAM_LATITUDE];
        [postUpdateParam setObject:strForCurLongitude forKey:PARAM_LONGITUDE];
        [postUpdateParam setObject:@"" forKey:PARAM_TRIP_ID];
        [postUpdateParam setObject:[NSNumber numberWithFloat:myDirection] forKey:PARAM_BEARING];
        
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@",PROVIDER_UPDATE_LOCATION,PREF.providerId];
        if (strForCurLatitude && strForCurLongitude)
        {
            
            [afn getDataFromUrl:strUrl withParamData:postUpdateParam withMethod:put withBlock:^(id response, NSError *error) {
                if ([[Parser sharedObject]updateLocation:response])
                {
                    [self calculateProviderLocationChang];
                    [_mapView animateToLocation:current_coordinate];
                }}];
        }
        else
        {
            NSLog(@"Location Not Available");
        }
    }
}
/**Check For Get Trip while no trips*/
-(void)wsGetTrip
{
    if (isTimerOnForGetTrip)
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        [dictparam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@/%@",PROVIDER_GET_TRIP,PREF.providerId,PREF.providerToken];
        [afn getDataFromUrl:strUrl withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error) {
            if ([[Parser sharedObject]getTrip:response])
            {
                if (self.isViewLoaded && self.view.window)
                {
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       [self.navigationController popToViewController:self animated:NO];
                                       [APPDELEGATE.window makeKeyAndVisible];
                                   });
                }
                secondsLeft=SECONDS_REMAIN;
                CLLocationCoordinate2D tempLatlong;
                tempLatlong.latitude = [[User sharedObject].pickUpLatitude doubleValue];
                tempLatlong.longitude = [[User sharedObject].pickUpLongitude doubleValue];
                if (tempLatlong.latitude && tempLatlong.longitude)
                {
                    [self getTimeAndDistance:current_coordinate destLocation:tempLatlong];
                }
                [self wsGetTripStatus];
                isTimerOnForGetTrip=NO;
            }
		}];
    }
}
/**IF Trip Found check status*/
-(void)wsGetTripStatus
{
    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
    [dictparam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:PROVIDER_GET_TRIP_STATUS withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error) {
        if(isTimerOnForGetTrip==NO)
        {
            if ([[Parser sharedObject]getTripStatus:response])
            {
             dispatch_async(dispatch_get_main_queue(), ^
             {
               if([[User sharedObject].paymentMode isEqualToString:@"1"])
                 {_lblPayment.text=NSLocalizedString(@"CASH", nil);}
               else
                 { _lblPayment.text=NSLocalizedString(@"CARD", nil);}
               if (_viewForUser.isHidden)
                 {
                    [self onGetNewTrip];
                    [self.view bringSubviewToFront:_viewForUser];
                 }
             });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^
                               {[self onCancelTrip];});
            }
        }
    }];
    
}
-(void)wsRespondTrip:(BOOL)isAccepted
{
    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
    [dictparam setObject:PREF.providerTripId forKey:PARAM_TRIP_ID];
    [dictparam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictparam setObject:[NSNumber numberWithBool:isAccepted] forKey:PARAM_IS_PROVIDER_ACCEPTED];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",PARAM_RESPOND_TRIP,PREF.providerTripId];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_RESPONDING_TRIP", nil)];
    NSLog(@"Respond Trip Called");
    AFNHelper *afn=[[AFNHelper alloc]init];
   
    [afn getDataFromUrl:strUrl withParamData:dictparam withMethod:put withBlock:^(id response, NSError *error) {
        
        if ([[Parser sharedObject]respondTrip:response])
        {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               
                                      if(isAccepted)
                                    {
                                        
                                        [self performSelectorOnMainThread:@selector(stopUpdateLocationTimer) withObject:nil waitUntilDone:YES];
                                        [_btnTimer setHidden:YES];
                                        [_viewForUser setHidden:YES];
										[_viewForNote setHidden:YES];
                                        [_viewForSourceAddress setHidden:YES];
                                        [_viewForDestinationAddress setHidden:YES];
                                        isTimerOnForGetTrip=YES;
                                   [_mapView clear];
                                   IS_TRIP_EXSIST=YES;
                                        IS_PROVIDER_ACCEPTED=YES;
                                   [APPDELEGATE hideLoadingView];
                                   [self performSegueWithIdentifier:SEGUE_TO_TRIP sender:self];
                                   return;
                                    }
                               else
                               {
                
                                   [APPDELEGATE hideLoadingView];
                                   isTimerOnForGetTrip=YES;
                                   IS_TRIP_EXSIST=NO;
                                   IS_PROVIDER_ACCEPTED=NO;
                                   [self setMarkerAndCamera:current_coordinate];
                                   [_viewForUser setHidden:YES];
							       [_viewForNote setHidden:YES];
                                   [_viewForSourceAddress setHidden:YES];
                                   [_viewForDestinationAddress setHidden:YES];
                                   [_btnTimer setHidden:YES];
                                   [_mapView clear];
                                   [_viewForCar setHidden:NO];
                                   [_btnGoOnline setHidden:NO];
                                   [User sharedObject].pickUpAddress=@"null";
                               }
                           });
        }
        else
        {
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [APPDELEGATE hideLoadingView];
                               
                                   [self performSelectorOnMainThread:@selector(stopAcceptRequestTimer) withObject:nil waitUntilDone:YES];
                               isTimerOnForGetTrip=YES;
                               [_viewForUser setHidden:YES];
						       [_viewForNote setHidden:YES];
                               [_viewForSourceAddress setHidden:YES];
                               [_viewForDestinationAddress setHidden:YES];
                               [_btnTimer setHidden:YES];});
            
            
        }
   
    }];
    
}

-(void)onCancelTrip
{
   
        [self performSelectorOnMainThread:@selector(stopAcceptRequestTimer) withObject:nil waitUntilDone:YES];
    isTimerOnForGetTrip=YES;
    [self setMarkerAndCamera:current_coordinate];
    [_viewForUser setHidden:YES];
	[_viewForNote setHidden:YES];
    [_viewForSourceAddress setHidden:YES];
    [_viewForDestinationAddress setHidden:YES];
    [_btnTimer setHidden:YES];
    [_viewForCar setHidden:NO];
    [_btnGoOnline setHidden:NO];
    if (![self.navigationController.visibleViewController isKindOfClass:[UIAlertController class]])
    {
        [[UtilityClass sharedObject]displayAlertWithTitle:@"Trip Cancel" andMessage:@"ok"];
    }
    
}
-(void)onGetNewTrip
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       User *user=[User sharedObject];
                       [_lblTripIdValue setText:user.tripNumber];
                        [self performSelectorOnMainThread:@selector(startAcceptRequestTimer) withObject:nil waitUntilDone:YES];
                       [_viewForCar setHidden:YES];
                       [_btnTimer setHidden:NO];
                       [_btnGoOnline setHidden:YES];
                       [_lblSrcAddress setText:user.pickUpAddress];
                       [_viewForSourceAddress setHidden:NO];
                       [self.view bringSubviewToFront:_viewForSourceAddress];
                       if ([UtilityClass isEmpty:user.destAddress])
                       {
                           [_viewForDestinationAddress setHidden:YES];
                       }
                       else
                       {
                           [_lblDestAddress setText:user.destAddress];
                           
                           [_lblSrcAddress setFontForLabel:_lblSrcAddress withMaximumFontSize:14 andMaximumLines:2];
                           [_lblDestAddress setFontForLabel:_lblDestAddress withMaximumFontSize:14 andMaximumLines:2];
                           CGFloat pointSize=MIN(_lblSrcAddress.font.pointSize, _lblDestAddress.font.pointSize);
                           UIFont *font=[UIFont fontWithName:_lblSrcAddress.font.fontName size:pointSize];
                           _lblSrcAddress.font=_lblDestAddress.font=font;
                           
                           [_viewForDestinationAddress setHidden:NO];
                           [self.view bringSubviewToFront:_viewForDestinationAddress];
                       }
                       [_viewForUser setHidden:NO];
					   [_viewForNote setHidden:NO];
                       [_lblUserName setTextColor:[UIColor textColor]];
                       [_lblUserName setText:[NSString stringWithFormat:@"%@ %@",user.firstName,user.lastName]];
                       CLLocationCoordinate2D dest_coordinate;
                       dest_coordinate.latitude=dest_coordinate.longitude=0;
                       dest_coordinate.latitude=[user.destLatitude doubleValue];
                       dest_coordinate.longitude=[user.destLongitude doubleValue];
                       CLLocationCoordinate2D client_coordinate;
                       client_coordinate.latitude=client_coordinate.longitude=0;
                       client_coordinate.latitude=[user.pickUpLatitude doubleValue];
                       client_coordinate.longitude=[user.pickUpLongitude doubleValue];
                       [self setMarkersAndCamera:current_coordinate pickUpLatLong:client_coordinate destLatLong:dest_coordinate];
                       [_imgUserPic downloadFromURL:user.profileImage withPlaceholder:[UIImage imageNamed:@"user"]];
                       _lblPayment.text=NSLocalizedString(@"CASH", nil);
				   });
}
-(void)wsGetCarDetail
{
    [APPDELEGATE showLoadingWithTitle:@""];
    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
    [dictparam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictparam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
    AFNHelper *afn=[[AFNHelper alloc]init];
    NSString *strUrl=[NSString stringWithFormat:@"%@",PROVIDER_DETAIL];
    [afn getDataFromUrl:strUrl withParamData:dictparam withMethod:post withBlock:^(id response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSDictionary *dictResponse=[[NSDictionary alloc]init];
                           if ([[Parser sharedObject]stringToJson:response To:&dictResponse])
                           {
                               if ([dictResponse valueForKey:SUCCESS])
                               {
                                   NSArray *typeDetail=[dictResponse valueForKey:PARAM_TYPE_DETAILS];
                                   NSDictionary *providerData=[dictResponse valueForKey:PARAM_PROVIDER];
							   
                                   providerRegisteredCity=[providerData valueForKey:PARAM_CITY];
                                   CurrencySign=[typeDetail valueForKey:PARAM_CURRENCY];
                                   DistanceUnit=[NSString stringWithFormat:@"%@",[typeDetail valueForKey:PARAM_DISTANCE_UNIT] ];
                                   PREF.ProviderActive=[[providerData valueForKey:PARAM_PROVIDER_IS_ACTIVE]boolValue];
                                   if ([DistanceUnit isEqualToString:@"0"])
                                   {
                                       DISTANCE_SUFFIX=NSLocalizedString(@"MILE", nil);
                                   }
                                   else
                                   {
                                       DISTANCE_SUFFIX=NSLocalizedString(@"KILOMETER", nil);
                                   }
                                   [_lblCarName setText:[typeDetail valueForKey:PARAM_CAR_NAME] withColor:[UIColor textColor]];
                                   [_lblBasePriceValue setText:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,[typeDetail valueForKey:PARAM_BASEPRICE],DISTANCE_SUFFIX ] withColor:[UIColor textColor]];
                                   [_lblDistancePriceValue setText:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,[typeDetail valueForKey:PARAM_DISTANCE_PRICE],DISTANCE_SUFFIX] withColor:[UIColor textColor]];
                                   [_lblTimePriceValue setText:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,[typeDetail valueForKey:PARAM_TIME_PRICE],TIME_SUFFIX] withColor:[UIColor textColor]];
                                   adminTypeId=[NSString stringWithFormat:@"%@",[providerData objectForKey:PARAM_ADMIN_TYPE_ID] ];
                                   serviceTypeId=[providerData objectForKey:PARAM_SERVICE_TYPE];
                                   @try
                                   {
                                       NSString *strImageUrl=[NSString stringWithFormat:@"%@%@",BASE_URL,[typeDetail valueForKey:PARAM_TYPE_IMAGE_URL] ];
                                       [_imgCar downloadFromURL:strImageUrl withPlaceholder:[UIImage imageNamed:@"user"]];
                                   }
                                   @catch (NSException *exception)
                                   {
                                       [_imgCar setImage:[UIImage imageNamed:@"car"]];
                                   }
                                   @finally
                                   {
                                       [_viewForCar setHidden:NO];
                                       [self getCountryAndCity:current_coordinate];
                                       [self isProviderOnline];
                                   }
                               }
                           }
                           [APPDELEGATE hideLoadingView];
                       });
		}];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgUserPic setRoundImageWithColor:[UIColor borderColor]];
}
-(void)isProviderOnline
{
    if (!PREF.isProviderActive)
    {
        [self.btnGoOnline setTitle:NSLocalizedString(@"GO_ONLINE", nil) forState:UIControlStateNormal];
        [self performSelectorOnMainThread:@selector(stopUpdateLocationTimer) withObject:nil waitUntilDone:YES];
        [_viewForOffline setHidden:NO];
        [_viewForCar setHidden:NO];
        [self.view bringSubviewToFront:_viewForOffline];
        [self.view bringSubviewToFront:_btnMyLocation];
        [self.view bringSubviewToFront:_btnGoOnline];
    }
    else
    {
        [self.btnGoOnline setTitle:[NSLocalizedString(@"GO_OFFLINE", nil) uppercaseString] forState:UIControlStateNormal];
        if (! [_timerForUpdateLocation isValid])
        {
            isTimerOnForGetTrip=YES;
            [self methodCallHandlier];
             [self performSelectorOnMainThread:@selector(startUpdateLocationTimer) withObject:nil waitUntilDone:YES];
            [_viewForOffline setHidden:YES];
        }
    }
    [APPDELEGATE hideLoadingView];
}
/*Get Time And Distance*/
-(void) getTimeAndDistance:(CLLocationCoordinate2D) src_location destLocation:(CLLocationCoordinate2D)dest_location
{
    NSString *src_lat=[NSString stringWithFormat:@"%f",src_location.latitude];
    NSString *src_long=[NSString stringWithFormat:@"%f",src_location.longitude];
    NSString *dest_lat=[NSString stringWithFormat:@"%f",dest_location.latitude];
    NSString *dest_long=[NSString stringWithFormat:@"%f",dest_location.longitude];
    NSString *strUrl = [NSString stringWithFormat:GoogleDistanceAPI,src_lat,src_long,dest_lat,dest_long,GoogleServerKey];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSData *jsonData = [NSData dataWithContentsOfURL:url];
    @try {
        if(jsonData != nil)
        {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
            NSString *status=[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_STATUS];
            if ([status isEqualToString:GOOGLE_PARAM_STATUS_OK])
            {
                NSString *tempDistance=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DISTANCE] valueForKey:GOOGLE_PARAM_VALUES];
                NSString *second=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DURATION] valueForKey:GOOGLE_PARAM_VALUES];
                NSString *tempTime=[[UtilityClass sharedObject]secondToTime:[second intValue]];
                tempDistance=[NSString stringWithFormat:@"%.2f",[[UtilityClass sharedObject]meterToKilometer:[tempDistance doubleValue]] ];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [_lblTimeValue setText:[NSString stringWithFormat:@"%@ %@",tempTime,TIME_SUFFIX]];
                                   [_lblDistanceValue setText:[NSString stringWithFormat:@"%@ %@",tempDistance,DISTANCE_SUFFIX]];
                                   [APPDELEGATE hideLoadingView];
                               });
                
            }
        }
    }
    @catch (NSException *exception)
    {
            
    }
    
}
#pragma mark-MAP AND CAMERA METHOD
#pragma mark - Google Map Delegate
-(void)setMarkerAndCamera:(CLLocationCoordinate2D)currentLatlong
{  [_mapView clear];
    driverMarker.position = currentLatlong;
    driverMarker.icon=[UIImage imageNamed:@"pin_driver"];
    driverMarker.map = _mapView;
    if(moveCamera)
    {
        GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:[strForCurLatitude doubleValue] longitude:[strForCurLongitude doubleValue] zoom:15];
        [_mapView setCamera:camera];
    }
}
-(void)setMarkersAndCamera:(CLLocationCoordinate2D)currentLatlong pickUpLatLong:(CLLocationCoordinate2D)pickUpLatlong destLatLong:(CLLocationCoordinate2D)destLatlong
{  [_mapView clear];
    driverMarker.position = currentLatlong;
    driverMarker.icon=[UIImage imageNamed:@"pin_driver"];
    driverMarker.map = _mapView;
    clientMarker=[[GMSMarker alloc] init];
    clientMarker.position = pickUpLatlong;
    clientMarker.icon=[UIImage imageNamed:@"pin_pickup"];
    clientMarker.map = _mapView;
    dest_marker=[[GMSMarker alloc] init];
    if (destLatlong.latitude !=0 && destLatlong.longitude !=0)
    {
        CLLocationCoordinate2D latLong;
        latLong.latitude = [[User sharedObject].destLatitude doubleValue];
        latLong.longitude = [[User sharedObject].destLongitude doubleValue];
        dest_marker.position = latLong;
        dest_marker.icon=[UIImage imageNamed:@"pin_destination"];
        dest_marker.map = _mapView;
    }
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    if (dest_marker.position.latitude)
    {bounds = [bounds includingCoordinate:dest_marker.position];}
    if (clientMarker.position.latitude)
    {bounds = [bounds includingCoordinate:clientMarker.position];}
    bounds = [bounds includingCoordinate:driverMarker.position];
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat: animationMapDelayed] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
    }];
   [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:80.0f]];
    [CATransaction commit];
}
-(void) mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
   if (gesture)
   {
       moveCamera=NO;
   }
}
-(void)mapViewSnapshotReady:(GMSMapView *)mapView
{
}

#pragma mark-Push Notificaiton
-(void)checkPush
{
    if (checkNewTrip)
    {
        isTimerOnForGetTrip=YES;
        [self performSelectorInBackground:@selector(wsGetTrip) withObject:nil];
        
        checkNewTrip=false;
    }
    else if (checkCancelTrip)
    {
        [self onCancelTrip];
        checkCancelTrip=false;
    }
    else if (checkApprove)
    {
        PREF.ProviderApproved=1;
        [APPDELEGATE goToMap];
    }
    else if(checkDecline)
    {
        PREF.ProviderApproved=0;
        viewForAdminAlert=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERET_TITLE_ADMIN_REVIEW", nil) message:NSLocalizedString(@"ALERET_MSG_ADMIN_REVIEW", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"LOGOUT", nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_EMAIL",nil)];
    }
    else
    {
        
    }
}
-(void)methodCallHandlier
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        [self wsUpdateLocation];
        if(isTimerOnForGetTrip)
        {
            [self wsGetTrip];
            
        }
        else
        {
            [self wsGetTripStatus];
            
        }
    });
}
#pragma mark-VisiTor Type Settings
-(void) getCountryAndCity:(CLLocationCoordinate2D )location2d
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:location2d.latitude longitude:location2d.longitude];
    userDefaultLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en_US", nil] forKey:@"AppleLanguages"];

    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *array, NSError *error){
                       if (error)
                       {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       CLPlacemark *placemark = [array objectAtIndex:0];
                       if (placemark.locality)
                       {
                           strForCity=placemark.locality;
                       }
                       else
                       {
                           if(placemark.administrativeArea)
                               strForCity=placemark.administrativeArea;
                           else
                               strForCity=placemark.subAdministrativeArea;
                       }
                       strForCountry=placemark.country;
                       [[NSUserDefaults standardUserDefaults] setObject:userDefaultLanguages forKey:@"AppleLanguages"];
                       
                       if (strForCountry==nil || strForCity == nil)
                       {
                           strForCity=@"";
                           strForCountry=@"";
                       }
                       
                       [self checkCity:providerRegisteredCity];
                   }];
}
-(void)checkCity:(NSString *)strRegisteredCity
{
    if ([strForCity isEqualToString:strRegisteredCity])
    {
    }
    else
    {
        [self getVehicalTypeInCurrentCity:strForCity];
    }
    
}
-(void)getVehicalTypeInCurrentCity:(NSString*)strCity
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:strForCountry forKey:PARAM_COUNTRY];
    [dictParam setObject:strForCity forKey:PARAM_CITY];
    [dictParam setObject:strForCurLatitude forKey:PARAM_LATITUDE];
    [dictParam setObject:strForCurLongitude forKey:PARAM_LONGITUDE];
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_GET_TYPELIST_SELETED_CITY withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
     {
         NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:kNilOptions
                                                                        error:nil];
         if (response)
         {
             if([[jsonResponse valueForKey:SUCCESS] intValue]==1)
             {
                 NSArray *carTypes=[jsonResponse valueForKey:PARAM_CITY_TYPES];
                 for (NSDictionary *carType in carTypes)
                 {
                     NSDictionary *typeDetail=[carType objectForKey:PARAM_TYPE_DETAILS];
                     if ([adminTypeId isEqualToString:[carType valueForKey:PARAM_TYPE_ID]]  && [serviceTypeId isEqualToString:[carType valueForKey:PARAM_ID]])
                     {
                         isTypeAvailable=TRUE;
                     }
                     else
                     {
                     if([adminTypeId isEqualToString:[carType valueForKey:PARAM_TYPE_ID]]  && ![serviceTypeId isEqualToString:[carType valueForKey:PARAM_ID]])
                     {
                         isTypeAvailable=TRUE;
                         dispatch_async(dispatch_get_main_queue(), ^{
                             serviceTypeId=[carType valueForKey:PARAM_ID];
                             visitorTypeDialog=[[CustomVisitorTypeDialog alloc]
                                                initWithTitle:NSLocalizedString(@"ALERT_TITLE_VISITOR_TYPE", nil)
                                                message:NSLocalizedString(@"ALERT_MSG_VISITOR_TYPE", nil)
                                                delegate:self
                                                typeName:[NSString stringWithFormat:@"%@",[typeDetail valueForKey:PARAM_CAR_NAME] ]
                                                baseFareCost:[NSString stringWithFormat:@"%@ %@",CurrencySign,[carType valueForKey:PARAM_BASEPRICE]]
                                                minFareCost:[NSString stringWithFormat:@"%@ %@",CurrencySign,[carType valueForKey:PARAM_MINFARE]]
                                                distanceCost:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,[carType valueForKey:PARAM_PRICE_PER_UNIT_DISTANCE],DISTANCE_SUFFIX]
                                                cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil)
                                                otherButtonTitles:NSLocalizedString(@"OK", nil)
                                                ];
                         });
                     }
                     }
                 }
                 if (!isTypeAvailable)
                 {
                     for (NSDictionary *carType in carTypes)
                     {
                         NSDictionary *typeDetail=[carType objectForKey:PARAM_TYPE_DETAILS];
                         NSString* isVisitType=[NSString stringWithFormat:@"%@",[typeDetail valueForKey:PARAM_SERVICE_TYPE]];
                         NSLog(@"%@",isVisitType);
                         if([isVisitType boolValue]&&![serviceTypeId isEqualToString:[carType valueForKey:PARAM_TYPE_ID]])
                         {
                             isTypeAvailable=TRUE;
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 serviceTypeId=[carType valueForKey:PARAM_ID];
                                 
                                 visitorTypeDialog=[[CustomVisitorTypeDialog alloc]
                                                    initWithTitle:NSLocalizedString(@"ALERT_TITLE_VISITOR_TYPE", nil)
                                                    message:NSLocalizedString(@"ALERT_MSG_VISITOR_TYPE", nil)
                                                    delegate:self
                                                    typeName:[NSString stringWithFormat:@"%@",[typeDetail valueForKey:PARAM_CAR_NAME] ]
                                                    baseFareCost:[NSString stringWithFormat:@"%@ %@",CurrencySign,[carType valueForKey:PARAM_BASEPRICE]]
                                                    minFareCost:[NSString stringWithFormat:@"%@ %@",CurrencySign,[carType valueForKey:PARAM_MINFARE]]
                                                    distanceCost:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,[carType valueForKey:PARAM_PRICE_PER_UNIT_DISTANCE],DISTANCE_SUFFIX]
                                                    cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil)
                                                    otherButtonTitles:NSLocalizedString(@"OK", nil)
                                                    ];
                             });
                             break;
                         }
                     }
                     if (!isTypeAvailable)
                     {
                         if (!viewForNoServiceProvided)
                         {
                             viewForNoServiceProvided=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_VISITOR_TYPE", nil) message:NSLocalizedString(@"ALERT_MSG_NO_SERVICE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES",nil)];
                         }
                     }
                     
                 }
             }
         }
     }];
}
-(void)onClickCustomVisitorDialogOk:(CustomVisitorTypeDialog *)view
{
    [self updateProviderType];
}
-(void)updateProviderType
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:strForCity forKey:PARAM_CITY];
    [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictParam setObject:serviceTypeId forKey:PARAM_TYPE_ID];
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_UPDATE_PROVIDER_TYPE withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error) {
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions error:nil];
                [APPDELEGATE hideLoadingView];
                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                {
                    [self wsGetCarDetail];
                    [APPDELEGATE goToMap];
                    return;
                }
                
            });
        }
        
    }];
}

#pragma mark- CUSTOM DELEGATE METHOD
-(void)onClickCustomSimpleDialogOk:(CustomSimpleAlert *)view
{
    
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_LOGOUT", nil)];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:PROVIDER_LOGOUT withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error) {
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[AppDelegate sharedAppDelegate]hideLoadingView];
                if([[Parser sharedObject]isLogoutSuccess:response] )
                {
                     [self performSelectorOnMainThread:@selector(stopUpdateLocationTimer) withObject:nil waitUntilDone:YES];
                    [APPDELEGATE goToMain];
                }
            });
        }
        
    }];
}
/*Dialog for Logout When  Provider is Not Approved*/
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    
    if(view==viewForAdminAlert)
    {
        [UtilityClass sendMailTo:PREF.contactEmail andSubject:@"title"];
    }
    else if(view==viewForPartnerAlert)
    {
        [UtilityClass sendMailTo:PREF.partnerEmail andSubject:@"title"];
    }
    else if(view==viewForNoServiceProvided)
    {
        exit(0);
    }
    else
    {
    
    }
    
}
-(void)onClickClose:(CustomAlertWithTitle *)view
{
    if (view==viewForAdminAlert || view==viewForPartnerAlert)
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_LOGOUT", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        [dictParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:PROVIDER_LOGOUT withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error) {
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[AppDelegate sharedAppDelegate]hideLoadingView];
                    if([[Parser sharedObject]isLogoutSuccess:response] )
                    {
                        [self performSelectorOnMainThread:@selector(stopUpdateLocationTimer) withObject:nil waitUntilDone:YES];                     [APPDELEGATE goToMain];
                    }
                });
            }
            
        }];
    }
    else if(view==viewForNoServiceProvided)
    {
        [[UtilityClass sharedObject] animateHide:viewForNoServiceProvided];
    }
    
}
-(void)rotateMarker:(CGFloat)degrees
{
    [_mapView animateToBearing:degrees];
    [_mapView animateToLocation:current_coordinate];

}

#pragma mark-counter
-(void) calculateProviderLocationChang
{
    
    if (current_coordinate.latitude)
    {
        if (!lastLocation.latitude)
        {
            lastLocation = current_coordinate;
            
        }
        if ([self getDistanceOfTwoPoints:current_coordinate toLatLong:lastLocation]<50)
        {
            countUpdate++;
        }
         else
        {
            countUpdate = 0;
        }
        if (counterForOffilineWarning == countUpdate)
        {
        }
        if (counterForMakeOffiline == countUpdate)
        {
            //[self clickGoOnline:nil];
        }
        
    }
}
-(double)getDistanceOfTwoPoints:(CLLocationCoordinate2D)fromLatlong toLatLong:(CLLocationCoordinate2D)toLatLong
{
    CLLocation *fromLocation= [[CLLocation alloc]initWithLatitude:fromLatlong.latitude longitude:fromLatlong.longitude];
    CLLocation *toLocation= [[CLLocation alloc]initWithLatitude:toLatLong.latitude longitude:toLatLong.longitude];
    CLLocationDistance distance=[fromLocation distanceFromLocation:toLocation];
    return distance;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}
#pragma mark- Timers



-(void)startAcceptRequestTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![_timerForAcceptRequest isValid])
        {
            _timerForAcceptRequest=nil;
            [_timerForAcceptRequest invalidate];
            _timerForAcceptRequest=[NSTimer scheduledTimerWithTimeInterval:1.0f
                                                                    target:self
                                                                  selector:@selector(changetitle)
                                                                  userInfo:nil
                                                                   repeats:YES];
        }
    });
}

-(void)startUpdateLocationTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _timerForUpdateLocation=[NSTimer scheduledTimerWithTimeInterval:10.0f
                                                                 target:self
                                                               selector:@selector(methodCallHandlier)
                                                               userInfo:nil
                                                                repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:_timerForUpdateLocation forMode:NSRunLoopCommonModes];
    });
}
-(void)stopAcceptRequestTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
		
    if ([_timerForAcceptRequest isValid ])
    {
        [_timerForAcceptRequest invalidate];
        _timerForAcceptRequest = nil;
        secondsLeft=SECONDS_REMAIN;
    }
    });
}

-(void)stopUpdateLocationTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{

    if ([_timerForUpdateLocation isValid])
    {
        [_timerForUpdateLocation invalidate];
        _timerForUpdateLocation = nil;
    }});
}

- (IBAction)onClickViewNote:(id)sender
{
	User *user = [User sharedObject];
	NSLog(@"Note = %@",user.note);
	
	UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",user.note] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	alert.tag = 3434;
	[alert show];
}
@end
