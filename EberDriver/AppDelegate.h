//
//  AppDelegate.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 12/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "MapVC.h"
#import "TripVC.h"
#import "AFNHelper.h"
@import Firebase;




//@import Firebase;
@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate,GIDSignInUIDelegate>
{
    UIView *view;
}

@property (strong, nonatomic) UIWindow *window;

/**Check weather given controller is already in stack or not*/
-(BOOL)isControllerAlreadyOnNavigationControllerStack:(id) myVc;
+(AppDelegate *)sharedAppDelegate;
@property (strong, nonatomic) MapVC *objMap;
@property (strong, nonatomic) TripVC *objTrip;
/**Display Loading View on Top of all screen*/
-(void)showLoadingWithTitle:(NSString *)title;
/**Hide Loading View*/
-(void)hideLoadingView;
/*
 *Check Internet Connectivity Of Mobile
 */
-(BOOL)connected;
/*
 *Restarts the Application
 */
-(void)goToMain;
-(void)goToMap;
- (NSString *)applicationCacheDirectoryString;
- (NSURL *)applicationDocumentsDirectory;
@end

