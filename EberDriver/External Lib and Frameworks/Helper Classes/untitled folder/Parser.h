//
//  Parser.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 12/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BankDetail.h"

@interface Parser : NSObject
- (BOOL)parseApiKeys:(NSString*)response;
- (BOOL)stringToJson:(NSString*)response To:(id*)json;
- (BOOL)isLoginSuccess:(NSString*)response;
- (BOOL)isProfileUpdated:(NSString*)response;
- (BOOL)getAllDocuments:(NSString*)response toArray:(NSMutableArray * __strong *)arrForDocuments;
- (BOOL)getDocument:(NSString*)response toString:(NSString**)strUrl;
- (BOOL)isLogoutSuccess:(NSString*)response;
- (BOOL)toggoleState:(NSString*)response;
- (BOOL)updateLocation:(NSString*)response;
- (BOOL)getTrip:(NSString*)response;
- (BOOL)getTripStatus:(NSString*)response;
- (BOOL)respondTrip:(NSString*)response;
- (BOOL)completeTrip:(NSString*)response;
- (BOOL)parseHistory:(NSString*)response toArray:(NSMutableArray * __strong *)arrForHistory;
- (BOOL)parseCities:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCities;
- (BOOL)parseVerificationTypes:(NSString*)response;
- (BOOL)parseServiceCountries:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCoutries;
- (BOOL)parseBankDetails:(NSString*)response toBankDetailObject:(BankDetail * __strong *)bankDetail;
- (BOOL)parseSessionToken:(NSString*)response;
-(BOOL)parseSuccess:(NSString*)response;
+(Parser *) sharedObject;

@end
