//
//  UserDefaults.m
//  FixatiSPS
//
//  Created by Disha Ladani on 24/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//


#import "PreferenceHelper.h"
#import "NSObject+Constants.h"
#define PREFERENCE [NSUserDefaults standardUserDefaults]
static NSString * const keyProviderID=@"providerID";
static NSString * const keyProviderLoginBy=@"providerLoginBy";
static NSString * const keyProviderSocialId=@"providerSocialID";
static NSString * const keyProviderToken=@"providerToken";
static NSString * const keyProviderFirstName=@"providerFirstName";
static NSString * const keyProviderLastName=@"providerLastName";
static NSString * const keyProviderPhone=@"providerPhone";
static NSString * const keyProviderEmail=@"providerEmail";
static NSString * const keyProviderPicture=@"providerPictureUrl";
static NSString * const keyProviderBio=@"providerBio";
static NSString * const keyProviderAddress=@"providerAddress";
static NSString * const keyProviderZipcode=@"providerZipcode";
static NSString * const keyTripId=@"tripId";
static NSString * const keyProviderCountryCode=@"providerCountryCode";
static NSString * const isDocumentUploaded=@"providerDocumentUploaded";
static NSString * const isProviderApproved=@"providerApproved";
static NSString * const isPartneApproved=@"partnerApproved";
static NSString * const keyProviderType=@"providerType";
static NSString * const isLogin=@"providerLogin";
static NSString * const isProviderActive=@"providerActive";
static NSString * const keyDeviceToken=@"deviceToken";
static NSString * const keyIsSoundOff=@"isSoundOff";
static NSString * const keyIsPickUpSoundOff=@"isPickUpSoundOff";
static NSString * const keyIsPartnerEmail=@"partnerEmail";


/*Google Keys and Settings*/
static NSString * const keyPublicStripeKey=@"publicStripeKey";
static NSString * const keyServerKey=@"googleServerKey";
static NSString * const keyIsEmailOtpOn=@"emailOtpOn";
static NSString * const keyIsSmsOtpOn=@"smsOtpOn";
static NSString * const  keyContactEmail=@"contactMailId";
static NSString * const  keyIsPathDraw=@"isPathDraw";
static NSString * const  keyCounter=@"counter";
static NSString * const  keyHotlineId=@"hotlineId";
static NSString * const  keyHotlineKey=@"hotlineKey";

@implementation PreferenceHelper
{
    
}

+ (PreferenceHelper *)sharedObject
{
    static PreferenceHelper *objPreference = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objPreference = [[PreferenceHelper alloc] init];
    });
    return objPreference;
}

#pragma Common Preference
-(void)setProviderId:(NSString *)providerId
{
   [PREFERENCE setObject:providerId forKey:keyProviderID];
   [PREFERENCE synchronize];
}
-(NSString*)providerId
{  return [PREFERENCE objectForKey:keyProviderID];}
-(void)setProviderToken:(NSString *)providerToken
{
    [PREFERENCE setObject:providerToken forKey:keyProviderToken];
    [PREFERENCE synchronize];
}
-(NSString*)providerToken
{  return [PREFERENCE objectForKey:keyProviderToken];}

#pragma Register Preference
-(void)setProviderFirstName:(NSString *)providerFirstName
{
    [PREFERENCE setObject:providerFirstName forKey:keyProviderFirstName];
    [PREFERENCE synchronize];
}
-(NSString*)providerFirstName
{  return [PREFERENCE objectForKey:keyProviderFirstName];}

-(void)setProviderLastName:(NSString *)providerLastName
{
    [PREFERENCE setObject:providerLastName forKey:keyProviderLastName];
    [PREFERENCE synchronize];
}
-(NSString*)providerLastName
{  return [PREFERENCE objectForKey:keyProviderLastName];}

-(void)setProviderEmail:(NSString *)providerEmail
{
    [PREFERENCE setObject:providerEmail forKey:keyProviderEmail];
    [PREFERENCE synchronize];
}
-(NSString*)providerEmail
{  return [PREFERENCE objectForKey:keyProviderEmail];}


-(void)setProviderPicture:(NSString *)providerPicture
{
    [PREFERENCE setObject:providerPicture forKey:keyProviderPicture];
    [PREFERENCE synchronize];
}
-(NSString*)providerPicture
{  return [PREFERENCE objectForKey:keyProviderPicture];}



-(void)setProviderZipcode:(NSString *)providerZipcode
{
    [PREFERENCE setObject:providerZipcode forKey:keyProviderZipcode];
    [PREFERENCE synchronize];
}
-(NSString*)providerZipcode
{  return [PREFERENCE objectForKey:keyProviderZipcode];}

-(void)setProviderAddress:(NSString *)providerAddress
{
    [PREFERENCE setObject:providerAddress forKey:keyProviderAddress];
    [PREFERENCE synchronize];
}
-(NSString*)providerAddress
{  return [PREFERENCE objectForKey:keyProviderAddress];}

-(void)setProviderBio:(NSString *)providerBio
{
    [PREFERENCE setObject:providerBio forKey:keyProviderBio];
    [PREFERENCE synchronize];
}
-(NSString*)providerBio
{  return [PREFERENCE objectForKey:keyProviderBio];}

-(void)setProviderPhone:(NSString *)providerPhone
{
    [PREFERENCE setObject:providerPhone forKey:keyProviderPhone];
    [PREFERENCE synchronize];
}
-(NSString*)providerPhone
{  return [PREFERENCE objectForKey:keyProviderPhone];}


-(void)setProviderCountryCode:(NSString *)providerCountryCode
{
    [PREFERENCE setObject:providerCountryCode  forKey:keyProviderCountryCode];
    [PREFERENCE synchronize];
}
-(NSString*)providerCountryCode
{  return [PREFERENCE objectForKey:keyProviderCountryCode];}


-(void)setProviderLoginBy:(NSString *)providerLoginBy
{
    [PREFERENCE setObject:providerLoginBy  forKey:keyProviderLoginBy];
    [PREFERENCE synchronize];
}
-(NSString*)providerLoginBy
{  return [PREFERENCE objectForKey:keyProviderLoginBy];}

-(void)setProviderSocialId:(NSString *)providerSocialId
{
    [PREFERENCE setObject:providerSocialId  forKey:keyProviderSocialId];
    [PREFERENCE synchronize];
}
-(NSString*)providerSocialId
{  return [PREFERENCE objectForKey:keyProviderSocialId];}

-(void)setProviderApproved:(BOOL) providerApproved
{
    [PREFERENCE setBool:providerApproved forKey:isProviderApproved];
    [PREFERENCE synchronize];
}
-(BOOL)ProviderApproved
{  return [PREFERENCE boolForKey:isProviderApproved];}


-(void)setProviderLogin:(BOOL) providerLogin
{
    [PREFERENCE setBool:providerLogin forKey:isLogin];
    [PREFERENCE synchronize];
}
-(BOOL)isProviderLogin
{  return [PREFERENCE boolForKey:isLogin];}


-(void)setProviderDocumentUploaded:(BOOL) providerDocumentUploaded
{
    [PREFERENCE setBool:providerDocumentUploaded forKey:isDocumentUploaded];
    [PREFERENCE synchronize];
}
-(BOOL)isProviderDocumentUploaded
{  return [PREFERENCE boolForKey:isDocumentUploaded];}


-(void)setProviderActive:(BOOL) providerActive
{
    [PREFERENCE setBool:providerActive forKey:isProviderActive];
    [PREFERENCE synchronize];
}
-(BOOL)isProviderActive
{  return [PREFERENCE boolForKey:isProviderActive];}
- (void) clearPreference
{
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)setProviderTripId:(NSString *)providerTripId
{
    [PREFERENCE setObject:providerTripId forKey:keyTripId];
    [PREFERENCE synchronize];
}
-(NSString*)providerTripId
{
 return [PREFERENCE objectForKey:keyTripId];
}


-(NSString*)deviceToken
{  return [PREFERENCE objectForKey:keyDeviceToken];}

-(void)setDeviceToken:(NSString*)deviceToken
{
    [PREFERENCE setObject:deviceToken forKey:keyDeviceToken];
    [PREFERENCE synchronize];
    
}
-(void)setSoundOff:(BOOL)soundOff
{
    [PREFERENCE setBool:soundOff forKey:keyIsSoundOff];
    [PREFERENCE synchronize];
}
-(BOOL)isSoundOff
{  return [PREFERENCE boolForKey:keyIsSoundOff];}
-(void)setPickUpSoundOff:(BOOL)soundOff
{
    [PREFERENCE setBool:soundOff forKey:keyIsSoundOff];
    [PREFERENCE synchronize];
}
-(BOOL)isPickUpSoundOff
{  return [PREFERENCE boolForKey:keyIsSoundOff];}


-(void)setEmailOtpOn:(BOOL)isEmailOtp
{
    [PREFERENCE setBool:isEmailOtp forKey:keyIsEmailOtpOn];
    [PREFERENCE synchronize];
}
-(BOOL)isEmailOtpOn
{  return [PREFERENCE boolForKey:keyIsEmailOtpOn];}

-(void)setSmsOtpOn:(BOOL)isSmsOtp
{
    [PREFERENCE setBool:isSmsOtp forKey:keyIsSmsOtpOn];
    [PREFERENCE synchronize];
    
}
-(BOOL)isSmsOtpOn
{ return [PREFERENCE boolForKey:keyIsSmsOtpOn];}
-(NSString*)stripeKey
{  return [PREFERENCE objectForKey:keyPublicStripeKey];}
-(void)setStripeKey:(NSString*)stripeKey
{   [PREFERENCE setObject:stripeKey forKey:keyPublicStripeKey];
    [PREFERENCE synchronize];
}


-(NSString*)serverKey
{  return [PREFERENCE objectForKey:keyServerKey];}
-(void)setServerKey:(NSString*)serverKey
{   [PREFERENCE setObject:serverKey forKey:keyServerKey];
    [PREFERENCE synchronize];
}
-(void)setContactEmail:(NSString*)contactMail
{
    [PREFERENCE setObject:contactMail forKey:keyContactEmail];
    [PREFERENCE synchronize];
}
-(NSString*)contactEmail
{
return [PREFERENCE objectForKey:keyContactEmail];
}
-(void)setIsPathDraw:(BOOL) isPathDraw
{
    [PREFERENCE setBool:isPathDraw forKey:keyIsPathDraw];
    [PREFERENCE synchronize];
    
}
-(BOOL)isPathDraw
{
    return [PREFERENCE boolForKey:keyIsPathDraw];
}

-(void)putCounterForLocation:(NSInteger)count
{
    [PREFERENCE setInteger:count forKey:keyCounter];
    [PREFERENCE synchronize];
}
-(NSInteger)checkCounterForLocation
{
   return [PREFERENCE integerForKey:keyCounter];
}

-(NSString*)hotLineAppKey
{  return [PREFERENCE objectForKey:keyServerKey];}
-(void)setHotlineAppKey:(NSString*)hotlineKey
{
    [PREFERENCE setObject:hotlineKey forKey:keyHotlineKey];
    [PREFERENCE synchronize];
}


-(NSString*)hotLineAppId
{  return [PREFERENCE objectForKey:keyHotlineId];}
-(void)setHotlineAppId:(NSString*)hotlineId
{
    [PREFERENCE setObject:hotlineId forKey:keyHotlineId];
    [PREFERENCE synchronize];
}


-(void)setPartnerEmail:(NSString*)partnerMail
{
    [PREFERENCE setObject:partnerMail forKey:keyIsPartnerEmail];
    [PREFERENCE synchronize];
}
-(NSString*)partnerEmail
{
    return [PREFERENCE stringForKey:keyIsPartnerEmail];
}

-(void)setPartnerApproved:(BOOL) partnerApproved
{
    [PREFERENCE setBool:partnerApproved forKey:isPartneApproved];
    [PREFERENCE synchronize];
}
-(BOOL)PartnerApproved
{  return [PREFERENCE boolForKey:isPartneApproved];}

-(NSInteger)ProviderType
{  return [PREFERENCE boolForKey:keyProviderType];}

-(void)setProviderType:(NSInteger) partnerApproved
{
    [PREFERENCE setBool:partnerApproved forKey:keyProviderType];
    [PREFERENCE synchronize];
}
@end
