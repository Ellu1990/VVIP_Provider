//
//  CustomAlertWithTitle.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "SelectImage.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import <AssetsLibrary/AssetsLibrary.h>




@implementation SelectImage
@synthesize delegate;


UIViewController *uv;

    
- (IBAction)onClickBtnCamera:(id)sender {
    [[UtilityClass sharedObject] animateHide:self];
    [self removeFromSuperview];
     [self takePhoto];
}
- (IBAction)onClickBtnGallary:(id)sender
{
    [[UtilityClass sharedObject] animateHide:self];
    
    [self removeFromSuperview];
    [self selectPhotos];
}



+(SelectImage *)getSelectImageViewwithParent:(id)parent {
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"SelectImage" owner:nil options:nil];
    SelectImage *view = [nibContents lastObject];
    view.parent = parent;
    view.center=APPDELEGATE.window.center;
    view=(SelectImage*)[[UtilityClass sharedObject]addShadow:view];
    view.delegate=parent;
    [view.lblCamera setText:[NSLocalizedString(@"CAMERA", nil) capitalizedString]];
    [view.lblGallery setText:[NSLocalizedString(@"GALLERY", nil) capitalizedString]];
    [view.lblCamera setTextColor:[UIColor labelTitleColor]];
    [view.lblGallery setTextColor:[UIColor labelTitleColor]];
    [view.viewForSelectImage setBackgroundColor:[UIColor buttonColor]];
    if([parent isKindOfClass:[UIViewController class]])
    {
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        view.frame=screenRect;
    }
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    return view;
}

/*Capture Image From Camera*/
-(void)takePhoto
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = _parent;
        imagePickerController.sourceType =UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing=YES;
        [_parent presentViewController:imagePickerController animated:YES completion:^{
        }];
        
    }
    else
    {
        [[UtilityClass sharedObject] displayAlertWithTitle:@"" andMessage:NSLocalizedString(@"ALERT_MSG_CAMERA_NOT_AVAILABLE", nil)];
    }
}

/* Fetch Photo From Gallery*/
- (void)selectPhotos
{
    // Set up the image picker controller and add it to the view
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = _parent;
    imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing=YES;
    [self.parent presentViewController:imagePickerController animated:YES completion:^{
    }];
}
/*Set Selected/Captured Image to Image View */


- (IBAction)onClickBtnOverLay:(id)sender {
    [[UtilityClass sharedObject] animateHide:self];
    
    [self removeFromSuperview];
}
@end
