//
//  UITextField+overtextfield.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 27/07/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "UITextField+overtextfield.h"
#import "Aspects.h"
#import "UIColor+Colors.h"

@implementation UITextField (overtextfield)
+ (void)load
{
    [[self class]aspect_hookSelector:@selector(awakeFromNib) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo) {
        UITextField* instance = [aspectInfo instance];
        /*UIFont* font = [UIFont fontWithName:@"Roboto-Regular_0" size:instance.font.pointSize];
        instance.font = font;
        [instance setAutocapitalizationType:UITextAutocapitalizationTypeWords];*/
        instance.textColor=[UIColor textColor];
        }error:nil];
}

-(void) setLeftPadding:(int) paddingValue
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void) setRightPadding:(int) paddingValue
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
    self.rightView = paddingView;
    self.rightViewMode = UITextFieldViewModeAlways;
}
-(void)setBorder
{
    self.layer.borderWidth=1.0f;
    self.layer.borderColor=[UIColor borderColor].CGColor;
    [self setLeftPadding:10];
    [self setRightPadding:10];
}


@end
