//
//  UserDefaults.h
//  FixatiSPS
//
//  Created by Disha Ladani on 24/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface PreferenceHelper : NSObject
+ (PreferenceHelper *)sharedObject;

-(void)setProviderId:(NSString *)newProviderId;
-(NSString*)providerId;


-(void)setProviderToken:(NSString *)providerToken;
-(NSString*)providerToken;


-(void)setProviderFirstName:(NSString *)providerFirstName;
-(NSString*)providerFirstName;

-(void)setProviderLastName:(NSString *)providerLastName;
-(NSString*)providerLastName;

-(void)setProviderEmail:(NSString *)providerEmail;
-(NSString*)providerEmail;

-(void)setProviderPicture:(NSString *)providerPicture;
-(NSString*)providerPicture;

-(void)setProviderZipcode:(NSString *)providerZipcode;
-(NSString*)providerZipcode;

-(void)setProviderAddress:(NSString *)providerAddress;
-(NSString*)providerAddress;

-(void)setProviderBio:(NSString *)providerBio;
-(NSString*)providerBio;

-(void)setProviderPhone:(NSString *)providerPhone;
-(NSString*)providerPhone;

-(void)setProviderCountryCode:(NSString *)providerCountryCode;
-(NSString*)providerCountryCode;

-(void)setProviderLoginBy:(NSString *)providerLoginBy;
-(NSString*)providerLoginBy;

-(void)setProviderSocialId:(NSString *)providerSocialId;
-(NSString*)providerSocialId;

-(void)setProviderTripId:(NSString *)providerTripId;
-(NSString*)providerTripId;

-(void)setDeviceToken:(NSString*)deviceToken;
-(NSString*)deviceToken;

-(void)setProviderApproved:(BOOL) providerApproved;
-(BOOL)ProviderApproved;
-(void)setProviderLogin:(BOOL) providerLogin;
-(BOOL)isProviderLogin;
-(void)setProviderDocumentUploaded:(BOOL) providerDocumentUploaded;
-(BOOL)isProviderDocumentUploaded;
-(void)setProviderActive:(BOOL) providerActive;
-(BOOL)isProviderActive;
-(void)setSoundOff:(BOOL)soundOff;
-(BOOL)isSoundOff;
-(void)setPickUpSoundOff:(BOOL)soundOff;
-(BOOL)isPickUpSoundOff;
-(void)setEmailOtpOn:(BOOL)isEmailOtp;
-(BOOL)isEmailOtpOn;
-(void)setSmsOtpOn:(BOOL)isSmsOtp;
-(BOOL)isSmsOtpOn;

-(void)setStripeKey:(NSString*)stripeKey;
-(NSString*)stripeKey;
-(void)setServerKey:(NSString*)serverKey;
-(NSString*)serverKey;
-(void)setContactEmail:(NSString*)contactMail;
-(NSString*)contactEmail;
-(void)setIsPathDraw:(BOOL) isPathDraw;
-(BOOL)isPathDraw;
-(void) clearPreference;
-(void)putCounterForLocation:(NSInteger)count;
-(NSInteger)checkCounterForLocation;
-(NSString*)hotLineAppKey;
-(void)setHotlineAppKey:(NSString*)hotlineKey;
-(NSString*)hotLineAppId;
-(void)setHotlineAppId:(NSString*)hotlineId;

-(void)setPartnerEmail:(NSString*)partnerMail;
-(NSString*)partnerEmail;

-(void)setPartnerApproved:(BOOL) partnerApproved;
-(BOOL)PartnerApproved;

-(NSInteger)ProviderType;
-(void)setProviderType:(NSInteger) partnerApproved;
@end
