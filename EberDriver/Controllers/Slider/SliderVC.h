//
//  SliderVC.h
//  Employee
//
//  Created by Elluminati - macbook on 19/05/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "MapVc.h"
#import "CustomAlertWithTitle.h"

@interface SliderVC : UIViewController<UITableViewDataSource,UITableViewDelegate,CustomAlertDelegate>
{
    UIViewController *frontVC;
	
    NSMutableArray *arrayMenuItems,*arrayMenuIcons;//Hold Menu Items
    NSMutableArray *arraySegueIdentifiers;//Hold Segue Identifiers
	
}
@property (weak, nonatomic) IBOutlet UIImageView *viewForBGimage;
@property(weak,nonatomic)IBOutlet UITableView *tblMenu;
@property (nonatomic,strong) MapVC *ViewObj;
@property (nonatomic, weak) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *viewForProfile;
@property (weak, nonatomic) IBOutlet UIView *gestureView;
@end
