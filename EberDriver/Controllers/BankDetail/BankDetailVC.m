//
//  ProfileVC.m
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//


#import "BankDetailVC.h"
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "UITextField+overtextfield.h"
#import "CountryCodeCell.h"
#import "Parser.h"
#import "BankDetail.h"
#import "PreferenceHelper.h"
#import "UIImageView+image.h"

@interface BankDetailVC ()
{
    CustomAlertWithTextInput *dialogForAccountVerification;
    BankDetail *bankDetail;
    NSMutableDictionary *dictBankParams;
    
}
@end

@implementation BankDetailVC
@synthesize activeTextField;

#pragma mark
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    dictBankParams=[[NSMutableDictionary alloc]init];
    bankDetail=[[BankDetail alloc]init];
    
    //[_scrollView addGestureRecognizer:tapGesture];
    [self SetLocalization];
    [self wsGetBankDetail];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:self.view.window];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
-(void)dealloc
{}
#pragma mark
#pragma mark - Action Methods

- (IBAction)onClickBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnUpdate:(id)sender
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    if ([PREF.providerLoginBy isEqualToString:GOOGLE] || [PREF.providerLoginBy isEqualToString:FACEBOOK] )
    {
            if([self checkValidation])
            {
                [self wsUpdateBankDetail:nil];
            }
    }
    else
    {
            if([self checkValidation])
            {
                if (dialogForAccountVerification==nil)
                {
                    dialogForAccountVerification=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_ACCOUNT_VERIFY", nil) placeHolder:NSLocalizedString(@"CURRENT_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) okButtonTitle:NSLocalizedString(@"ALERT_BTN_SEND", nil)];
                    [self.view bringSubviewToFront:dialogForAccountVerification];
                }
                else
                {
                    [self.view bringSubviewToFront:dialogForAccountVerification];
                    
                }
                
                
            }
    }
}
#pragma mark
#pragma mark - WEB SERVICE Methods

-(BOOL) checkValidation
{
        if(_txtAccountHolderName.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_ACCOUNT_HOLDER_NAME", nil)];
            return false;
        }
        else if(_txtAccountNumber.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_ACCOUNT_NUMBER", nil)];
            return false;
        }
        else if(_txtBankName.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_BANK_NAME", nil)];
            return false;
        }
        else if(_txtBranch.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_BANK_BRANCH", nil)];
            return false;
        }
        else if(_txtBankAddress.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_BANK_ADDRESS", nil)];
            return false;
        }
        else if(_txtUniqueCode.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_UNIQUE_CODE", nil)];
            return false;
        }
        else if(_txtSwiftCode.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_SWIFT_CODE", nil)];
            return false;
        }
        else
        {
        return true;
        }
    
}
-(void) wsUpdateBankDetail:(CustomAlertWithTextInput*)view
{
    [dictBankParams setObject:bankDetail.id_ forKey:PARAM_ID];
    [dictBankParams setObject:PREF.providerId forKey:PARAM_BANK_HOLDER_ID];
    [dictBankParams setObject:@"11" forKey:PARAM_BANK_HOLDER_ID];
    [dictBankParams setObject:_txtBankName.text forKey:PARAM_BANK_NAME];
    [dictBankParams setObject:_txtBranch.text forKey:PARAM_BANK_BRANCH];
    [dictBankParams setObject:_txtAccountNumber.text forKey:PARAM_BANK_ACCOUNT_NUMBER];
    [dictBankParams setObject:_txtAccountHolderName.text forKey:PARAM_BANK_ACCOUNT_HOLDER_NAME];
    [dictBankParams setObject:_txtBankAddress.text forKey:PARAM_BANK_BENEFICIARY_ADDRESS];
    [dictBankParams setObject:_txtUniqueCode.text forKey:PARAM_BANK_UNIQUE_CODE];
    [dictBankParams setObject:_txtSwiftCode.text forKey:PARAM_BANK_SWIFT_CODE];
    [self.view endEditing:YES];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATING_PROFILE", nil)];
    
    
        
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:WS_UPDATE_BANK_DETAIL  withParamData:dictBankParams withMethod:post withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                if ([[Parser sharedObject]parseSuccess:response])
                                {
                                    [self onClickBtnBack:nil];
                                    if (view)
                                    {
                                        [view removeFromSuperview];
                                    }
                                }
                                [APPDELEGATE hideLoadingView];
                            });
         }];
}

#pragma mark
#pragma mark - Text Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{  activeTextField=textField;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField=nil;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_txtAccountHolderName)
    {
        [_txtAccountNumber becomeFirstResponder];textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtAccountNumber)
    {
        [_txtBankName becomeFirstResponder];
    }

    else if(textField==_txtBankName)
    {
        [_txtBranch becomeFirstResponder];
    }
    else if(textField==_txtBranch)
    {
           [_txtBankAddress becomeFirstResponder];
    }
    else if(textField==_txtUniqueCode){
        [_txtUniqueCode becomeFirstResponder];
    }
    else if(textField==_txtSwiftCode)
    {
        [_txtBankAddress becomeFirstResponder];
    
    }
    else if(textField==_txtBankAddress)
    {
        textField.text = [textField.text capitalizedString];
        [textField resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)tapDetected
{
    [self.view endEditing:YES];
}
-(void)SetLocalization
{
    [_btnBack setTitle:NSLocalizedString(@"TITLE_BANK_DETAIL", nil) forState:UIControlStateNormal];
    [_viewForBankDetail setBackgroundColor:[UIColor backGroundColor]];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _viewForBankDetail.frame.size.height);
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    _txtAccountHolderName.placeholder=[NSLocalizedString(@"AC_HOLDER_NAME", nil) capitalizedString];
    _txtAccountNumber.placeholder=[NSLocalizedString(@"ACCOUNT_NUMBER", nil) capitalizedString];
    _txtBankName.placeholder=[NSLocalizedString(@"BANK_NAME", nil) capitalizedString];
    _txtBranch.placeholder=[NSLocalizedString(@"BANK_BRANCH", nil) capitalizedString];
    _txtBankAddress.placeholder=[NSLocalizedString(@"BANK_BEN_ADDRESS", nil) capitalizedString];
    _txtSwiftCode.placeholder=[NSLocalizedString(@"BANK_SWIFT_CODE", nil) capitalizedString];
    _txtUniqueCode.placeholder=[NSLocalizedString(@"BANK_UNIQUE_CODE", nil) capitalizedString];
    [_txtAccountHolderName setBorder];
    [_txtAccountNumber setBorder];
    [_txtBankName setBorder];
    [_txtBranch setBorder];
    [_txtBankAddress setBorder];
    [_txtSwiftCode setBorder];
    [_txtUniqueCode setBorder];
    
    
    [_txtAccountHolderName setTextColor:[UIColor textColor]];
    [_txtAccountNumber setTextColor:[UIColor textColor]];
    [_txtBankName setTextColor:[UIColor textColor]];
    [_txtBranch setTextColor:[UIColor textColor]];
    [_txtBankAddress setTextColor:[UIColor textColor]];
    [_txtSwiftCode setTextColor:[UIColor textColor]];
    [_txtUniqueCode setTextColor:[UIColor textColor]];
}
-(void)wsGetBankDetail
{
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_GET_BANKING_DETAIL",nil)];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:PREF.providerId forKey:PARAM_BANK_HOLDER_ID];
    [dictParam setObject:@"11" forKey:PARAM_BANK_HOLDER_TYPE];
    AFNHelper *afn=[[AFNHelper alloc]init];
    
    [afn getDataFromUrl:WS_GET_BANK_DETAIL withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            [[AppDelegate sharedAppDelegate]hideLoadingView];
                            
                            if([[Parser sharedObject]parseBankDetails:response toBankDetailObject:&bankDetail])
                            {
                                
                                @try
                                {
                                    [_txtAccountHolderName setText:bankDetail.bankAccountHolderName];
                                    [_txtAccountNumber     setText:bankDetail.bankAccountNumber];
                                    [_txtBankName          setText:bankDetail.bankName];
                                    [_txtBranch            setText:bankDetail.bankBranch];
                                    [_txtUniqueCode        setText:bankDetail.bankUniqueCode];
                                    [_txtSwiftCode         setText:bankDetail.bankSwiftCode];
                                    [_txtBankAddress       setText:bankDetail.bankBeneficiaryAddress];
                                }
                                @catch (NSException *exception)
                                {
                                        
                                }
                                
                            }
                            
                        });
     }];
    
    
}
-(void) wsAddBankDetail :(CustomAlertWithTextInput*)view
{
    [dictBankParams setObject:PREF.providerId forKey:PARAM_BANK_HOLDER_ID];
    [dictBankParams setObject:@"11" forKey:PARAM_BANK_HOLDER_TYPE];
    [dictBankParams setObject:_txtBankName.text forKey:PARAM_BANK_NAME];
    [dictBankParams setObject:_txtBranch.text forKey:PARAM_BANK_BRANCH];
    [dictBankParams setObject:_txtAccountNumber.text forKey:PARAM_BANK_ACCOUNT_NUMBER];
    [dictBankParams setObject:_txtAccountHolderName.text forKey:PARAM_BANK_ACCOUNT_HOLDER_NAME];
    [dictBankParams setObject:_txtBankAddress.text forKey:PARAM_BANK_BENEFICIARY_ADDRESS];
    [dictBankParams setObject:_txtUniqueCode.text forKey:PARAM_BANK_UNIQUE_CODE];
    [dictBankParams setObject:_txtSwiftCode.text forKey:PARAM_BANK_SWIFT_CODE];
    [self.view endEditing:YES];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATING_PROFILE", nil)];
    
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromUrl:WS_ADD_BANK_DETAIL  withParamData:dictBankParams withMethod:post withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            [APPDELEGATE hideLoadingView];
                            if ([[Parser sharedObject]parseSuccess:response])
                            {
                                [self onClickBtnBack:nil];
                                [view removeFromSuperview];
                            }
                        });
     }];
}

-(void)onClickOkButton:(NSString *)inputTextData view:(CustomAlertWithTextInput *)view
{
    if (inputTextData.length<6)
    {
        [[UtilityClass sharedObject] showToast:NSLocalizedString(@"TOAST_ENTER_PASSWORD", nil) ];
    }
    else
    {
        [dictBankParams setObject:inputTextData forKey:PARAM_PASSWORD];
        if ([UtilityClass isEmpty:bankDetail.id_ ])
        {
            [self wsAddBankDetail:view];
        }
        else
        {
            [self wsUpdateBankDetail:view];
        }
        
    };
}

// Called when UIKeyboardWillShowNotification is sent
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    if (activeTextField)
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGPoint pt;
        CGRect rc = [activeTextField bounds];
        rc = [activeTextField convertRect:rc toView:_scrollView];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= kbSize.height;
        CGRect mainRect=self.scrollView.frame;
        mainRect.size.height-=kbSize.height;
        if (!CGRectContainsRect(mainRect, rc))
        {
            [_scrollView setContentOffset:pt animated:YES];
        }
    }
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillHide:(NSNotification*)aNotification
{
    
}
- (void)hideKeyBoard
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgBank setRoundImageWithColor:[UIColor borderColor]];
    
}


@end
