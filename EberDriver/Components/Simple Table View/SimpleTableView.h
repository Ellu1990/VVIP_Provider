//
//  CustomAlertWithTitle.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SimpleTableViewDelegate <NSObject>
@optional
- (void) onCloseSimpleTableView;
- (void) onClickAddCity;
@required
- (void) onDidSelectItem:(NSString*)item;
@end

@interface SimpleTableView: UIView <UITableViewDelegate,UITableViewDataSource>
{ BOOL isSearching;}

@property (weak, nonatomic) IBOutlet UIView *alertView;
-(instancetype)initWithTitle:(NSString*)title dataSource:(NSMutableArray *)arrList delegate:(id)delegate;
@property(strong,nonatomic)id parent;
@property(copy,nonatomic) NSMutableArray *arrForList;
@property(copy,nonatomic) NSMutableArray *arrForFilteredList;
@property (nonatomic, assign) id<SimpleTableViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblCity;
@property (weak, nonatomic) IBOutlet UIButton *btnAddYourCity;
@property (weak, nonatomic) IBOutlet UISearchBar *searchCity;
- (IBAction)onClickAddCity:(id)sender;

@end
