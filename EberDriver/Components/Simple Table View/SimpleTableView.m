//
//  CustomAlertWithTitle.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "SimpleTableView.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
@implementation SimpleTableView

-(instancetype)initWithTitle:(NSString*)title dataSource:(NSMutableArray *)arrList delegate:(id)delegate
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableView" owner:nil options:nil];
        self = [nibContents lastObject];
        
        
        self.delegate = delegate;
        UIFont *font=[UIFont fontWithName:@"Roboto-Bold" size:_lblTitle.font.pointSize];
        [_lblTitle setFont:font];
        [_lblTitle setTextColor:[UIColor GreenColor]];
        self.frame=APPDELEGATE.window.frame;
        self.alertView.center=self.center;
        _arrForList=[[NSMutableArray alloc]init];
        _arrForFilteredList=[[NSMutableArray alloc]init];
        if ([arrList isKindOfClass:[NSMutableArray class]])
        {
            _arrForList=arrList;
        }
        [_tblCity reloadData];
        self.alertView=[[UtilityClass sharedObject]addShadow:self.alertView];
        self.backgroundColor=[UIColor clearColor];
        self.alertView.backgroundColor= [UIColor whiteColor];
        [self.lblTitle setText:title];
        [_btnAddYourCity setBackgroundColor:[UIColor buttonColor]];
        [_btnAddYourCity setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        return self;
    }
    return self;
}
- (IBAction)onClickCloseView:(id)sender {
    [self removeFromSuperview];
 }
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching)
        return _arrForFilteredList.count;
    else
        return _arrForList.count;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableCell"];
    if (isSearching)
        cell.textLabel.text= [_arrForFilteredList objectAtIndex:[indexPath row]];
    else
        cell.textLabel.text= [_arrForList objectAtIndex:[indexPath row]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSString *strSelectedItem;
    if(isSearching)
    {
        strSelectedItem=[_arrForFilteredList objectAtIndex:[indexPath row]];
    }
    else
    {
        strSelectedItem=[_arrForList objectAtIndex:[indexPath row]];
    }
    [_delegate onDidSelectItem:strSelectedItem];
    [self removeFromSuperview];
}
#pragma  mark Search Country;
- (void)searchBar:(UISearchBar *)SearchBar textDidChange:(NSString *) searchText {
     if (searchText.length == 0)
    
     {
         isSearching = NO;
     }
     else
     {
         isSearching = YES;
         _arrForFilteredList=[[NSMutableArray alloc]init];
         for (NSString *strCellData in _arrForList)
            {
                if ([strCellData hasPrefix:[searchText capitalizedString]] )
                {
                    [_arrForFilteredList addObject:strCellData];
                }
            }
            
    }
    [_tblCity reloadData];
}

- (IBAction)onClickAddCity:(id)sender {
    [_delegate onClickAddCity];
    [self removeFromSuperview];
}
@end
