//
//  AFNHelper.m
//  Tinder
//
//  Created by Elluminati - macbook on 04/04/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import "AFNHelper.h"
#import "AFNetworking.h"
#import "NSObject+Constants.h"
#import "AFHTTPSessionManager.h"
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "PreferenceHelper.h"
#import "Parser.h"
@implementation AFNHelper
{
}


#pragma mark -
#pragma mark - Methods
@synthesize networkDialog;
-(void)onClickClose:(CustomAlertWithTitle *)view
{
    [[UtilityClass sharedObject] animateHide:networkDialog];
    
}
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    if (view==networkDialog)
    {
        [[UtilityClass sharedObject] animateHide:networkDialog];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}
-(void)getDataFromUrl:(NSString *)path withParamData:(NSMutableDictionary *)postData  withMethod:(NSString*)method withBlock:(RequestCompletionBlock)block
{
    if ([APPDELEGATE connected])
    {   if (block)
        {dataBlock=[block copy];}
        if ([method isEqualToString:post] || [method isEqualToString:put]||[method isEqualToString:del])
        {
	    NSString *url=[NSString stringWithFormat:@"%@%@",BASE_URL,path];
	    NSLog(@"Final Url:%@",url);
       // NSLog(@"Final Parameters:%@",postData);
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
	    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSError *error;
            
		NSLog(@"post data:%@",postData);
        if (postData)
        {
	    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData options:NSJSONWritingPrettyPrinted error:&error];
	    
	    [urlRequest setHTTPMethod:method];
	    [urlRequest setHTTPBody:jsonData];
	    
	    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
	    
	    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
	    
	    [urlRequest setHTTPBody:jsonData];
    
	    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
	    {
            if(response!=nil)
		    {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                
                if ([httpResponse statusCode]==200)
                {
                    if (error==nil)
                    {
                        if (dataBlock)
                        {
                            if(data!=nil)
                            {
                                NSString *responseStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                if ([[Parser sharedObject]parseSessionToken:responseStr])
                                {
                                    dataBlock(responseStr,nil);
                                }
                            }
                            else
                            {
                                NSLog(@"Data Response is Null");
                            }
                        }
                        else
                        {
                            NSLog(@"DataBlock Not Initiate");
                        }
                    }
                    else
                    {
                        
                        if (dataBlock)
                            dataBlock(nil,error);
                        else
                            NSLog(@"DataBlock Not Initiate");
                    }

                }
                else
                {
                    [APPDELEGATE hideLoadingView];
                switch ([httpResponse statusCode])
                {
                    case 404:
                        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_404", nil)];
                        
                        break;
                    case 408:
                        
                        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_408", nil)];
                        
                        break;
                    case 413:
                        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_413", nil)];
                        break;
                    case 500:
                        
                        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_500", nil)];
                    case 502:
                        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_502", nil)];
                        
                        break;
                        
                    case 503:
                        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_503", nil)];
                        break;
                        
                    case 504:
                        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_504", nil)];
                        break;
                    default:
                        break;
                    }
                }
                
            }
		    else
		    {
			    
                [APPDELEGATE hideLoadingView];
                [[UtilityClass sharedObject]showToast:NSLocalizedString(@"Could not connect to the server", nil)];
            }
			    
		    }];
	    [postDataTask resume];
        }
        else
        {
            NSLog(@"No Data Set");
        }

        }
        else
        {
            NSString *url=[NSString stringWithFormat:@"%@%@",BASE_URL,path];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
            {
                NSError *writeError = nil;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject options:NSJSONWritingPrettyPrinted error:&writeError];
                NSString *result = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                dataBlock(result,nil);
                
            }
                failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
            }];
        }
        
    }
    else
    {
        [APPDELEGATE hideLoadingView];
        if (networkDialog==nil)
        {
            networkDialog=[[CustomAlertWithTitle alloc]
                           initWithTitle:NSLocalizedString(@"ALERT_TITLE_NETWORK_STATUS", nil)
                           message:NSLocalizedString(@"ALERT_MSG_NO_INTERNET", nil)
                           delegate:APPDELEGATE
                           cancelButtonTitle:NSLocalizedString(@"NO", nil)
                           otherButtonTitles:NSLocalizedString(@"YES", nil)];
            
        }
    }
}


@end
