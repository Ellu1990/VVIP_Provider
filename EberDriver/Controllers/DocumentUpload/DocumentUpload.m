//
//  DocumentUpload.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 16/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "DocumentUpload.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "PreferenceHelper.h"
#import "DocumentCell.h"
#import "Document.h"
#import "Parser.h"
#import "UtilityClass.h"
#import "UIColor+Colors.h"
#import "SelectImage.h"
#import "CustomAlertWithTitle.h"
#import "UILabel+overrideLabel.h"

@interface DocumentUpload ()
{
    NSIndexPath *selectedIndex;
    CustomAlertWithTitle *logoutView;
}
@end

@implementation DocumentUpload
@synthesize arrForDocuments,parentIsRegister;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_btnNavigation setTitle:NSLocalizedString(@"TITLE_DOCUMENTS", nil) forState:UIControlStateNormal];
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    arrForDocuments=[[NSMutableArray alloc]init];
    if(IS_DOCUMENT_PARENT_IS_REGISTER)
    {
        _viewForNavigation.hidden=NO;
        [_viewForNavigation setBackgroundColor:[UIColor GreenColor]];
        [self.navigationController.navigationBar setHidden:YES];
    }
    else
    {
        _viewForNavigation.hidden=YES;
        [self.navigationController.navigationBar setHidden:NO];
    }
    [self wsGetAllDocument];
    self.view.backgroundColor=[UIColor backGroundColor];
    _tblForDocument.backgroundColor=[UIColor backGroundColor];
    [_btnSubmit setBackgroundColor:[UIColor buttonColor]];
    [_btnSubmit setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    _btnSubmit=(UIButton*)[[UtilityClass sharedObject]addShadow:_btnSubmit];
    if (PREF.isProviderDocumentUploaded) {
        _btnSubmit.enabled=YES;
    }
    [_btnSubmit setTitle:[NSLocalizedString(@"BTN_SUBMIT", nil) uppercaseString] forState:UIControlStateNormal];
    [_lblTakePictureTitle setText:NSLocalizedString(@"TAKE_PICTURE_AND_UPLOAD_FROM_GALLERY", nil)];
    [_lblTitle setText:NSLocalizedString(@"DOCUMENT", nil) withColor:[UIColor labelTitleColor]];
    
}
- (IBAction)onClickMenu:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)wsGetAllDocument
{
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_GETIING_DOCUMENTS",nil)];
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        
        [afn getDataFromUrl:GET_DOCUMENTS withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                [APPDELEGATE hideLoadingView];
                                if([[Parser sharedObject]getAllDocuments:response toArray:&arrForDocuments])
                                {
                                    if (arrForDocuments.count)
                                    {
                                        Boolean isnotupload = NO;
                                        for (Document *temp in arrForDocuments)
                                        {
                                            if(!temp.isUploaded && !temp.isOptional)
                                                isnotupload=YES;
                                        }
                                        if (!isnotupload)
                                        {
                                            PREF.ProviderDocumentUploaded=YES;
                                            [_btnSubmit setEnabled:YES];
                                        }

                                        [_tblForDocument reloadData];
                                    }
                                    else
                                    {
                                        PREF.ProviderDocumentUploaded=YES;
                                        [APPDELEGATE goToMap];
                                        return;
                                    }
                                }
                                
                            });
			}];
}
#pragma mark-TableViewDelegateMethods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForDocuments.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"customCell";
    DocumentCell *cell = (DocumentCell* )[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
       cell=[[DocumentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    [cell setCellData:[arrForDocuments objectAtIndex:[indexPath row]]];
   // cell.contentView.backgroundColor=[UIColor backGroundColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    SelectImage *view=[SelectImage getSelectImageViewwithParent:self];
    [view bringSubviewToFront:self.parentViewController.view];
    selectedIndex=indexPath;
}
#pragma -
#pragma mark Image picker delegate methdos
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPLOADING_DOCUMENT",nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        NSString *strImg=[self encodeToBase64String:image];
        if (strImg)
        {
        [dictParam setObject:strImg forKey:PARAM_PICTURE_DATA];
        [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
        Document *d=[arrForDocuments objectAtIndex:selectedIndex.row];
        AFNHelper *afn=[[AFNHelper alloc]init];
        NSString *url=[NSString stringWithFormat:@"%@%@",UPLOAD_DOCUMENT,d.providerDocumentId];
        [afn getDataFromUrl:url withParamData:dictParam withMethod:put withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSString *str;
                                if([[Parser sharedObject]getDocument:response toString:&str])
                                {
                                    
                                    d.providerDocumenUrl=str;
                                    d.isUploaded=YES;
                                    [_tblForDocument reloadData];
                                    [arrForDocuments replaceObjectAtIndex:selectedIndex.row withObject:d];
                                    [_tblForDocument reloadRowsAtIndexPaths:@[selectedIndex] withRowAnimation:UITableViewRowAnimationNone];
                                    Boolean isnotupload = NO;
                                    for (Document *temp in arrForDocuments)
                                    {
                                        if(!temp.isUploaded && !temp.isOptional)
                                            isnotupload=YES;
                                    }
                                    if (!isnotupload)
                                    {
                                        PREF.ProviderDocumentUploaded=YES;
                                        [_btnSubmit setEnabled:YES];
                                    }
                                }
                                [APPDELEGATE hideLoadingView];
                            });
             
         }];
        }
        else
        {
            [APPDELEGATE hideLoadingView];
        }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (NSString *)encodeToBase64String:(UIImage *)image
{
    
    NSData *data=UIImageJPEGRepresentation(image, 0.5);
    NSString *str=[data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return str;
}
#pragma -
#pragma mark Logout methdos
- (IBAction)onClickBack:(id)sender
{
    logoutView=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"LOGOUT",nil) message:NSLocalizedString(@"ALERT_MSG_LOGOUT",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO",nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES",nil)];
    [logoutView bringSubviewToFront:self.parentViewController.view];
    
}
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    [[UtilityClass sharedObject] animateHide:view];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_LOGOUT", <#comment#>)];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
    
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:PROVIDER_LOGOUT withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
        {
                dispatch_async(dispatch_get_main_queue(), ^
                {
                    [[AppDelegate sharedAppDelegate]hideLoadingView];
                    if([[Parser sharedObject]isLogoutSuccess:response])
                        [APPDELEGATE goToMain];
                });
        }];
   

}

@end
