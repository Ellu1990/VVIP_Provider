//
//  HistoryDetailVC.m
//  Rider Driver
//
//  Created by My Mac on 7/8/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "HistoryDetailVC.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "Trip.h"
#import "UtilityClass.h"
#import "PreferenceHelper.h"
#import "UIColor+Colors.h"
#import "UIImageView+image.h"
#import "UILabel+overrideLabel.h"
#import "Parser.h"
#import "invoice.h"

@interface HistoryDetailVC ()
{
    NSString *strForCancelReason;
    BOOL _Authenticated;
    NSURLRequest *_FailedRequest;
    NSURLConnection * connection;
    NSURLRequest *requestURL;
    NSString* webStringURL;
}
@end

@implementation HistoryDetailVC
@synthesize currency,distanceUnit;
-(void)viewDidLoad{
    [super viewDidLoad];
    [_lblFrom setTextColor:[UIColor textColor]];
    [_lblTo setTextColor:[UIColor textColor]];
    [_lblYourEarning setTextColor:[UIColor buttonColor]];
    [_lblTripDistance setText:[NSString stringWithFormat:@"0 %@",distanceUnit]];
    [_lblTripDuration setText:[NSString stringWithFormat:@"0 %@",TIME_SUFFIX]];
    [_lblDestAddress setContentMode:UIViewContentModeTop];
    [_lblSrcAddress setContentMode:UIViewContentModeTop];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_HISTORY_DETAIL",nil)];
    _viewForHistory=[self addShadow:_viewForHistory];
    [self callwebservice];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    _btnInvoice.layer.cornerRadius = 5; // this value vary as per your desire
    _btnInvoice.clipsToBounds = YES;
    [_btnInvoice setHidden:YES];
    [self.lblTripNo setText:NSLocalizedString(@"TRIP_ID", nil)];
    [self.lblTripNo setTextColor:[UIColor labelTextColor]];
    [self.lblTripNoValue setTextColor:[UIColor textColor]];
    [_btnInvoice setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnInvoice setBackgroundColor:[UIColor buttonColor]];
    [_btnInvoice setTitle:[NSLocalizedString(@"INVOICE", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnNavigation setTitle:[NSLocalizedString(@"TITLE_HISTORY_DETAIL", nil) capitalizedString] forState:UIControlStateNormal];
    [_lblYourEarning setText:NSLocalizedString(@"YOUR_EARNING",nil )];
    [_btnNavigation setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];

}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [_imgProvider setRoundImageWithColor:[UIColor borderColor]];
    
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
-(IBAction)onClickBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)callwebservice{
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
	    NSString * strForUserId=PREF.providerId;
		NSString * strForUserToken=PREF.providerToken;
		NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		[dictParam setObject:strForUserId forKey:PARAM_PROVIDER_ID];
		[dictParam setObject:strForUserToken forKey:PARAM_PROVIDER_TOKEN];
		[dictParam setObject:_trip_id forKey:PARAM_TRIP_ID];
	
		AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:PROVIDER_TRIP_DETAIL withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
            {
            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
             NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             if (response)
             {
                 if([[jsonResponse valueForKey:SUCCESS] intValue]==1)
                 {
                     NSDictionary *tripJson= [jsonResponse objectForKey:PARAM_TRIP];
                      if ([[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_IS_TRIP_COMPLETED] ]boolValue])
                     {
                         if ([[Parser sharedObject]completeTrip:response])
                         {
                             [_btnInvoice setHidden:NO];
                         }
                     }
                     else
                     {
                         BOOL cancelationCharge=[[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_IS_CANCELLATION_FEE] ]boolValue];
                         if(cancelationCharge)
                         {
                             if ([[Parser sharedObject]completeTrip:response])
                             {
                                 [_btnInvoice setHidden:NO];
                             }
                             else
                             {
                                 [_btnInvoice setHidden:YES];
                             }
                         }
                         else
                         {
                             [_btnInvoice setHidden:YES];
                         }
                     }
                     NSString *strProviderEarning=[tripJson valueForKey:PARAM_PROVIDER_SERVICE_FEES];
                     NSString *strTime=[tripJson valueForKey:PARAM_USER_CREATE_TIME];
                     _tripDate=[[UtilityClass sharedObject]stringToDate:strTime withFormate:DATE_TIME_FORMAT_WEB];
                     _lblTripTime.text=[NSString stringWithFormat:@"%@",[[UtilityClass sharedObject] DateToString:_tripDate withFormate:TIME_FORMAT_AM]];
                     NSString *strDuration=[tripJson valueForKey:PARAM_TOTAL_TIME];
                     NSDictionary *tripJsonUser= [jsonResponse objectForKey:PARAM_USER];
                     NSString *url=[tripJsonUser valueForKey:PARAM_PICTURE];
                     [self.imgProvider downloadFromURL:url withPlaceholder:[UIImage imageNamed:@"user"]];
                     self.lblTripDistance.text=[NSString stringWithFormat:@"%@ %@",[tripJson valueForKey:PARAM_TOTAL_DISTANCE],distanceUnit];
                     self.lblSrcAddress.text=[tripJson valueForKey:PARAM_TRIP_SOURCE_ADDRESS];
                     self.lblDestAddress.text=[tripJson valueForKey:PARAM_TRIP_DESTINATION_ADDRESS];
                     self.lblTripCost.text=[NSString stringWithFormat:@"%@ %@",currency,[tripJson valueForKey:PARAM_TOTAL]];
                     self.lblEarningValue.text=[NSString stringWithFormat:@"%@ %@",currency,strProviderEarning];
                     [self setDate];
                     [APPDELEGATE hideLoadingView];
                     _lblTripDuration.text= [NSString stringWithFormat:@"%@ %@",strDuration,TIME_SUFFIX];
                      self.lblProviderNAme.text=[tripJsonUser valueForKey:PARAM_FIRST_NAME];
                     self.lblTripNoValue.text=[NSString stringWithFormat:@"%@",[tripJsonUser valueForKey:PARAM_TRIP_NUMBER]];
                     
                     [_lblSrcAddress setFontForLabel:_lblSrcAddress withMaximumFontSize:14 andMaximumLines:2];
                      [_lblDestAddress setFontForLabel:_lblDestAddress withMaximumFontSize:14 andMaximumLines:2];
                      CGFloat pointSize=MIN(_lblSrcAddress.font.pointSize, _lblDestAddress.font.pointSize);
                      UIFont *font=[UIFont fontWithName:_lblSrcAddress.font.fontName size:pointSize];
                      _lblSrcAddress.font=_lblDestAddress.font=font;
                     [_lblDestAddress sizeToFit];
                     [_lblSrcAddress sizeToFit];
                     url=[jsonResponse valueForKey:PARAM_MAP_IMAGE];
                     webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                     NSURL *urlAddress = [NSURL URLWithString:webStringURL];
                     requestURL = [NSURLRequest requestWithURL:urlAddress];
                     [_webView loadRequest:requestURL];
                     
                 }
                 else
                 {
                     [[UtilityClass sharedObject]displayAlertWithTitle:@"" andMessage:NSLocalizedString([jsonResponse valueForKey:ERROR_CODE],nil)];
                 }
                 [APPDELEGATE hideLoadingView];
             }
            });
		 }];
}
-(void)setDate
{
    NSString *strDate=[[UtilityClass sharedObject]DateToString:_tripDate];
    strDate=[strDate substringToIndex:10];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] ];
    current=[current substringToIndex:10];
    
    ///   YesterDay Date Calulation
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday];
    strYesterday=[strYesterday substringToIndex:10];
    
    
    
    if([strDate isEqualToString:current])
    {
        [_lblTripDate setText:[NSString stringWithFormat:@"  %@  ",NSLocalizedString(@"TODAY", nil)]];
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        
        [_lblTripDate setText:[NSString stringWithFormat:@"  %@  ",NSLocalizedString(@"YESTER_DAY", nil)]];
        
    }
    else
    {
        NSString *text=[[UtilityClass sharedObject]DateToString:_tripDate withFormateSufix:@"d MMMM yyyy"];
        _lblTripDate.text= [NSString stringWithFormat:@"  %@  ",text];
    }
    _lblTripDate.layer.cornerRadius = 10;
    _lblTripDate.layer.masksToBounds = YES;
    
    _lblTripDate.textColor=[UIColor whiteColor];
    _lblTripDate.backgroundColor=[UIColor GreenColor];
    [_lblTripDate sizeToFit];
    [_lblTripDate setFrame:CGRectMake(_lblTripDate.frame.origin.x,_lblTripDate.frame.origin.y, _lblTripDate.frame.size.width,30)];
}
#pragma UIWebViewDelegate
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request   navigationType:(UIWebViewNavigationType)navigationType {
    BOOL result = _Authenticated;
    if (!_Authenticated) {
        _FailedRequest = requestURL;
        
        connection = [[NSURLConnection alloc]
                      initWithRequest:request
                      delegate:self startImmediately:NO];
        
        [connection scheduleInRunLoop:[NSRunLoop mainRunLoop]
                              forMode:NSDefaultRunLoopMode];
        [connection start];
        [self.view bringSubviewToFront:_webView];
    }
    return result;
}
#pragma NSURLConnectionDelegate
-(void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        NSURL* baseURL = [NSURL URLWithString:webStringURL];
        if ([challenge.protectionSpace.host isEqualToString:baseURL.host]) {
            NSLog(@"trusting connection to host %@", challenge.protectionSpace.host);
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        }
        else
            NSLog(@"Not trusting connection to host %@", challenge.protectionSpace.host);
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}
-(void)connection:(NSURLConnection *)connections didReceiveResponse:(NSURLResponse *)pResponse {
    _Authenticated = YES;
    [connections cancel];
    [self.webView loadRequest:_FailedRequest];
}
-(UIView*)addShadow:(UIView*)view{
    view.layer.shadowOpacity=0.6;
    view.layer.shadowOffset= CGSizeMake(0, 2.0f);
    view.layer.shadowColor = [UIColor textColor].CGColor;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
}

- (IBAction)onClickBtnInvoice:(id)sender
{
    if(_viewForInvoice.isHidden)
    {
        [_btnInvoice setTitle:NSLocalizedString(@"DETAIL", nil) forState:UIControlStateNormal];
        [self addShadow:self.viewForDiscounts];
        [_viewForInvoice setHidden:NO];
        [self setLocalization];
        [self setInvoiceData];
    }
    else
    {
        [_viewForInvoice setHidden:YES];
             [_btnInvoice setTitle:NSLocalizedString(@"INVOICE", nil) forState:UIControlStateNormal];
    }
}
-(void)setInvoiceData
{
    Invoice *invoice=[Invoice sharedObject];
    [_lblInvoiceId setText:invoice.invoiceNumber];
    if ([invoice.paymentMode boolValue])
    {
        [_imgPayment setImage:[UIImage imageNamed:@"invoice_cash"]];
        [_lblPayment setText:NSLocalizedString(@"PAY_BY_CASH", nil)];
        [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"CASH_REMAINING_AMOUNT", nil), invoice.cashPayment]];
     }
    else
    {
        [_imgPayment setImage:[UIImage imageNamed:@"invoice_cash"]];
        [_lblPayment setText:NSLocalizedString(@"PAY_BY_CARD", nil)];
        if ([invoice.remainingPayment doubleValue]>0.00)
        {
            [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"REMAINING_AMOUNT", nil), invoice.remainingPayment]];
        }
        else
        {
            [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"CARD_REMAINING_AMOUNT", nil), invoice.cardPayment]];
            
        }
    }
    [_lblWalletAmount setText:[NSString stringWithFormat:NSLocalizedString(@"WALLET_PAYMENT", nil), invoice.walletPayment]];
    [_lblTotalValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.total]];
    [_lblTime setText:[NSString stringWithFormat:@"%@ %@", invoice.time,TIME_SUFFIX]];
    [_lblDistance setText:[NSString stringWithFormat:@"%@ %@", invoice.distance,DISTANCE_SUFFIX]];
    [_lblBaseCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.basePrice]];
    NSString *strTitle,*strSubValue;
    strTitle=NSLocalizedString(@"BASE_PRICE", nil);
    if ([invoice.basePriceDistance intValue]==1)
    {
        strSubValue=[NSString stringWithFormat:@"%@",invoice.distanceUnit];
    }
    else
    {
        strSubValue=[NSString stringWithFormat:@"%@ / %@",invoice.basePriceDistance,invoice.distanceUnit];
    }
    [_lblBaseCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    strTitle=NSLocalizedString(@"DISTANCE_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@ / %@",invoice.currency,invoice.pricePerUnitDistance,invoice.distanceUnit];
    [_lblDistanceCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblDistanceCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.distanceCost]];
    strTitle=NSLocalizedString(@"TIME_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@ / %@",invoice.currency,invoice.pricePerUnitTime,TIME_SUFFIX];
    [_lblTimeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblTimeCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.timeCost]];
    strTitle=NSLocalizedString(@"TAX_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %%",invoice.tax];
    [_lblTax setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblTaxCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.taxCost]];
    
    strTitle=NSLocalizedString(@"WAIT_TIME_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@",invoice.pricePerWaitingTime,TIME_SUFFIX];
    [_lblWaitTimeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblWaitTimeCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.waitingTimeCost]];
    
    strTitle=NSLocalizedString(@"SURGE_COST", nil);
    strSubValue=[NSString stringWithFormat:@"x%@",invoice.surgeMultiplier];
    [_lblSurgeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblSurgeCostValue setText:[NSString stringWithFormat:@" %@ %@",invoice.currency,invoice.surgeTimeFee]];
    
    [_lblRefferalBonousValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.referralBonus]];
    [_lblPromoBonousValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.promoBonus]];
    [APPDELEGATE hideLoadingView];
}
- (IBAction)onClickBtnSubmit:(id)sender
{
    [APPDELEGATE
     showLoadingWithTitle:NSLocalizedString(@"LOADING_FEEDBACK_VIEW", nil)];
    [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:self];
}
-(NSMutableAttributedString*)makeAttributedString:(NSString*)title andSubtitle:(NSString*)subtitle
{
    UIFont *titleFont = [UIFont fontWithName:_lblBaseCostValue.font.fontName    size:14.0];
    // Define your regular font
    UIFont *subtitleFont = [UIFont fontWithName:_lblBaseCostValue.font.fontName    size:11.0];;
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",title,subtitle]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor textColor] range:NSMakeRange(0,title.length)];
    [string addAttribute:NSFontAttributeName value:titleFont range:NSMakeRange(0,title.length)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor labelTextColor] range:NSMakeRange(title.length+1,subtitle.length)];
    [string addAttribute:NSFontAttributeName value:subtitleFont range:NSMakeRange(title.length+1,subtitle.length)];
    return string;
    
}
-(void)setLocalization
{
    
    [_lblInvoiceId setTextColor:[UIColor textColor]];
    [_lblInvoiceId setBackgroundColor:[UIColor whiteColor]];
    
    [_lblTotalValue setTextColor:[UIColor buttonColor]];
    [_lblDiscount setTextColor:[UIColor buttonColor]];
    
    [_lblTime setTextColor:[UIColor buttonTextColor]];
    [_lblDistance setTextColor:[UIColor buttonTextColor]];
    [_lblPayment setTextColor:[UIColor buttonTextColor]];
    [_lblTotal setContentMode:UIViewContentModeCenter];
    [_lblTotalValue setContentMode:UIViewContentModeCenter];
    
    [_lblTimeCost setTextColor:[UIColor textColor]];
    [_lblTimeCostValue setTextColor:[UIColor textColor]];
    [_lblDistanceCost setTextColor:[UIColor textColor]];
    [_lblDistanceCostValue setTextColor:[UIColor textColor]];
    [_lblBaseCost setTextColor:[UIColor textColor]];
    [_lblBaseCostValue setTextColor:[UIColor textColor]];
    [_lblWaitTimeCost setTextColor:[UIColor textColor]];
    [_lblWaitTimeCostValue setTextColor:[UIColor textColor]];
    [_lblSurgeCost setTextColor:[UIColor textColor]];
    [_lblSurgeCostValue setTextColor:[UIColor textColor]];
    [_lblTotal setTextColor:[UIColor textColor]];
    [_lblTotal setText:[NSLocalizedString(@"TOTAL", nil) uppercaseString]];
    [_lblRefferalBonous setText:[NSLocalizedString(@"REFFEREL_BONOUS", nil) capitalizedString]];
    [_lblPromoBonous setText:[NSLocalizedString(@"PROMO_BONOUS", nil) capitalizedString]];
    [_lblPromoBonous setTextColor:[UIColor labelTextColor]];
    [_lblRefferalBonous setTextColor:[UIColor labelTextColor]];
    [_lblRemainingAmount setTextColor:[UIColor labelTextColor]];
    [_lblWalletAmount setTextColor:[UIColor labelTextColor]];
    [_lblDiscount setText:[NSLocalizedString(@"DISCOUNT", nil) uppercaseString]];
    
    [_lblTax setTextColor:[UIColor textColor]];
    [_lblTaxCostValue setTextColor:[UIColor textColor]];
    
    _viewForInvoice=[[UtilityClass sharedObject]addShadow:_viewForInvoice];
    [_viewForInvoice setBackgroundColor:[UIColor backGroundColor]];
    [self.viewForTimeAndDistance setBackgroundColor:[UIColor GreenColor]];
    [self.viewForDiscounts setBackgroundColor:[UIColor whiteColor]];
}

@end
