//
//  SettingsVC.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 08/11/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "SettingsVC.h"
#import "SWRevealViewController.h"
#import "UIColor+Colors.h"
#import "PreferenceHelper.h"
#import "NSObject+Constants.h"

@interface SettingsVC ()

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLocalization];
    [_swtchSound setOn:!PREF.isSoundOff];
    [_switchPickUpSound setOn:!PREF.isPickUpSoundOff];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnSwitchSound:(id)sender
{
    UISwitch *switchObject = (UISwitch *)sender;
    if(switchObject.isOn)
    {
        PREF.SoundOff=NO;
        [_imgContent setImage:[UIImage imageNamed:@"menuSoundOn"]];
    }
    else
    {
        PREF.SoundOff=YES;
        [_imgContent setImage:[UIImage imageNamed:@"menuSoundOff"]];
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [_btnNavigation setTitle:NSLocalizedString(@"SETTINGS", nil) forState:UIControlStateNormal];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [_btnNavigation setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    
    [_viewForSound setBackgroundColor:[UIColor backGroundColor]];
    [_lblTitle setText:NSLocalizedString(@"SETTING_ITEM_1_TITLE", nil)];
    [_lblDescription setText:NSLocalizedString(@"SETTING_ITEM_1_DESCRIPTION", nil)];
    [_lblTitle setTextColor:[UIColor textColor]];
    [_lblDescription setTextColor:[UIColor labelTextColor]];

    [_viewForPickUpSound setBackgroundColor:[UIColor backGroundColor]];
    [_lblPickupSoundTitle setText:NSLocalizedString(@"SETTING_ITEM_2_TITLE", nil)];
    [_lblPickUpSoundMessage setText:NSLocalizedString(@"SETTING_ITEM_2_DESCRIPTION", nil)];
    [_lblPickupSoundTitle setTextColor:[UIColor textColor]];
    [_lblPickUpSoundMessage setTextColor:[UIColor labelTextColor]];
}
- (IBAction)onClickSwitchPickupSound:(id)sender
{

    UISwitch *switchObject = (UISwitch *)sender;
    if(switchObject.isOn)
    {
        PREF.PickUpSoundOff=NO;
        [_imgPickupAlertSound setImage:[UIImage imageNamed:@"menuSoundOn"]];
    }
    else
    {
        PREF.PickUpSoundOff=YES;
        [_imgPickupAlertSound setImage:[UIImage imageNamed:@"menuSoundOff"]];
        
    }
}

@end
