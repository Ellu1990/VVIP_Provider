//
//  User.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 20/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "User.h"

@implementation User
@synthesize firstName,phoneCountryCode,lastName,destAddress,pickUpAddress,phone,profileImage,pickUpLatitude,pickUpLongitude,rating,paymentMode,totalTime,totalDistance,destLatitude,destLongitude,providerStatus,tripNumber,waitingPrice,note;
+ (User *)sharedObject
{
    static User *objUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objUser = [[User alloc] init];
    });
    return objUser;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        firstName=@"0";
        lastName=@"0";
        destAddress=@"0";
        pickUpAddress=@"0";
        phone=@"0";
        profileImage=@"0";
        paymentMode=@"0";
        pickUpLatitude=@"0";
        pickUpLongitude=@"0";
        destLongitude=@"0";
        destLatitude=@"0";
        phoneCountryCode=@"0";
        providerStatus=0;
        tripNumber=@"0";
        waitingPrice=0.0;
		note=@"0";

    }
    return self;
}
-(void)resetObject
{
    firstName=@"0";
    lastName=@"0";
    destAddress=@"0";
    pickUpAddress=@"0";
    phone=@"0";
    profileImage=@"0";
    paymentMode=@"0";
    pickUpLatitude=@"0";
    pickUpLongitude=@"0";
    destLongitude=@"0";
    destLatitude=@"0";
    phoneCountryCode=@"0";
    tripNumber=@"0";
    waitingPrice=0.0;
	note=@"0";
}
@end
