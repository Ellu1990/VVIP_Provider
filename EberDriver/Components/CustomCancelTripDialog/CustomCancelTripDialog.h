//
//  CustomAlertWithTitle.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioButton.h"
@protocol CustomCancelTripDialogDelegate <NSObject>
@required
-(void)onClickCancelTripDialogWithReason:(NSString*)strCancelTripReason;

@end


@interface CustomCancelTripDialog: UIView<UITextFieldDelegate>
{
    NSString *strSelectedReason,*strCancelationCharge;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet RadioButton *rbtnCancelTripReason2;
@property (weak, nonatomic) IBOutlet RadioButton *rbtnCancelTripReason3;
@property (weak, nonatomic) IBOutlet UITextField *txtCancelTripReason;
@property (weak, nonatomic) IBOutlet RadioButton *rbtnOther;
@property (weak, nonatomic) IBOutlet UILabel *lblCancelationCharge;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet RadioButton *rbtnCancelTripReason;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (nonatomic, assign) id<CustomCancelTripDialogDelegate> delegate;
/*
 *  Title and message label styles
 */
@property (weak, nonatomic) IBOutlet UILabel *lblCancelTripDialog;

- (instancetype)initInView:(id)delegate;
- (IBAction)onClickRadioButtonCancelTripReason:(RadioButton*)sender;
- (IBAction)onClickCancel:(id)sender;
- (IBAction)onClickOk:(id)sender;



@end
