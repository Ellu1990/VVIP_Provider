//
//  UIColor+Colors.m
//  Eber Driver
//
//  Created by My Mac on 6/13/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "UIColor+Colors.h"

@implementation UIColor (Colors)



#pragma mark-TextColor

+(UIColor *)textBackColor
{
    UIColor *regularColor= [UIColor colorWithRed:(241/255.0f) green:(245/255.0f) blue:(248/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *)textColor
{
    UIColor *regularColor= [UIColor colorWithRed:(92/255.0f) green:(92/255.0f) blue:(92/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *)borderColor
{
    UIColor *regularColor= [UIColor colorWithRed:(236/255.0f) green:(236/255.0f) blue:(237/255.0f) alpha:1.0];
    return regularColor;
}

#pragma mark-APP THeme
+(UIColor *)DarkGreenColor
{
    UIColor *regularColor= [UIColor colorWithRed:(19/255.0f) green:(19/255.0f) blue:(36/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *)GreenColor
{
    UIColor *regularColor= [UIColor colorWithRed:(16/255.0f) green:(19/255.0f) blue:(36/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *)lightGreenColor
{
    UIColor *regularColor= [UIColor colorWithRed:(32/255.0f) green:(37/255.0f) blue:(65/255.0f) alpha:1.0];
    return regularColor;
}
/**Main View BackGround Color*/
+(UIColor *)backGroundColor
{
    UIColor *regularColor= [UIColor colorWithRed:(241/255.0f) green:(245/255.0f) blue:(248/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *)transparentGreenColor
{
    UIColor *regularColor= [UIColor colorWithRed:(16/255.0f) green:(19/255.0f) blue:(36/255.0f) alpha:0.8];
    return regularColor;

}
/*TextView Border Color*/
+(UIColor *)dividerColor
{
    UIColor *regularColor= [UIColor colorWithRed:(241/255.0f) green:(245/255.0f) blue:(248/255.0f) alpha:1.0];
    return regularColor;
}
#pragma mark-Label Colors:

+(UIColor *)labelTextColor
{
    UIColor *regularColor= [UIColor colorWithRed:(136/255.0f) green:(136/255.0f) blue:(136/255.0f) alpha:1.0];
    return regularColor;
}

+(UIColor *)labelTitleColor
{
    UIColor *regularColor= [UIColor colorWithRed:(255/255.0f) green:(255/255.0f) blue:(255/255.0f) alpha:1.0];
    return regularColor;
}

#pragma mark-Button Color

+(UIColor *)buttonColor
{
    UIColor *regularColor= [UIColor colorWithRed:(25/255.0f) green:(96/255.0f) blue:(118/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *) buttonTextColor
{
    UIColor *regularColor= [UIColor colorWithRed:(255/255.0f) green:(255/255.0f) blue:(255/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *) gradientColorDown
{
    UIColor *regularColor= [UIColor colorWithRed:(16/255.0f) green:(19/255.0f) blue:(36/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *) gradientColorUp
{
    UIColor *regularColor= [UIColor colorWithRed:(16/255.0f) green:(19/255.0f) blue:(36/255.0f) alpha:0.0];
    return regularColor;
}
+(UIColor *) googlePathColor
{
    UIColor *regularColor= [UIColor colorWithRed:(71/255.0f) green:(170/255.0f) blue:(255/255.0f) alpha:1.0];
    return regularColor;
}
+(UIColor *) currentPathColor
{
    UIColor *regularColor= [UIColor colorWithRed:(149/255.0f) green:(47/255.0f) blue:(50/255.0f) alpha:1.0];
    return regularColor;
}
 @end
