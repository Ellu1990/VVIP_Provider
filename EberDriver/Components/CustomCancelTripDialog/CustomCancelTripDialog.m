//
//  CustomAlertWithTitle.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "CustomCancelTripDialog.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
@implementation CustomCancelTripDialog


 -(void)awakeFromNib
{
    [super awakeFromNib];
    [_rbtnCancelTripReason setSelected:YES];
    strSelectedReason=_rbtnCancelTripReason.titleLabel.text;
}
- (IBAction)onClickRadioButtonCancelTripReason:(RadioButton*)sender
{
    if (sender.tag==4)
    {
        strSelectedReason=@"";
        [_txtCancelTripReason setText:@""];
        if (_txtCancelTripReason.hidden)
        {
        [_txtCancelTripReason setHidden:NO];
            /*CGRect frame=_btnOk.frame;
            frame.origin.y+=_txtCancelTripReason.frame.size.height+10;
            [_btnOk setFrame:frame];
            frame=_btnCancel.frame;
            frame.origin.y+=_txtCancelTripReason.frame.size.height+10;
            [_btnCancel setFrame:frame];*/
            CGRect frame=_alertView.frame;
            frame.size.height+=_txtCancelTripReason.frame.size.height+10;
            [_alertView setFrame:frame];
        
        }
        
    }
    else
    {
        
        if (!_txtCancelTripReason.hidden)
        {
            [_txtCancelTripReason setHidden:YES];
           /* CGRect frame=_btnOk.frame;
            frame.origin.y-=_txtCancelTripReason.frame.size.height+10;
            [_btnOk setFrame:frame];
            frame=_btnCancel.frame;
            frame.origin.y-=_txtCancelTripReason.frame.size.height+10;
            [_btnCancel setFrame:frame];*/
            CGRect frame=_alertView.frame;
            frame.size.height -=_txtCancelTripReason.frame.size.height+10;
            [_alertView setFrame:frame];
            
        }
        strSelectedReason = [NSString stringWithFormat:@"%@", sender.titleLabel.text];
    }
    
}

- (IBAction)onClickCancel:(id)sender
{
    [self removeFromSuperview];
    
}

- (IBAction)onClickOk:(id)sender
{
    if (_txtCancelTripReason.hidden)
    {
        [_delegate onClickCancelTripDialogWithReason:strSelectedReason];
        [self removeFromSuperview];
        
    }
    else
    {
        strSelectedReason=[_txtCancelTripReason text];
        if ([UtilityClass isEmpty:strSelectedReason])
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_FILL_THE_FIELD",nil)];
        }
        else
        {
            [_delegate onClickCancelTripDialogWithReason:strSelectedReason];
            
            [self removeFromSuperview];
        }
    }
}

- (instancetype)initInView:(nullable id)delegate
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomCancelTripDialog" owner:nil options:nil];
        self = [nibContents lastObject];
        self.delegate = delegate;
        self.frame=APPDELEGATE.window.frame;
        self.alertView.center=self.center;
        self.alertView=[[UtilityClass sharedObject]addShadow:self.alertView];
        [self setLocalization];
        self.backgroundColor=[UIColor clearColor];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        return self;
    }
    return self;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint pt;
    CGRect rc = [_alertView bounds];
    rc = [_alertView convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -=30;
    [_scrollView setContentOffset:pt animated:YES];
}
-(void)setLocalization
{
    [_lblCancelTripDialog setTextColor:[UIColor textColor]];
    [_lblCancelTripDialog setText:NSLocalizedString(@"ALERT_TITLE_CANCEL_TRIP", nil)];

    
 [_lblCancelationCharge setHidden:YES];
 [_rbtnOther setTitle:NSLocalizedString(@"RBTN_OTHER", nil) forState:UIControlStateNormal];
 [_rbtnCancelTripReason setTitle:NSLocalizedString(@"RBTN_CANCELATION_REASON_1", nil) forState:UIControlStateNormal];
 [_rbtnCancelTripReason2 setTitle:NSLocalizedString(@"RBTN_CANCELATION_REASON_2", nil) forState:UIControlStateNormal];
 [_rbtnCancelTripReason3 setTitle:NSLocalizedString(@"RBTN_CANCELATION_REASON_3", nil) forState:UIControlStateNormal];
 [_btnOk setTitle:NSLocalizedString(@"ALERT_BTN_IM_SURE", nil) forState:UIControlStateNormal];
 [_btnCancel setTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) forState:UIControlStateNormal];
    [_btnOk setBackgroundColor:[UIColor buttonColor]];
    [_btnCancel setBackgroundColor:[UIColor buttonColor]];
    [_btnOk setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnCancel setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor clearColor]];
 [self.alertView setBackgroundColor:[UIColor whiteColor]];
    
}
-(void)setCancelationCharge:(NSString*)cancelationCharge
{
   strCancelationCharge=cancelationCharge;
   NSString *finalString = [NSString stringWithFormat:NSLocalizedString(@"ALERT_MSG_CANCELATION_CHARGE", nil), strCancelationCharge];
   [_lblCancelationCharge setText:finalString withColor:[UIColor textColor]];
   [_lblCancelationCharge setHidden:NO];
}
@end
