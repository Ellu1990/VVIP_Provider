//
//  LoginVc.h
//  Eber Driver
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CustomAlertWithTextInput.h"

@interface LoginVc : UIViewController
<CustomAlertWithTextInput,UITextFieldDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIView *viewForLogin;
@property(nonatomic,weak)IBOutlet UIScrollView *scrLogin;
#pragma mark-Text
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UITextField *txtEmail;
@property(nonatomic,weak)IBOutlet UITextField *txtPsw;
@property(nonatomic,weak)IBOutlet UITextField *activeTextField;
#pragma mark-Button
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPsw;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (strong, nonatomic) IBOutlet UIButton *btnGoogle;
@property (strong, nonatomic) IBOutlet UIButton *btnFb;
@property (weak, nonatomic) IBOutlet UILabel *lblHaventAccount;

@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;
#pragma mark-Action
- (IBAction)onClickLogin:(id)sender;
- (IBAction)onClickGooglePlus:(id)sender;
- (IBAction)onClickFacebook:(id)sender;
- (IBAction)onClickForgotPsw:(id)sender;
/**Used to Pass Parameter To The Web Service*/
@property(nonatomic,copy) NSMutableDictionary *dictParam;
/** Used To Identify User*/
@property(nonatomic,copy) NSString *strLoginBy,*strSocialUniqueID,*strEmail;
@end
