
//
//  DummyInvoice.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 20/09/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoiceVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)onClickBtnSubmit:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForTimeAndDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblInvoiceId;
@property (weak, nonatomic) IBOutlet UIView *viewForDiscounts;
@property (weak, nonatomic) IBOutlet UILabel *lblBaseCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceCost;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitTimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblTax;
@property (weak, nonatomic) IBOutlet UILabel *lblSurgeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblRefferalBonous;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoBonous;
@property (weak, nonatomic) IBOutlet UIImageView *imgPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;

/*Values*/
@property (weak, nonatomic) IBOutlet UILabel *lblPromoBonousValue;
@property (weak, nonatomic) IBOutlet UILabel *lblRefferalBonousValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSurgeCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitTimeCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBaseCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblWalletAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainingAmount;

@end
