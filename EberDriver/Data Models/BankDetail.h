//
//  invoice.h
//  
//
//  Created by Elluminati Mini Mac 5 on 29/08/16.
//
//

#import <Foundation/Foundation.h>

@interface BankDetail : NSObject

@property(nonatomic,copy) NSString* id_;
@property(nonatomic,copy) NSString* bankHolderType;
@property(nonatomic,copy) NSString* bankName;
@property(nonatomic,copy) NSString* bankBranch;
@property(nonatomic,copy) NSString* bankAccountNumber;
@property(nonatomic,copy) NSString* bankAccountHolderName;
@property(nonatomic,copy) NSString* bankBeneficiaryAddress;
@property(nonatomic,copy) NSString* bankUniqueCode;
@property(nonatomic,copy) NSString* bankSwiftCode;

@end
