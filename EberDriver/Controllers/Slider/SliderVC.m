//
//  SliderVC.m
//  Employee
//
//  Created by Elluminati - macbook on 19/05/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "SliderVC.h"
#import "CustomAlertWithTitle.h"
#import "NSObject+Constants.h"
#import "UIImageView+image.h"
#import "SWRevealViewController.h"
#import "MapVC.h"
#import "CellSlider.h"
#import "PreferenceHelper.h"
#import "UIColor+Colors.h"
#import "UtilityClass.h"
#import "Parser.h"
#import "Hotline.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <QuartzCore/QuartzCore.h>


@interface SliderVC ()
{
    CustomAlertWithTitle *logoutView;
    UITapGestureRecognizer *singleTap;
}
@end

@implementation SliderVC


#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_imgProfilePic downloadFromURL:PREF.providerPicture withPlaceholder:[UIImage imageNamed:@"user"]];
    [self setLocalization];
    [_viewForProfile setBackgroundColor:[UIColor clearColor ]];
    self.lblName.text=[NSString stringWithFormat:@"%@ %@",PREF.providerFirstName,PREF.providerLastName];
   	arrayMenuIcons=[[NSMutableArray alloc]initWithObjects:@"menuProfile",
                    @"menuHistory",
                    @"menuBank",
                    @"menuDocument",
                    @"menuSettings",
                    @"menuHelp",
                    @"menuLogout",nil ];
    arrayMenuItems=[[NSMutableArray alloc]initWithObjects:
                    NSLocalizedString(@"PROFILE", nil),
                    NSLocalizedString(@"HISTORY", nil),
                    NSLocalizedString(@"BANK_DETAIL", nil),
                    [NSLocalizedString(@"DOCUMENT", nil) capitalizedString],
                    NSLocalizedString(@"SETTINGS", nil),
                    NSLocalizedString(@"HELP", nil),
                    NSLocalizedString(@"LOGOUT", nil),nil ];
    
    
    arraySegueIdentifiers=[[NSMutableArray alloc]initWithObjects:SEGUE_PROFILE,SEGUE_TO_HISTORY, nil,SEGUE_TO_DOCUMENT,nil,SEGUE_TO_SETTINGS,nil];
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_viewForProfile setUserInteractionEnabled:YES];
    [_viewForProfile addGestureRecognizer:singleTap];
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgProfilePic setRoundImageWithColor:[UIColor borderColor]];
    _imgProfilePic.center= CGPointMake(([UIScreen mainScreen].bounds.size.width * 0.8/2), _viewForProfile.frame.size.height/2-_lblName.frame.size.height/2);
    [_lblName setCenter:CGPointMake(_imgProfilePic.center.x,_imgProfilePic.center.y+_imgProfilePic.frame.size.height/2+_lblName.frame.size.height/2+10)];
    
}
-(void)tapDetected
{
    
    [self.revealViewController rightRevealToggle:self];
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    self.ViewObj=(MapVC *)[nav.childViewControllers objectAtIndex:0];
    [self.ViewObj performSegueWithIdentifier:SEGUE_PROFILE sender:self];
    return;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
-(void)setLocalization
{
     UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    frontVC=[nav.childViewControllers objectAtIndex:0];
    [_lblName setTextColor:[UIColor whiteColor]];
    UIFont *font=[UIFont fontWithName:@"Roboto_Bold" size: 20];
    [_lblName setFont:font];
    _lblName.font=[_lblName.font fontWithSize:18];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:NO];
    [self.revealViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [_tblMenu reloadData];
    [[UtilityClass sharedObject] loadFromURL:[NSURL URLWithString:PREF.providerPicture] callback:^(UIImage *image)
    {
        if (image)
        {
            _imgProfilePic.image=image;
        }else
        {
            _imgProfilePic.image=[UIImage imageNamed:@"user"];
        }
    }];
    
    [self setLocalization];
    self.lblName.text=[NSString stringWithFormat:@"%@ %@",PREF.providerFirstName,PREF.providerLastName];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
}
#pragma mark -
#pragma mark - UITableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayMenuItems count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellSlider *cell=(CellSlider *)[tableView dequeueReusableCellWithIdentifier:@"CellSlider"];
    if (cell==nil)
    {
        cell=[[CellSlider alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellSlider"];
    }
    [cell setCellData:[arrayMenuItems objectAtIndex:indexPath.row] andImage:[NSString stringWithFormat:@"%@",[arrayMenuIcons objectAtIndex:indexPath.row]]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   [tableView deselectRowAtIndexPath:indexPath animated:YES];
   [self.revealViewController rightRevealToggle:self];
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    self.ViewObj=(MapVC *)[nav.childViewControllers objectAtIndex:0];
    if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"Logout"])
    {
	    logoutView=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"LOGOUT",nil) message:NSLocalizedString(@"ALERT_MSG_LOGOUT",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO",nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES",nil)];
        [logoutView bringSubviewToFront:self.parentViewController.view];
        return;
    }
	
	else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"Profile"])
	{
		[self.ViewObj performSegueWithIdentifier:SEGUE_PROFILE sender:self];
		return;
    }
	
	else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"Document"])
	{
		 [self.ViewObj performSegueWithIdentifier:SEGUE_TO_DOCUMENT sender:self];
        
        return;
	}
	else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"History"])
	{
		[self.ViewObj performSegueWithIdentifier:SEGUE_TO_HISTORY sender:self];
		return;
	}
    
    else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"HELP",nil)])
    {
        [[Hotline sharedInstance] showConversations:self];
    }
    else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"SETTINGS", nil)])
    {
        [self.ViewObj performSegueWithIdentifier:SEGUE_TO_SETTINGS sender:self];
        return;
    }
    else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"BANK_DETAIL", nil)])
    {
        [self.ViewObj performSegueWithIdentifier:SEGUE_TO_BANKDETAIL sender:self];
        return;
    }
    
}
#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    [[UtilityClass sharedObject] animateHide:view];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictParam setObject:PREF.providerToken forKey:PARAM_PROVIDER_TOKEN];
    [APPDELEGATE showLoadingWithTitle:@""];
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:PROVIDER_LOGOUT withParamData:dictParam withMethod:post withBlock:^(id response, NSError *error) {
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                        if([[Parser sharedObject]isLogoutSuccess:response] )
                        {
                            UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
                            self.ViewObj=(MapVC *)[nav.childViewControllers objectAtIndex:0];
                            if ( [_ViewObj.timerForUpdateLocation isValid])
                            {
                                [_ViewObj.timerForUpdateLocation invalidate],
                                _ViewObj.timerForUpdateLocation=nil;
                            }
                            [APPDELEGATE hideLoadingView];
                            [APPDELEGATE goToMain];
                        
                        }
                     }
                 );
             }
             
         }];
}
-(void)ShareScreen
{
    NSString* someText = @"text To Share";
    NSArray* dataToShare = @[someText];  // ...or whatever pieces of data you want to share.
    UIActivityViewController* activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:dataToShare
                                      applicationActivities:nil];
    [self presentViewController:activityViewController animated:YES completion:^{}];
}
@end
