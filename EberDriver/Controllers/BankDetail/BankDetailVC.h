//
//  ProfileVC.h
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNHelper.h"
#import "SelectImage.h"
#import "CustomAlertWithTitle.h"
#import "CustomAlertWithTextInput.h"
#import "CustomOtpDialog.h"

@interface BankDetailVC : UIViewController
<UITextFieldDelegate,
UINavigationControllerDelegate,
UIGestureRecognizerDelegate,
CustomAlertWithTextInput>
{
   
}

//Views
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewForBankDetail;
@property (weak, nonatomic) IBOutlet UIImageView *imgBank;

///

/*viewForBank*/

@property (weak, nonatomic) IBOutlet UITextField *txtAccountHolderName;
@property (weak, nonatomic) IBOutlet UITextField *txtAccountNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtBankName;
@property (weak, nonatomic) IBOutlet UITextField *txtBranch;
@property (weak, nonatomic) IBOutlet UITextField *txtUniqueCode;
@property (weak, nonatomic) IBOutlet UITextField *txtSwiftCode;
@property (weak, nonatomic) IBOutlet UITextField *txtBankAddress;

@property (weak, nonatomic) IBOutlet UITextField *activeTextField;
//Buttons
@property (weak, nonatomic) IBOutlet UIButton *btnUpdateBankDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
////// Actions
- (IBAction)onClickBtnUpdate:(id)sender;
- (IBAction)onClickBtnBack:(id)sender;
@end
