//
//  RegisterVc.h
//  Eber Driver
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNHelper.h"
#import <CoreLocation/CoreLocation.h>
#import "SelectImage.h"
#import "CustomAlertWithTitle.h"
#import "SimpleTableView.h"
#import "CustomOtpDialog.h"


@interface RegisterVc : UIViewController
<CLLocationManagerDelegate,
UITextFieldDelegate,
UINavigationControllerDelegate,
UIActionSheetDelegate,
UIGestureRecognizerDelegate,
SimpleTableViewDelegate,
CustomAlertDelegate,
CustomOtpDialogDelegate
>

{
CLLocationManager *_locationManager;
CLLocationCoordinate2D currentLocation;
}
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCity;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCountryName;

//Views
@property (weak, nonatomic) IBOutlet UIView *viewForRegister;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIView *viewForSelectCountryCode;
@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtCarNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtCarModel;
@property (weak, nonatomic) IBOutlet UITextField *activeTextField;
//View For Country Code
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UITableView *tblForCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectCountry;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblAllReadyHaveAccount;

//Buttons
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnTerm;
@property (weak, nonatomic) IBOutlet UIButton *btnFb;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
////// Actions
- (IBAction)onClickBtnPickerCancel:(id)sender;
- (IBAction)onClickBtnFb:(id)sender;
- (IBAction)onClickBtnselectCountry:(id)sender;
- (IBAction)onClickBtnGoogle:(id)sender;
- (IBAction)onClickBtnRegister:(id)sender;
- (IBAction)checkBoxBtnPressed:(id)sender;
- (IBAction)termsBtnPressed:(id)sender;
- (IBAction)onClickBtnSignIn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogl;
//Other Properties
@property(nonatomic,copy) NSString *jsonParameter;
@property(nonatomic,copy)NSMutableArray *arrForCountry;
@property(nonatomic,copy) NSMutableDictionary *dictParam;
@property(nonatomic,copy) NSString *strImageData,
*strForRegistrationType,
*strForSocialId,
*strForToken,
*strForID,
*strForCity,
*strForCountry;
@property(nonatomic,assign) BOOL isPicAdded;


@end
