//
//  HistoryDetailVC.h
//  Rider Driver
//
//  Created by My Mac on 7/8/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HistoryDetailVC:UIViewController<UIWebViewDelegate,NSURLConnectionDelegate>
{
    
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTripNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTripNoValue;
@property (weak, nonatomic) IBOutlet UILabel *lblEarningValue;
@property (weak, nonatomic) IBOutlet UILabel *lblYourEarning;
@property (weak, nonatomic) IBOutlet UIView *viewForHistory;
@property (strong, nonatomic) NSString *trip_id;
@property (strong, nonatomic) NSString *distanceUnit;
@property (strong, nonatomic) NSString *currency;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (strong, nonatomic) NSDate *tripDate;
@property (strong, nonatomic) IBOutlet UILabel *lblSrcAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblDestAddress;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIImageView *imgProvider;
@property (strong, nonatomic) IBOutlet UILabel *lblProviderNAme;
@property (strong, nonatomic) IBOutlet UILabel *lblTripTime;
@property (strong, nonatomic) IBOutlet UILabel *lblTripDistance;
@property (strong, nonatomic) IBOutlet UILabel *lblTripCost;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDuration;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnInvoice;
- (IBAction)onClickBtnInvoice:(id)sender;

/*ViewForInvoice*/
@property (weak, nonatomic) IBOutlet UIView *viewForInvoice;


@property (weak, nonatomic) IBOutlet UIView *viewForTimeAndDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblInvoiceId;
@property (weak, nonatomic) IBOutlet UIView *viewForDiscounts;
@property (weak, nonatomic) IBOutlet UILabel *lblBaseCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceCost;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitTimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblTax;
@property (weak, nonatomic) IBOutlet UILabel *lblSurgeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblRefferalBonous;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoBonous;
@property (weak, nonatomic) IBOutlet UIImageView *imgPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;

/*Values*/
@property (weak, nonatomic) IBOutlet UILabel *lblPromoBonousValue;
@property (weak, nonatomic) IBOutlet UILabel *lblRefferalBonousValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSurgeCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitTimeCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBaseCostValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainingAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblWalletAmount;

@end
