//
//  CustomAlertWithTitle.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomVisitorTypeDialog;
@protocol CustomVisitorTypeDialogDelegate <NSObject>
@optional
- (void) onClickClose;
@required
- (void) onClickCustomVisitorDialogOk:(CustomVisitorTypeDialog*)view;
@end

@interface CustomVisitorTypeDialog : UIView
- (instancetype)initWithTitle:(NSString*)title
                      message:(NSString *)message
                     delegate:(id)delegate
                     typeName:(NSString*)typeName
                 baseFareCost:(NSString*)baseFareCostValue
                  minFareCost:(NSString*)minFareCostValue
                 distanceCost:(NSString*)distanceCostValue
            cancelButtonTitle:(NSString*)cancelButtonTitle
            otherButtonTitles:(NSString*)otherButtonTitle;


@property (nonatomic, assign) id<CustomVisitorTypeDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (nonatomic,strong) UIColor *buttonTextColor;
@property (nonatomic,strong) UIFont  *buttonFont;
@property (nonatomic,strong) UIColor *buttonShadowColor;
@property (nonatomic,assign) CGSize   buttonShadowOffset;
@property (nonatomic,assign) CGFloat  buttonShadowBlur;
@property (nonatomic,assign) CGFloat cornerRadius;
@property(strong,nonatomic)id parent;
-(void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;

/*Type Detail*/
@property (weak, nonatomic) IBOutlet UILabel *lblTypeName;
@property (weak, nonatomic) IBOutlet UILabel *lblBaseFare;
@property (weak, nonatomic) IBOutlet UILabel *lblMinFare;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceCost;
@property (weak, nonatomic) IBOutlet UILabel *lblMinFareCost;
@property (weak, nonatomic) IBOutlet UILabel *lblBaseFareCost;



@end
