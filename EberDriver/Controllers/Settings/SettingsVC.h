//
//  SettingsVC.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 08/11/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIImageView *imgContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UISwitch *swtchSound;
@property (weak, nonatomic) IBOutlet UIView *viewForSound;
@property (weak, nonatomic) IBOutlet UIView *viewForPickUpSound;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupSoundTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpSoundMessage;
@property (weak, nonatomic) IBOutlet UISwitch *switchPickUpSound;
@property (weak, nonatomic) IBOutlet UIImageView *imgPickupAlertSound;

@end
