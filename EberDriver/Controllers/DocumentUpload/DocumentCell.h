//
//  DocumentCell.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 16/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Document.h"
@interface DocumentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDocumentTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgDocument;
@property (nonatomic,copy)NSString *strDocId;
-(void)setCellData:(Document*)doc;
@property (weak, nonatomic) IBOutlet UIButton *btnCell;
- (IBAction)onClickBtnCell:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForCell;
@end
