//
//  NSObject+Constants.m
//  Eber Driver
//
//  Created by My Mac on 6/13/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "NSObject+Constants.h"

@implementation NSObject (Constants)
#pragma mark-Segues
NSString *const SEGUE_TO_DIRCET_REGI=@"segueToRegister";
NSString *const SEGUE_TO_DIRCET_LOGIN=@"segueToLogin";
NSString *const SEGUE_TO_MAP=@"segueToMap";
NSString *const SEGUE_TO_DOCUMENT=@"segueToDocument";
NSString *const SEGUE_PROFILE=@"segueToProfile";
NSString *const SEGUE_TO_TRIP=@"segueToTrip";
NSString *const SEGUE_TO_HISTORY=@"segueToHistory";
NSString *const SEGUE_TO_HISTORY_DETAIL=@"segueToHistoryDetail";
NSString *const SEGUE_TO_INVOICE=@"segueToInvoice";
NSString *const SEGUE_TO_FEEDBACK=@"segueToFeedback";
NSString *const SEGUE_TO_SETTINGS=@"segueToSettings";
NSString *const SEGUE_TO_BANKDETAIL=@"segueToBankDetail";
#pragma mark -
#pragma mark - WS METHODS
NSString *const WS_PROVIDER_EARNING=@"";
/*BANK_DETAIL WEB-SERVICES*/
NSString *const WS_UPDATE_BANK_DETAIL=@"update_bank_detail";
NSString *const WS_ADD_BANK_DETAIL=@"add_bank_detail";
NSString *const WS_GET_BANK_DETAIL=@"get_bank_detail";
/*GOOGLE MAP PATH*/
NSString *const WS_SET_GOOGLE_MAP_PATH=@"setgooglemappath";
NSString *const WS_GET_GOOGLE_MAP_PATH=@"getgooglemappath";

/*CURRENT TRIP WEBSERVICE*/
NSString *const PROVIDER_GET_TRIP_STATUS=@"providergettripstatus";
NSString *const PROVIDER_SET_TRIP_STATUS=@"settripstatus/";
NSString *const WS_CANCEL_TRIP=@"tripcancelbyprovider/";
/*PROVIDER DETAILS*/
NSString *const PROVIDER_REGISTER=@"providerregister";
NSString *const PARAM_TYPE_IMAGE_URL=@"type_image_url";
NSString *const WS_GET_CITY_LIST=@"citilist_selectedcountry";
NSString *const WS_GET_APP_KEYS=@"getappkeys";
NSString *const WS_GET_COUNTRIES=@"countries";
NSString *const WS_GET_SETTING_DETAILS=@"getsettingdetail";
NSString *const WS_GET_VERIFICATION_OTP=@"verification";
NSString *const WS_UPDATE_PROVIDER_TYPE=@"providerupdatetype";
NSString *const WS_GET_TYPELIST_SELETED_CITY=@"typelist_selectedcountrycity";
NSString *const PROVIDER_LOGIN=@"providerslogin";
NSString *const WS_PROVIDER_FORGET_PASSWORD=@"forgotpassword";
NSString *const PROVIDER_LOGOUT=@"providerlogout";
NSString *const GET_DOCUMENTS=@"getproviderdocument";
NSString *const UPLOAD_DOCUMENT=@"uploaddocument/";
NSString *const PROVIDER_UPDATE_PROFILE=@"providerupdatedetail/";
NSString *const PROVIDER_TOGGLE_STATE=@"togglestate/";
NSString *const PROVIDER_UPDATE_LOCATION=@"provider_location/";
NSString *const PROVIDER_GET_TRIP=@"gettrips/";
NSString *const PROVIDER_DETAIL=@"get_provider_detail";
NSString *const PROVIDER_COMPLETE_TRIP=@"completetrip/";
NSString *const PROVIDER_HISTORY=@"providerhistory";
NSString *const PROVIDER_TRIP_DETAIL=@"providertripdetail";
NSString *const PROVIDER_RATE_USER=@"providergiverating/";
NSString *const PARAM_USER_CREATE_TIME=@"user_create_time";
NSString *const PROVIDER_REFRESH_TOKEN=@"updateproviderdevicetoken/";
#pragma mark -
#pragma mark - COMMON-PARAMETER NAME
NSString *const PARAM_PROVIDER_ID=@"provider_id";
NSString *const PARAM_PROVIDER_TOKEN=@"token";
NSString *const PARAM_ID=@"_id";
NSString *const PARAM_TYPE=@"type";
NSString *const PARAM_USER=@"user";
NSString *const PARAM_CITY_TYPES=@"citytypes";

NSString *const PARAM_IS_PARTNER_APPROVED_BY_ADMIN=@"is_partner_approved_by_admin";
NSString *const PARAM_PARTNER_EMAIL=@"partner_email";
NSString *const PARAM_PROVIDER_TYPE_ID=@"provider_type_id";
NSString *const PARAM_PROVIDER_TYPE=@"provider_type";
NSString *const PARAM_CURRENCY=@"currency";
NSString *const PARAM_DISTANCE_UNIT=@"unit";
NSString *const PARAM_PICTURE_DATA=@"pictureData";
NSString *const PARAM_TYPE_ID=@"typeid";
NSString *const PARAM_ADMIN_TYPE_ID=@"admintypeid";
NSString *const PARAM_IS_VISIT_TYPE=@"isVisitType";
NSString *const PARAM_MINFARE=@"min_fare";
NSString *const PARAM_TRIP_NUMBER=@"unique_id";
NSString *const PARAM_INVOICE_NUMBER=@"invoice_number";


#pragma mark -
#pragma mark - LOGIN-REGISTER AND PROFILE-PARAMS

NSString *const PARAM_BEARING=@"bearing";
NSString *const PARAM_RATING=@"rating";
NSString *const PARAM_REVIEW=@"review";
NSString *const PARAM_LOGIN_BY=@"login_by";
NSString *const PARAM_SOCIAL_UNIQUE_ID=@"social_unique_id";
NSString *const PARAM_PICTURE=@"picture";
NSString *const PARAM_MAP_IMAGE=@"map";
NSString *const PARAM_TIME_LEFT=@"time_left_to_responds_trip";
NSString *const PARAM_FIRST_NAME=@"first_name";
NSString *const PARAM_LAST_NAME=@"last_name";
NSString *const PARAM_COUNTRY_CODE=@"country_phone_code";
NSString *const PARAM_PHONE=@"phone";
NSString *const PARAM_BIO=@"bio";
NSString *const PARAM_ADDRESS=@"address";
NSString *const PARAM_ZIPCODE=@"zipcode";
NSString *const PARAM_COUNTRY=@"country";
NSString *const PARAM_CITY=@"city";
NSString *const PARAM_COUNTRY_NAME=@"countryname";
NSString *const PARAM_COUNTRY_PHONE_CODE=@"countryphonecode";
NSString *const PARAM_TYPE_DETAILS=@"type_details";
NSString *const PARAM_PROVIDER=@"provider";
NSString *const PARAM_CITY_NAME=@"cityname";
NSString *const PARAM_DEVICE_TOKEN=@"device_token";
NSString *const PARAM_DEVICE_TIMEZONE=@"device_timezone";
NSString *const PARAM_SERVICE_TYPE_ID=@"service_type_id";
NSString *const PARAM_SERVICE_TYPE=@"service_type";
NSString *const PARAM_DEVICE_TYPE=@"device_type";
NSString *const PARAM_PROVIDER_APPROVED=@"is_approved";
NSString *const PARAM_PROVIDER_DOC_UPLOADED=@"is_document_uploaded";
NSString *const PARAM_PROVIDER_IS_ACTIVE=@"is_active";
NSString *const PARAM_CAR_MODEL=@"car_model";
NSString *const PARAM_CAR_NUMBER=@"car_number";
NSString *const PARAM_CAR_NAME=@"typename";
NSString *const PARAM_TYPE_IMAGE=@"type_image";
NSString *const PARAM_TIME_PRICE=@"time_price";
NSString *const PARAM_DISTANCE_PRICE=@"distance_price";
/*login*/
NSString *const PARAM_EMAIL=@"email";
NSString *const PARAM_PASSWORD=@"password";
NSString *const PROVIDER_PATH_DRAW=@"providerPath";
NSString *const PARAM_EMAIL_VERIFICATION_ON=@"providerEmailVerification";
NSString *const PARAM_SMS_VERIFICATION_ON=@"providerSms";
NSString *const PARAM_SMS_OTP=@"otpForSMS";
NSString *const PARAM_EMAIL_OTP=@"otpForEmail";
/*Update Profile*/
NSString *const PARAM_OLD_PASSWORD=@"old_password";
NSString *const PARAM_NEW_PASSWORD=@"new_password";
/*Map PARAM*/
NSString *const PARAM_LATITUDE=@"latitude";
NSString *const PARAM_LONGITUDE=@"longitude";
/*BANK PARAMS*/
NSString *const PARAM_BANK_DETEILS=@"bankdetails";
NSString *const PARAM_BANK_HOLDER_TYPE=@"bank_holder_type";
NSString *const PARAM_BANK_HOLDER_ID=@"bank_holder_id";
NSString *const PARAM_BANK_NAME=@"bank_name";
NSString *const PARAM_BANK_BRANCH=@"bank_branch";
NSString *const PARAM_BANK_ACCOUNT_NUMBER=@"bank_account_number";
NSString *const PARAM_BANK_ACCOUNT_HOLDER_NAME=@"bank_account_holder_name";
NSString *const PARAM_BANK_BENEFICIARY_ADDRESS=@"bank_beneficiary_address";
NSString *const PARAM_BANK_UNIQUE_CODE=@"bank_unique_code";
NSString *const PARAM_BANK_SWIFT_CODE=@"bank_swift_code";


#pragma mark -TRIP Param
NSString *const PARAM_GET_GOOGLE_MAP_PATH_START_LOCATION_TO_PICKUP_LOCATION=@"googlePathStartLocationToPickUpLocation";
NSString *const PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION=@"googlePickUpLocationToDestinationLocation";
NSString *const PARAM_TRIP_LOCATION=@"triplocation";
NSString *const PARAM_IS_CANCELLATION_FEE=@"is_cancellation_fee";
NSString *const PARAM_START_TRIP_TO_END_TRIP_LOCATION=@"startTripToEndTripLocations";
NSString *const PARAM_TRIP=@"trip";
NSString *const PARAM_TRIP_SERVICE=@"tripservice";
NSString *const PARAM_IS_TRIP_COMPLETED=@"is_trip_completed";
NSString *const PARAM_IS_TRIP_CANCELLED_BY_USER=@"is_trip_cancelled_by_user";
NSString *const PARAM_IS_TRIP_CANCELLED_BY_PROVIDER=@"is_trip_cancelled_by_provider";
NSString *const PARAM_TRIPS=@"trips";
NSString *const PARAM_IS_PROVIDER_ACCEPTED=@"is_provider_accepted";
NSString *const PARAM_TRIP_ID=@"trip_id";
NSString *const PARAM_RESPOND_TRIP=@"respondstrip/";
NSString *const PARAM_TRIP_SOURCE_ADDRESS=@"source_address";
NSString *const PARAM_TRIP_DESTINATION_ADDRESS=@"destination_address";
NSString *const PARAM_PROVIDER_STATUS=@"is_provider_status";
NSString *const PARAM_TRIP_CREATE_TIME=@"user_create_time";
NSString *const PARAM_DISTANCE=@"distance";
NSString *const PARAM_CANCEL_TRIP_REASON=@"cancelReason";
NSString *const PARAM_SOURCE_LOCATION=@"sourceLocation";
NSString *const PARAM_DESTINATION_LOCATION=@"destinationLocation";
NSString *const PARAM_SCHEDULED_REQUEST_PRE_START_TIME=@"scheduledRequestPreStartMinute";
NSString *const PARAM_PROVIDER_SERVICE_FEES=@"provider_service_fees";
#pragma mark-Document Parameter
NSString *const PARAM_DOCUMENT_PICTURE=@"document_picture";
NSString *const PARAM_CONTACT_EMAIL=@"contactUsEmail";
NSString *const PARAM_PROVIDER_DOCUMENT=@"providerdocument";
NSString *const PARAM_DOCUMENT_NAME=@"name";
NSString *const PARAM_DOCUMENT_UPLOADED=@"is_uploaded";
NSString *const PARAM_DOCUMENT_OPTIONAL=@"option";
#pragma mark -
#pragma mark - INVOICE PARAM
NSString *const PARAM_BASEPRICE=@"base_price";
NSString *const PARAM_TOTAL=@"total";
NSString *const PARAM_DISTANCE_COST=@"distance_cost";
NSString *const PARAM_TIME_COST=@"time_cost";
NSString *const PARAM_PROMO_BONOUS=@"promo_payment";
NSString *const PARAM_REFFERAL_BONOUS=@"referral_payment";
NSString *const PARAM_PAYMENT_MODE=@"payment_mode";
NSString *const PARAM_PRICE_FOR_TOTAL_TIME=@"price_for_total_time";
NSString *const PARAM_TOTAL_DISTANCE=@"total_distance";
NSString *const PARAM_TOTAL_TIME=@"total_time";
NSString *const PARAM_TAX=@"tax";
NSString *const PARAM_BASE_PRICE_DISTANCE=@"base_price_distance";
NSString *const PARAM_TAX_FEE=@"tax_fee";
NSString *const PARAM_SURGE_FEE=@"surge_fee";
NSString *const PARAM_PRICE_PER_UNIT_DISTANCE=@"price_per_unit_distance";
NSString *const PARAM_TOTAL_WAITING_TIME=@"total_waiting_time";
NSString *const PARAM_FOR_WAITING_TIME=@"price_for_waiting_time";
NSString *const PARAM_SURGE_MULTIPLIEER=@"surge_multiplier";
NSString *const PARAM_WAITING_TIME_COST=@"waiting_time_cost";
NSString *const PARAM_TOTAL_WAIT_TIME=@"total_wait_time";
NSString *const PARAM_WALLET_PAYMENT=@"wallet_payment";
NSString *const PARAM_REMAINING_PAYMENT=@"remaining_payment";
NSString *const PARAM_CARD_PAYMENT=@"card_payment";
NSString *const PARAM_CASH_PAYMENT=@"cash_payment";
#pragma mark -
#pragma mark - Prefences key
NSString *const PREF_DEVICE_TOKEN=@"pref_device_token";
NSString *const PREF_PROVIDER_TOKEN=@"token";
NSString *const PREF_PROVIDER_ID=@"provider_id";
NSString *const PREF_IS_LOGIN=@"is_login";
NSString *const PREF_LOGIN_OBJECT=@"login_object";
NSString *const PREF_PASSWORD=@"password";
NSString *const PREF_TRIP_ID=@"trip_id";
#pragma mark-PARAM-VALUES
NSString *const APPLICATION_NAME=@"Eber Provider";
NSString *const COPYRIGHTS_NOTE=@"COPYRIGHTS_NOTE";
NSString *const MANUAL=@"manual";
NSString *const GOOGLE=@"google";
NSString *const FACEBOOK=@"facebook";
NSString *const DEVICE_TYPE=@"ios";
NSString *const SUCCESS=@"success";
NSString *const MESSAGE=@"message";
NSString *const ERROR_CODE=@"error_code";
NSString *strForCurLatitude=@"0";
NSString *strForCurLongitude=@"0";
NSString *const IOS=@"ios";
NSString *DEVICE_TOKEN=@"";/*CHANGE ON DEMAND*/
BOOL IS_TRIP_EXSIST=NO;
BOOL IS_PROVIDER_ACCEPTED=NO;
BOOL IS_DOCUMENT_PARENT_IS_REGISTER=NO;
BOOL PATH_DRAW_ENABLE=YES;
#pragma mark-Tablview Spacing
NSInteger const ROW_HEIGHT=70;
NSInteger const CELL_SPACE=2;
#pragma mark -PUSH Param
Boolean checkNewTrip=false;
Boolean checkCancelTrip=false;
Boolean checkApprove=false;
Boolean checkDecline=false;
#pragma mark-Measurement Units
NSString *TIME_SUFFIX=@"min";
NSInteger SECONDS_REMAIN=60;
NSString *DISTANCE_SUFFIX=@"km";
NSString *CurrencySign=@"$";
NSString *DistanceUnit=@"1";
NSString *GoogleServerKey= @"";
NSString *HotlineAppKey= @"";
NSString *HotlineAppId= @"";


#pragma mark -
#pragma mark - GOOGLE-PARAMS
NSString *const PARAM_GOOGLE_SERVER_KEY=@"ios_provider_app_google_key";
NSString *const PARAM_HOTLINE_KEY=@"hotline_app_key";
NSString *const PARAM_HOTLINE_APP_ID=@"hotline_app_id";

NSString *const GOOGLE_PARAM_DESTINATION_ADDRESS=@"destination_addresses";
NSString *const GOOGLE_PARAM_ELEMENTS=@"elements";
NSString *const GOOGLE_PARAM_VALUES=@"value";
NSString *const GOOGLE_PARAM_STATUS=@"status";
NSString *const GOOGLE_PARAM_ROWS=@"rows";
NSString *const GOOGLE_PARAM_STATUS_OK=@"OK";
NSString *const GOOGLE_PARAM_DISTANCE=@"distance";
NSString *const GOOGLE_PARAM_DURATION=@"duration";
NSString *const GOOGLE_PARAM_RESULTS=@"results";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_FORMATTED_ADDRESS=@"formatted_address";
#pragma FACEBOOK_PARAMETERS
NSString *const PARAM_FB_PUBLIC_PROFILE=@"public_profile";
NSString *const PARAM_FB_EMAIL=@"email";
NSString *const PARAM_FB_USER_FRIENDS=@"user_friends";
NSString *const PARAM_FB_FIELDS=@"fields";
NSString *const PARAM_FB_ID=@"id";
NSString *const PARAM_FB_REQUIRED_FIELDS=@"first_name, last_name, picture.type(large), email, name, id, gender";
NSInteger WAITING_SECONDS_REMAIN=0;
@end
