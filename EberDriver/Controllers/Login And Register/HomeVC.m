//
//  HomeVC.m
//  
//
//  Created by Elluminati Mini Mac 5 on 12/08/16.
//
//

#import "HomeVC.h"
#import "PreferenceHelper.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Parser.h"
#import "UtilityClass.h"
#import "invoice.h"
#import "SWRevealViewController.h"
#import "TripVC.h"
#import "SliderVC.h"
@interface HomeVC ()
{
    
}
@end

@implementation HomeVC

#pragma mark-View LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocalization];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
/*Set Intial View Colors & Text  */
-(void) setLocalization
{
    [self.view bringSubviewToFront:_imgLogo];
    [self.btnSignIn setBackgroundColor:[UIColor clearColor]];
    [self.btnRegister setBackgroundColor:[UIColor clearColor]];
    [_lblCopyrighttext setText:NSLocalizedString(@"ALL_COPYRIGHTS_RESERVED", nil)];
    [self.btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateNormal];
    [self.btnSignIn setTitle:NSLocalizedString(@"LOGIN", nil) forState:UIControlStateNormal];
    self.navigationController.navigationBarHidden=YES;
    [_lblCopyrighttext setTextColor:[UIColor labelTitleColor]];
    [self.btnSignIn setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [self.btnRegister setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
#pragma mark -Actions
- (IBAction)onClickBtnSignIn:(id)sender
{}
- (IBAction)onClickBtnRegister:(id)sender
{}

@end
