//
//  InvoiceVC.m
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 29/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "FeedbackVC.h"
#import "invoice.h"
#import "RatingBar.h"
#import "PreferenceHelper.h"
#import "Parser.h"
#import "MapVC.h"
#import "NSObject+Constants.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "User.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "MapVC.h"
#import "UILabel+overrideLabel.h"
#import "UIImageView+image.h"
#import "UIColor+Colors.h"
@interface FeedbackVC ()
{
    MapVC *m;
    UILabel *placeholderLabel;
}
@end

@implementation FeedbackVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocalization];
    _imgUser.image=[UIImage imageNamed:@"user"];
    [[UtilityClass sharedObject] loadFromURL:[NSURL URLWithString:[[User sharedObject] profileImage ]] callback:^(UIImage *image) {
        if (image)
        {  _imgUser.image=image;
        }else
        {
            _imgUser.image=[UIImage imageNamed:@"user"];
        }
    }];
    [_lblProviderName setText:[NSString stringWithFormat:@"%@ %@",[User sharedObject].firstName,[User sharedObject].lastName] withColor:[UIColor textColor]];
    Invoice *i=[Invoice sharedObject];
    [_lblfTime setText:[NSString stringWithFormat:@"%@ %@", i.time,TIME_SUFFIX] withColor:[UIColor textColor]];
    [_lblCost setText:[NSString stringWithFormat:@"%@ %@", i.distance,DISTANCE_SUFFIX ] withColor:[UIColor textColor]];
    [APPDELEGATE hideLoadingView];
}
-(void)setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [_viewForFeedBack setBackgroundColor:[UIColor backGroundColor]];
    [_btnRate setBackgroundColor:[UIColor buttonColor]];
    [_commentView setBackgroundColor:[UIColor whiteColor]];
    [_txtComment setBackgroundColor:[UIColor whiteColor]];
    [_btnRate setTitle:[NSLocalizedString(@"BTN_SUBMIT", nil) uppercaseString] forState:UIControlStateNormal];
    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, _txtComment.frame.size.width - 20.0, 34.0)];
    [placeholderLabel setText:NSLocalizedString(@"ENTER_REVIEW", nil)];
    [placeholderLabel setBackgroundColor:[UIColor clearColor]];
    [placeholderLabel setFont:[_txtComment font]];
    [placeholderLabel setTextColor:[UIColor lightGrayColor]];
    [placeholderLabel setNumberOfLines:2];
    [_txtComment setText:@""];
    [_txtComment setTextColor:[UIColor textColor]];
    [_txtComment addSubview:placeholderLabel];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self customSetup];
    [_btnNavigation setTitle:NSLocalizedString(@"TITLE_FEEDBACK", nil) forState:UIControlStateNormal];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
 
}
-(void)viewDidLayoutSubviews
{
    [_imgUser setRoundImageWithColor:[UIColor borderColor]];
    _imgUser.center=CGPointMake(self.view.center.x,_imgUser.center.y);
    ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(200, 40) AndPosition:CGPointMake(self.view.center.x-100,_lblProviderName.frame.origin.y+_lblProviderName.frame.size.height+10)];
    ratingView.backgroundColor=[UIColor clearColor];
    [self.viewForFeedBack addSubview:ratingView];
    [self.viewForFeedBack bringSubviewToFront:ratingView];
}

-(void)wsRateProvider:(double)rate andReview:(NSString*)review
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:PREF.providerId forKey:PARAM_PROVIDER_ID];
    [dictParam setValue:PREF.providerTripId forKey:PARAM_TRIP_ID];
    [dictParam setValue:[NSNumber numberWithDouble:rate] forKey:PARAM_RATING];
    [dictParam setValue:review forKey:PARAM_REVIEW];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",PROVIDER_RATE_USER,PREF.providerTripId];
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_RATING", nil)];
        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn getDataFromUrl:strUrl withParamData:dictParam withMethod:put withBlock:^(id response, NSError *error)
        {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                               NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions error:nil];
                               [APPDELEGATE hideLoadingView];
						   
							   if([[jsonResponse valueForKey:SUCCESS]boolValue])
                               {
                                   [[User sharedObject] resetObject];
                                   [APPDELEGATE goToMap];
                                   return;
							   }
                           });
			}];
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

- (IBAction)onClickBtnRate:(id)sender
{
    RBRatings rating=[ratingView getcurrentRatings];
    double rate=rating/2.0;
    if (rating%2 != 0)
    {rate += 0.5;}
    if(rate==0)
    {
        [[AppDelegate sharedAppDelegate] hideLoadingView];
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_GIVE_RATE",nil)];
    }
    else
    {
        [self wsRateProvider:rate andReview:[NSString stringWithFormat:@"%@",_txtComment.text]];
    }

}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComment resignFirstResponder];
}
- (void)customSetup
{
    [self.btnNavigation addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    SWRevealViewController *reveal = self.revealViewController;
    reveal.panGestureRecognizer.enabled = NO;
}
-(void)dealloc
{
    [User sharedObject].pickUpLatitude=@"null";
    [User sharedObject].pickUpLongitude=@"null";
    [User sharedObject].destAddress=@"null";
    
}
#pragma mark-TEXTVIEW DELEGATE
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [txtView resignFirstResponder];
    return NO;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    CGPoint pt;
    CGRect rc = [textView bounds];
    rc = [textView convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 100;
    [_scrollView setContentOffset:pt animated:YES];
    return YES;
}

- (void) textViewDidChange:(UITextView *)theTextView
{
    if(![_txtComment hasText]) {
        [_txtComment addSubview:placeholderLabel];
        [UIView animateWithDuration:0.15 animations:^{
            placeholderLabel.alpha = 1.0;
        }];
    } else if ([[_txtComment subviews] containsObject:placeholderLabel]) {
        
        [UIView animateWithDuration:0.15 animations:^{
            placeholderLabel.alpha = 0.0;
        } completion:^(BOOL finished) {
            [placeholderLabel removeFromSuperview];
        }];
    }
}


- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if (![_txtComment hasText])
    {
        [_txtComment addSubview:placeholderLabel];
        [UIView animateWithDuration:0.15 animations:^{
            placeholderLabel.alpha = 1.0;
        }];
    }
}
@end
