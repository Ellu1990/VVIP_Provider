//
//  CustomAlertWithTitle.h
//  Eber Provider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomSimpleAlert;
@protocol CustomSimpleAlertDelegate <NSObject>
@optional
- (void) onClickCustomSimpleDialogOk:(CustomSimpleAlert*)view;
@end

@interface CustomSimpleAlert: UIView
-(instancetype)initWithTitle:(NSString*)title message:(NSString *)message delegate:(id)delegate okButtonTitle:(NSString *)okButtonTitle;
@property (nonatomic, assign) id<CustomSimpleAlertDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property(strong,nonatomic)id parent;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
- (IBAction)onClickBtnOk:(id)sender;
- (void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;

@end
